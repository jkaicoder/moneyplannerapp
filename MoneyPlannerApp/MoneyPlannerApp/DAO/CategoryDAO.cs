﻿using System.Collections.Generic;
using System.Linq;
using MoneyPlannerApp.Helper;
using MoneyPlannerApp.Models;
using SQLite;

namespace MoneyPlannerApp.DAO
{
    public static class CategoryDAO
    {
        private static SQLiteConnection db = DBConnection._dbConnection;

        public static bool AddCategory(Category category)
        {
            DBConnection._dbConnection.CreateTable<Category>();
            //var maxPK = _dbConnection.Table<Category>().OrderByDescending(c => c.CategoryID).FirstOrDefault();
            //category.CategoryID = (maxPK == null ? 1 : maxPK.CategoryID + 1);
            if (DBConnection._dbConnection.Insert(category) != 0)
                return true;
            return false;
        }

        public static IEnumerable<Category> GetCategoryList(bool categoryType)
        {
            return (from cat in db.Table<Category>() select cat).Where(x => x.CategoryType == categoryType);
        }

        //public static IEnumerable<Category> GetCategoryByID(int categoryID)
        //{
        //    return (from cat in db.Table<Category>() select cat).Where(x => x.CategoryID == categoryID);
        //}

        public static bool UpdateCategory(Category category)
        {
            if (db.Update(category) != 0)
            {
                return true;
            }
            return false;
        }

        public static bool DeleteCategory(Category category)
        {
            if (db.Delete(category) != 0)
            {
                return true;
            }
            return false;
        }

    }
}
