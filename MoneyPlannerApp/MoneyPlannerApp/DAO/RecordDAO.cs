﻿using System.Collections.Generic;
using MoneyPlannerApp.Helper;
using MoneyPlannerApp.Models;
using SQLite;
using SQLiteNetExtensions.Extensions;

namespace MoneyPlannerApp.DAO
{
    public static class RecordDAO
    {
        private static SQLiteConnection db = DBConnection._dbConnection;

        public static IEnumerable<Record> GetRecordList()
        {
            return db.GetAllWithChildren<Record>();
        }

        public static bool AddRecord(Record record)
        {

            if (db.Insert(record) != 0)
            {
                db.UpdateWithChildren(record);
                
                return true;
            }
            return false;

        }

        public static bool UpdateRecord(Record record)
        {
            if(db.Update(record) != 0)
            {
                db.UpdateWithChildren(record);
                return true;
            }
            return false;
        }

        public static bool DeleteRecord(Record record)
        {
            if(db.Delete(record) !=0)
            {
                db.UpdateWithChildren(record);
                return true;
            }
            return false;
        }



        public static bool DeleteAllRecord()
        {
            if (db.DeleteAll<Record>() != 0)
            {

                return true;
            }
            return false;
        }
    }
}
