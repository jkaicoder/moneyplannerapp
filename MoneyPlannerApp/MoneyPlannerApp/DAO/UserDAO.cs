﻿using System;
using System.Collections.Generic;
using System.Text;
using MoneyPlannerApp.Helper;
using MoneyPlannerApp.Models;
using SQLite;
using SQLiteNetExtensions.Extensions;

namespace MoneyPlannerApp.DAO
{
    public static class UserDAO
    {
        private static SQLiteConnection db = DBConnection._dbConnection;

        public static bool InsertToken(UserToken userToken)
        {
            

            if (db.Insert(userToken) != 0)
            {
                db.UpdateWithChildren(userToken);
                return true;
            }

            return false;
        }

        public static IEnumerable<UserToken> GetUserToken()
        {
            return db.GetAllWithChildren<UserToken>();
        }

        public static bool RemoveUserToken()
        {
            if (db.DeleteAll<UserToken>() != 0)
            {
                return true;
            }

            return false;
        }
    }
}
