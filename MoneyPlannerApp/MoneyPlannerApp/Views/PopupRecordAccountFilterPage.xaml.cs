﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopupRecordAccountFilterPage : Rg.Plugins.Popup.Pages.PopupPage
    {
		public PopupRecordAccountFilterPage (ObservableCollection<Record> recordList)
		{
			InitializeComponent ();
            DataViewAndViewModel.PopupRecordAccountFilterPage = this;
            BindingContext = new PopupRecordAccountFilterViewModel(Navigation, recordList);
        }
	}
}