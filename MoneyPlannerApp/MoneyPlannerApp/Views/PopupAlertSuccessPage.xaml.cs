﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopupAlertSuccessPage : Rg.Plugins.Popup.Pages.PopupPage
    {
        public PopupAlertSuccessPage()
        {
            InitializeComponent();
        }

        private void BtnOK_OnClicked(object sender, EventArgs e)
        {
            Navigation.RemovePopupPageAsync(this);
        }
    }
}