﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FormsControls.Base;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IntroPage : AnimationPage
    {
        public IntroPage()
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense(LicenseUI.GetLicense());
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = new IntroViewModel(Navigation);
        }

        protected override bool OnBackButtonPressed()
        {
            Navigation.PushPopupAsync(new CustomDialogExit());
            return true;
        }

    }
}