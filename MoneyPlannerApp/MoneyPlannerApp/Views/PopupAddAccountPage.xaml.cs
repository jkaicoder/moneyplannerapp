﻿using MoneyPlannerApp.Models;
using MoneyPlannerApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopupAddAccountPage : Rg.Plugins.Popup.Pages.PopupPage
    {
		public PopupAddAccountPage ( )
		{
			InitializeComponent ();
            BindingContext = new PopupAddAccountViewModel(Navigation, this);
		}

        public PopupAddAccountPage(Account account)
        {
            InitializeComponent();
            BindingContext = new PopupAddAccountViewModel(Navigation, this, account);
        }
    }
}