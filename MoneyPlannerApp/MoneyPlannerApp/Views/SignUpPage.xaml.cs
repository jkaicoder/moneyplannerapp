﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FormsControls.Base;
using ImageCircle.Forms.Plugin.Abstractions;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SignUpPage : AnimationPage
    {
		public SignUpPage ()
		{
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense(LicenseUI.GetLicense());
            InitializeComponent();
            DataViewAndViewModel.SignUpPage = this;
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = new SignUpViewModel(Navigation);

        }

        //protected override bool OnBackButtonPressed()
        //{
        //    Avatar.Source = DataViewAndViewModel.Avatar;
        //    return true;
        //}


    }
}