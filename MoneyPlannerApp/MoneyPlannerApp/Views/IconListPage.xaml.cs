﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FormsControls.Base;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.ViewModels;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IconListPage : AnimationPage
    {
        Account _account;
        private Category _category;
        private int isType;

        public IconListPage(Category category)
        {
            isType = 1;
            InitializeComponent();
            _category = category;
            BindingContext = new IconRepository(Navigation, category);
            NavigationPage.SetHasNavigationBar(this, false);
        }

        public IconListPage(Category category, bool isEditCategory)
        {
            isType = 2;
            InitializeComponent();
            _category = category;
            BindingContext = new IconRepository(Navigation, category, isEditCategory);
            NavigationPage.SetHasNavigationBar(this, false);

        }

        public IconListPage(Account account, int isAddAccount)
        {
            isType = 3;
            InitializeComponent();
            _account = account;
            BindingContext = new IconRepository(Navigation, account, isAddAccount);
            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override bool OnBackButtonPressed()
        {
            if (isType == 1)
            {
                Navigation.PopAsync(true);
                Navigation.PushPopupAsync(new PopupAddCategoryPage(_category));
                isType = 0;
                return true;
            }
            else if (isType == 2)
            {
                Navigation.PopAsync(true);
                Navigation.PushPopupAsync(new PopupEditCategoryPage(_category));
                return true;
            }
            else if (isType == 3)
            {
                Navigation.PopAsync(true);
                Navigation.PushPopupAsync(new PopupAddAccountPage(_account));
                return true;
            }
            else
            {
                Navigation.PopAsync(true);
                return true;
            }

            return true;
        }

    }
}