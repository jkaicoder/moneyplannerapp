﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopupAddCategoryPage : Rg.Plugins.Popup.Pages.PopupPage
    {
		public PopupAddCategoryPage ()
		{
			InitializeComponent ();
            DataViewAndViewModel.PopupAddCategoryPage = this;
            BindingContext = new PopupAddCategoryViewModel(Navigation);
		}

        public PopupAddCategoryPage(Category category)
        {
            InitializeComponent();
            DataViewAndViewModel.PopupAddCategoryPage = this;
            BindingContext = new PopupAddCategoryViewModel(Navigation, category);
        }
    }
}