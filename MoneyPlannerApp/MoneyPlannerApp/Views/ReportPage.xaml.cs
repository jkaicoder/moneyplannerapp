﻿using MoneyPlannerApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ReportPage : ContentPage
	{
		public ReportPage (Models.Account _accountSelected)
		{
			InitializeComponent ();
            BindingContext = new ReportViewModel(Navigation, _accountSelected);
            NavigationPage.SetHasNavigationBar(this, false);
        }
	}
}