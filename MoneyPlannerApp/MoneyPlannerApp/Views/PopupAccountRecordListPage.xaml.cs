﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopupAccountRecordListPage : Rg.Plugins.Popup.Pages.PopupPage
    {
		public PopupAccountRecordListPage (Account account)
		{
			InitializeComponent ();
            DataViewAndViewModel.PopupAccountRecordListPage = this;
            BindingContext = new PopupAccountRecordListViewModel(Navigation, account);
        }
	}
}