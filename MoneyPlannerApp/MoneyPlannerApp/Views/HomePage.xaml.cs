﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BottomBar.XamarinForms;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.ViewModels;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomePage 
    {
		public HomePage ()
		{
			InitializeComponent ();

            BindingContext = new HomeViewModel(Navigation);
            NavigationPage.SetHasNavigationBar(this, false);

            var page = new ItemHomePage();
            MainView.Content = page.Content;
        }

        private void TapGestureRecognizer_OnTappedHome(object sender, EventArgs e)
        {
            var page = new ItemHomePage();
            MainView.Content = page.Content;
        }

        private void TapGestureRecognizer_OnTappedNewRecord(object sender, EventArgs e)
        {
            var page = new ItemRecordPage();
            MainView.Content = page.Content;
        }

        private void TapGestureRecognizer_OnTappedAccount(object sender, EventArgs e)
        {
            var page = new ItemAccountPage();
            MainView.Content = page.Content;
        }

        private void TapGestureRecognizer_OnTappedMore(object sender, EventArgs e)
        {
            var page = new ItemMorePage();
            MainView.Content = page.Content;
        }

        protected override bool OnBackButtonPressed()
        {
            Navigation.PushPopupAsync(new CustomDialogExit());
            return true;
        }


    }
}