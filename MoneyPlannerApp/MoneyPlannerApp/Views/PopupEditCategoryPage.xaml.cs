﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.ViewModels;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopupEditCategoryPage : Rg.Plugins.Popup.Pages.PopupPage
    {

        private string _categoryNameTemp;
        private string _categoryDescriptionTemp;

		public PopupEditCategoryPage(Category category)
		{
			InitializeComponent ();
            _categoryNameTemp = category.CategoryName;
            _categoryDescriptionTemp = category.CategoryDescription;
            DataViewAndViewModel.PopupEditCategoryPage = this;
            BindingContext = new PopupEditCategoryViewModel(Navigation, category);
		}

        protected override bool OnBackButtonPressed()
        {
            DataViewAndViewModel.EditCategory = false;
            txtCategoryName.Text = _categoryNameTemp;
            txtCategoryDescription.Text = _categoryDescriptionTemp;
            Navigation.RemovePopupPageAsync(this);
            return true;
        }
    }
}