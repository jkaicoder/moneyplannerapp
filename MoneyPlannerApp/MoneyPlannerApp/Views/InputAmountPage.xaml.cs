﻿using MoneyPlannerApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InputAmountPage : ContentPage
    {
        public InputAmountPage(Models.Account _accountSelected)
        {
            InitializeComponent();
            BindingContext = new InputAmountViewModel(Navigation, _accountSelected);
            NavigationPage.SetHasNavigationBar(this, false);

        }

    }
}