﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoneyPlannerApp.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ItemRecordPage : ContentPage
	{
		public ItemRecordPage ()
		{

            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense(LicenseUI.GetLicense());
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
	}
}