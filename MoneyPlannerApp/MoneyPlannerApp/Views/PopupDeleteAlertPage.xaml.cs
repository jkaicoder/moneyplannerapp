﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopupDeleteAlertPage : Rg.Plugins.Popup.Pages.PopupPage
    {
		public PopupDeleteAlertPage(Category category)
		{
			InitializeComponent ();
            DataViewAndViewModel.PopupDeleteAlertPage = this;
            BindingContext = new PopupDeleteAlertViewModel(Navigation, category);
        }
	}
}