﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopupSettingsAccountPage : Rg.Plugins.Popup.Pages.PopupPage
    {
		public PopupSettingsAccountPage(AccountDetailPage accountDetailPage, HomeViewModel homeViewModel,
            Account account)
		{
			InitializeComponent ();
            DataViewAndViewModel.PopupSettingsAccountPage = this;
            BindingContext = new PopupAccountSettingViewModel(Navigation, account);
        }
	}
}