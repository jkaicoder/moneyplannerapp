﻿using MoneyPlannerApp.Models;
using MoneyPlannerApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopupAddRecord : Rg.Plugins.Popup.Pages.PopupPage
    {
        private INavigation navigation;
        private Record _record;

        public PopupAddRecord (Models.Account _accountSelected)
		{
			InitializeComponent ();
            BindingContext = new PopupAddRecordViewModel(Navigation, _accountSelected, this);
		}

        public PopupAddRecord(INavigation navigation, Record record)
        {
            this.navigation = navigation;
            _record = record;
            InitializeComponent();
            BindingContext = new PopupAddRecordViewModel(Navigation, _record, this);
        }
    }
}