﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SplashScreenPage : ContentPage
	{
		public SplashScreenPage ()
		{
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent ();
		}

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            

            await Title.TranslateTo(0, 200, 2000, Easing.BounceIn);
            await Title.ScaleTo(2, 2000, Easing.CubicIn);
            await Title.RotateTo(360, 2000, Easing.SinInOut);
            await Title.ScaleTo(1, 2000, Easing.CubicOut);
            await Title.TranslateTo(0, -50, 2000, Easing.BounceOut);

            await Title.FadeTo(1, 1500);
            await Title.FadeTo(0.5, 1000);
            await Title.FadeTo(0.1, 500);
            await Title.FadeTo(0, 0);

            //Application.Current.MainPage = new NavigationPage(new HomePage());
        }
    }
}