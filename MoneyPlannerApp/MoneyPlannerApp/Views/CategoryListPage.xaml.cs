﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FormsControls.Base;
using MoneyPlannerApp.Helper;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels;
using SQLite;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CategoryListPage : AnimationPage
    {
		public CategoryListPage ()
		{
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense(LicenseUI.GetLicense());
            InitializeComponent ();

            BindingContext = new CategoryListViewModel(Navigation);

            NavigationPage.SetHasNavigationBar(this, false);

        }

        //protected override bool OnBackButtonPressed()
        //{
        //    Navigation.PushAsync(new HomePage());
        //    return true;
        //}

    }
}