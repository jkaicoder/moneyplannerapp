﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CustomDialogExit : Rg.Plugins.Popup.Pages.PopupPage
    {
		public CustomDialogExit ()
		{
			InitializeComponent ();
		}

        private void BtnCancel_OnClicked(object sender, EventArgs e)
        {
            Navigation.RemovePopupPageAsync(this);
        }

        private void BtnExit_OnClicked(object sender, EventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().CloseMainWindow();
        }
    }
}