﻿using MoneyPlannerApp.Models;
using MoneyPlannerApp.ViewModels;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopupEditRecordPage : Rg.Plugins.Popup.Pages.PopupPage
    {
        public PopupEditRecordPage(INavigation navigation, Record record)
        {
            InitializeComponent();
            BindingContext = new PopupEditRecordViewModel(Navigation, record, this);
        }
        public PopupEditRecordPage(Record record)
        {
            InitializeComponent();
            BindingContext = new PopupEditRecordViewModel(Navigation, record, this);
        }
        protected override bool OnBackButtonPressed()
        {
            Navigation.RemovePopupPageAsync(this);
            DataViewAndViewModel.ButtonEditRecordPopup = false;
            return true;
        }
    }
}