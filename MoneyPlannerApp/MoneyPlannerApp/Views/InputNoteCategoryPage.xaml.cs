﻿using MoneyPlannerApp.Models;
using MoneyPlannerApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class InputNoteCategoryPage : ContentPage
	{
		public InputNoteCategoryPage (Record _record, bool isCreateRecord)
		{
			InitializeComponent ();
            
            BindingContext = new InputNoteCategoryViewModel(Navigation, _record, isCreateRecord);
        }

    }
}