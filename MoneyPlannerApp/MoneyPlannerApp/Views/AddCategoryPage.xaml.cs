﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FormsControls.Base;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddCategoryPage : AnimationPage
    {

        public AddCategoryPage()
        {

            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = new AddCategoryViewModel(Navigation);
        }

    }
}