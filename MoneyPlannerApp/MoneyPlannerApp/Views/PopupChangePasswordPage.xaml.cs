﻿using MoneyPlannerApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopupChangePasswordPage : Rg.Plugins.Popup.Pages.PopupPage
    {
		public PopupChangePasswordPage ()
		{
			InitializeComponent ();
            BindingContext = new PopupChangePasswordViewModel(Navigation, this);
		}
	}
}