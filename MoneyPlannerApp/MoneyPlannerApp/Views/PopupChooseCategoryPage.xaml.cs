﻿using MoneyPlannerApp.Models;
using MoneyPlannerApp.ViewModels;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopupChooseCategoryPage : Rg.Plugins.Popup.Pages.PopupPage
    {
        bool _isCreateRecord;
        Record _record;
        public PopupChooseCategoryPage (Record record, bool isCreateRecord)
		{
			InitializeComponent ();
            _isCreateRecord = isCreateRecord;
            _record = record;
            BindingContext = new PopupChooseCategoryViewModel(record, Navigation, isCreateRecord, this);
		}
        protected override bool OnBackButtonPressed()
        {
            if(_isCreateRecord)
            {
                Navigation.RemovePopupPageAsync(this);
                Navigation.PushPopupAsync(new PopupAddRecord(Navigation, _record));
                return true;
            }
            if(!_isCreateRecord)
            {
                Navigation.RemovePopupPageAsync(this);
                Navigation.PushPopupAsync(new PopupEditRecordPage(Navigation, _record));
                return true;
            }
            return true;
            

        }

    }
}