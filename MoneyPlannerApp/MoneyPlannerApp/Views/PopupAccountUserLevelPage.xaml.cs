﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopupAccountUserLevelPage : Rg.Plugins.Popup.Pages.PopupPage
    {
		public PopupAccountUserLevelPage (Account account, User user)
		{
			InitializeComponent ();
            DataViewAndViewModel.PopupAccountUserLevelPage = this;
            BindingContext = new PopupAccountUserLevelViewModel(Navigation, account, user);
        }
	}
}