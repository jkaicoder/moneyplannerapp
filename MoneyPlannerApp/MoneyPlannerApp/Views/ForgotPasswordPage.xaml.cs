﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FormsControls.Base;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ForgotPasswordPage : AnimationPage
    {
		public ForgotPasswordPage ()
		{
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense(LicenseUI.GetLicense());
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = new ForgotPasswodViewModel(Navigation);
        }
	}
}