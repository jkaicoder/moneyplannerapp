﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FormsControls.Base;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AccountDetailPage : AnimationPage
    {
		public AccountDetailPage(HomeViewModel homeViewModel, Account account)
		{
            InitializeComponent ();
            DataViewAndViewModel.AccountDetailPage = this;

            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = new AccountDetailViewModel(this, homeViewModel, Navigation, account);
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();



            //await Frame.TranslateTo(0, 5, 1000, Easing.CubicInOut);

            //await Frame.TranslateTo(0, 5, 1000, Easing.CubicInOut);
            //await Frame.ScaleTo(2, 2000, Easing.CubicIn);
            //await Frame.RotateTo(360, 2000, Easing.SinInOut);
            //await Frame.ScaleTo(1, 2000, Easing.CubicOut);
            //await Frame.TranslateTo(0, 0, 2000, Easing.BounceOut);

        }
    }
}