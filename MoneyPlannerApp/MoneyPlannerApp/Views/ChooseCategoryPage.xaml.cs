﻿using MoneyPlannerApp.Models;
using MoneyPlannerApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ChooseCategoryPage : ContentPage
	{
		public ChooseCategoryPage (Record record, bool isCreateRecord)
		{
            
			InitializeComponent ();
            BindingContext = new ChooseCategoryViewModel(record, Navigation, isCreateRecord);
		}


    }
}