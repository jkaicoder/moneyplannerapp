﻿using MoneyPlannerApp.Models;
using MoneyPlannerApp.ViewModels;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopupSummaryPage : Rg.Plugins.Popup.Pages.PopupPage
    {
        Record _record;
        bool _isCreateRecord;
		public PopupSummaryPage (Record record, bool isCreateRecord)
		{
			InitializeComponent ();
            _record = record;
            _isCreateRecord = isCreateRecord;
            BindingContext = new PopupSummaryViewModel(Navigation, _record, isCreateRecord, this);
        }
        protected override bool OnBackButtonPressed()
        {
                Navigation.RemovePopupPageAsync(this);
                Navigation.PushPopupAsync(new PopupChooseCategoryPage(_record, _isCreateRecord));
                return true;


        }
    }
}