﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyPlannerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopupAccountUserListPage : Rg.Plugins.Popup.Pages.PopupPage
    {
		public PopupAccountUserListPage (Account account)
		{
			InitializeComponent ();
            DataViewAndViewModel.PopupAccountUserListPage = this;
            BindingContext = new PopupAccountUserListViewModel(Navigation, account);
        }
	}
}