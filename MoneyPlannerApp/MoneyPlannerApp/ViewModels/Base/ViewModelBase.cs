﻿using System;
using System.Collections.Generic;
using System.Text;
using MoneyPlannerApp.MVVM;

namespace MoneyPlannerApp.ViewModels.Base
{
    public class ViewModelBase : BindableBase
    {
        private bool _isBusy;
        private string _title;

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                if (SetProperty(ref _isBusy, value))
                {
                    RaisePropertyChanged(nameof(IsNotBuSy));
                }
            }

        }

        public bool IsNotBuSy => !IsBusy;
    }
}
