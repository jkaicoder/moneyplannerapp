﻿using System;
using System.Collections.Generic;
using System.Text;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using System.Diagnostics;
using Xamarin.Forms;
using Plugin.Toast;
using MoneyPlannerApp.Services;
using System.Collections;
using Rg.Plugins.Popup.Extensions;

namespace MoneyPlannerApp.ViewModels
{
    class PopupAddRecordViewModel : ViewModelBase
    {
        private Account _accountSelected;
        private INavigation _navigation;
        private Record _record;
        private bool _isRecordNameEmpty;
        private bool _isRecordAmountEmpty;
        private DateTime _dateTimeNow;
        public bool IsDateTimeFuture
        {
            get => _isDateTimeFuture;
            set
            {
                SetProperty(ref _isDateTimeFuture, value);
            }
        }
        public string AccountNameTemp
        {
            get => _accountNameTemp;
            set
            {
                SetProperty(ref _accountNameTemp, value);
            }
        }

        public string ErrorTextAmount
        {
            get => _errorTextAmount;
            set
            {
                SetProperty(ref _errorTextAmount, value);
            }
        }
        public string ErrorText
        {
            get => _errorText;
            set
            {
                SetProperty(ref _errorText, value);
            }
        }
        private bool _isCreateRecord;
        private string _errorText;
        private string _errorTextAmount;
        private string _accountNameTemp;
        PopupAddRecord _popupAddRecord;

        public bool IsRecordAmountEmpty
        {
            get => _isRecordAmountEmpty;
            set
            {
                SetProperty(ref _isRecordAmountEmpty, value);
            }
        }

        private bool _isDescriptionNull;
        private bool _isDateTimeFuture;
        //private int _recordAmountTemp;

        public bool IsDescriptionNull
        {
            get => _isDescriptionNull;
            set
            {
                SetProperty(ref _isDescriptionNull, value);
            }
        }

        public bool IsRecordNameEmpty
        {
            get => _isRecordNameEmpty;
            set => SetProperty(ref _isRecordNameEmpty, value);
        }
        //public int RecordAmountTemp
        //{
        //    get => _recordAmountTemp;
        //    set
        //    {
        //        SetProperty(ref _recordAmountTemp, value);
        //        Debug.Write(RecordAmountTemp);
        //        //----------------------------------------/
        //        if (_recordAmountTemp > 99999999)
        //        {
        //            IsRecordAmountEmpty = true;
        //            ErrorTextAmount = "Value is too large";
        //        }
        //        else
        //        {
        //            IsRecordAmountEmpty = false;
        //        }
        //    }
        //}

        public DelegateCommand NextCommand { get; }

        public Record Record
        {
            get => _record;
            set
            {
                SetProperty(ref _record, value);
                Debug.Write(_record.RecordAmount);

            }
        }

        public PopupAddRecordViewModel(INavigation navigation, Account accountSelected, PopupAddRecord popupAddRecord)
        {
            AccountNameTemp = "New Record in Account " + DataViewAndViewModel.SelectedAccountDropdown.AccountName;
            _accountSelected = accountSelected;
            _popupAddRecord = popupAddRecord;
            NextCommand = new DelegateCommand(NextScreen);
            _navigation = navigation;
            _isCreateRecord = true;
            Record = new Record();
            Record.RecordCategory = new Category();
            Record.RecordDate = DateTime.Now;

            _dateTimeNow = new DateTime();
            _dateTimeNow = DateTime.Now;
        }
        //The second Constructer
        public PopupAddRecordViewModel(INavigation navigation, Record record, PopupAddRecord popupAddRecord)
        {
            AccountNameTemp = "New Record in Account " + DataViewAndViewModel.SelectedAccountDropdown.AccountName;
            _popupAddRecord = popupAddRecord;
            NextCommand = new DelegateCommand(NextScreen);
            _navigation = navigation;
            _isCreateRecord = true;
            Record = record;

            _dateTimeNow = new DateTime();
            _dateTimeNow = DateTime.Now;
        }

        private void NextScreen()
        {
            if (ValidationClass.CheckInternet())
            {
                Debug.Write(Record.RecordDescription);
                Debug.Write(Record.RecordDate);
                IsRecordNameEmpty = string.IsNullOrEmpty(Record.RecordName);

                if (Record.RecordAmount == 0)
                {
                    IsRecordAmountEmpty = true;
                    ErrorTextAmount = "This field is requied";
                }

                else
                {
                    IsRecordAmountEmpty = false;
                }
                if (DateTime.Compare(Record.RecordDate, _dateTimeNow) > 0)
                {
                    IsDateTimeFuture = true;
                }
                else
                {
                    IsDateTimeFuture = false;
                }



                CheckValidation();
                if (!IsRecordNameEmpty && !IsRecordAmountEmpty && DateTime.Compare(Record.RecordDate, _dateTimeNow) <= 0)
                {


                    if (CheckValidation())
                    {
                        IsDescriptionNull = string.IsNullOrEmpty(_record.RecordDescription);
                        if (!IsDescriptionNull)
                        {
                            _record.RecordDescription = _record.RecordDescription.Trim();
                        }


                        _navigation.RemovePopupPageAsync(_popupAddRecord);
                        _navigation.PushPopupAsync(new PopupChooseCategoryPage(_record, _isCreateRecord));
                    }


                }
            }


            //if (IsRecordNameEmpty)
            //    CrossToastPopUp.Current.ShowToastMessage("Name is empty");
            //if(IsRecordAmountEmpty)
            //    CrossToastPopUp.Current.ShowToastMessage("Amount is zero");
            //if (IsRecordAmountEmpty && IsRecordNameEmpty)
            //    CrossToastPopUp.Current.ShowToastMessage("Please enter fully");



        }

        private bool CheckValidation()
        {
            ArrayList errorList = new ArrayList();

            IsRecordNameEmpty = string.IsNullOrEmpty(Record.RecordName);

            string recordName = Record.RecordName;

            if (IsRecordNameEmpty)
            {
                ErrorText = "This field is required";
                errorList.Add(this);
            }
            else
            {
                if (string.IsNullOrWhiteSpace(recordName) || ValidationClass.StartSpaces(recordName) || ValidationClass.EndSpaces(recordName))
                {
                    IsRecordNameEmpty = true;
                    ErrorText = "Not allow spaces characters";
                    errorList.Add(this);
                }
                else
                {
                    if (ValidationClass.MinimumString(recordName, 4))
                    {
                        IsRecordNameEmpty = true;
                        ErrorText = "Please input more than 4 characters";
                        errorList.Add(this);
                    }
                    else
                    {
                        if (ValidationClass.IsHasSpecialCharacters(recordName))
                        {
                            IsRecordNameEmpty = true;
                            ErrorText = "Not allow special characters";
                            errorList.Add(this);
                        }
                    }
                }
            }

            if (errorList.Count > 0)
            {
                return false;
            }
            return true;


        }
    }
}
