﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Plugin.Toast;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace MoneyPlannerApp.ViewModels
{
    public class AccountInviteMemberViewModel : ViewModelBase
    {
        private INavigation _navigation;
        private UserServices _userServices;
        private Account _account;
        private ObservableCollection<User> _userListInvite;
        private string _searchTextUserInvite;
        private bool _isVisibleTextNotiSearch;
        private string _notiUserText;
        private User _userSelectedInvite;
        private AccountServices _accountServices;

        public DelegateCommand SearchUserCommand { get; }

        public ObservableCollection<User> UserListInvite
        {
            get => _userListInvite;
            set => SetProperty(ref _userListInvite, value);
        }

        public ObservableCollection<User> UserListInviteTemp { get; set; }

        public bool IsVisibleTextNotiSearch
        {
            get => _isVisibleTextNotiSearch;
            set => SetProperty(ref _isVisibleTextNotiSearch, value);
        }

        public string NotiUserText
        {
            get => _notiUserText;
            set => SetProperty(ref _notiUserText, value);
        }

        public string SearchTextUserInvite
        {
            get => _searchTextUserInvite;
            set => SetProperty(ref _searchTextUserInvite, value);
        }

        
        private void SearchCommandExecute()
        {
            if (!String.IsNullOrEmpty(SearchTextUserInvite))
            {
                UserListInvite.Clear();
                ObservableCollection<User> searchUserList = new ObservableCollection<User>();

                var tempUserList = UserListInviteTemp.Where(x => x.Email.ToLower().EndsWith(SearchTextUserInvite.ToLower()));

                foreach (User item in tempUserList)
                {
                    searchUserList.Add(item);
                }

                UserListInvite = searchUserList;

                if (UserListInvite.Count == 0)
                {
                    IsVisibleTextNotiSearch = true;
                    NotiUserText = "Sorry, we didn't find any results matching this search";
                }
                else
                {
                    IsVisibleTextNotiSearch = false;
                }
            }
            else
            {
                UserListInvite.Clear();
                IsVisibleTextNotiSearch = true;
                NotiUserText = "Sorry, we didn't find any results matching this search";
            }
        }

        public AccountInviteMemberViewModel(INavigation navigation, Account account)
        {
            _navigation = navigation;
            _userServices = new UserServices();
            _accountServices = new AccountServices();
            _account = account;
            _userListInvite = new ObservableCollection<User>();
            SearchUserCommand = new DelegateCommand(SearchCommandExecute);
            GetUserListInviteApi();
        }

        private async void GetUserListInviteApi()
        {
            if (ValidationClass.CheckInternet())
            {
                var resultAsync = await _userServices.GetUserListInvite(_account.AccountID);
                if (resultAsync.Status)
                {
                    for (int i = 0; i < resultAsync.Data.Count; i++)
                    {
                        resultAsync.Data[i].UserImage = Configuration.ID_HOST + resultAsync.Data[i].UserImage;
                    }
                    UserListInviteTemp = resultAsync.Data;
                }
                else
                {
                    CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                }
            }
        }

        public User UserSelectedInvite
        {
            get => _userSelectedInvite;
            set
            {
                SetProperty(ref _userSelectedInvite, value);
                if (_userSelectedInvite != null)
                {
                    AddUserToAccount(_account.AccountID, UserDAO.GetUserToken().Last().UserID, UserSelectedInvite.UserID);
                }
            }
        }

        private async void AddUserToAccount(string accountID, string userID, string userIdAdd)
        {
            if (ValidationClass.CheckInternet())
            {
                var page = new PopupPage();
                await _navigation.PushPopupAsync(page);

                var resultAsync = await _accountServices.AddUserToAccount(new InfoID()
                {
                    UserID = userID,
                    AccountID = accountID,
                    UserIDAdd = userIdAdd,
                    Role = 3
                });
                await _navigation.RemovePopupPageAsync(page);
                TokenServices.CheckToken(resultAsync.StatusCode, _navigation);
                if (resultAsync.Status)
                {
                    //UserSelectedInvite.AccountLevel = "Member";

                    //for (int i = 0; i < Account.AccountUserList.Count; i++)
                    //{
                    //    if (Account.AccountUserList[i].OwerAccount == 1)
                    //    {
                    //        Account.AccountUserList[i].AccountLevel = "Admin";
                    //    }
                    //    else
                    //    {
                    //        Account.AccountUserList[i].AccountLevel = "Member";
                    //    }
                    //}
                    _navigation.PushPopupAsync(new PopupAlertSuccessPage());
                }
                else
                {
                    CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                }
            }
        }
    }
}
