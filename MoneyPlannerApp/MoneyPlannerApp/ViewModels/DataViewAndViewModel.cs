﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.Views;
using Xamarin.Forms;

namespace MoneyPlannerApp.ViewModels
{
    public static class DataViewAndViewModel
    {
        //View
        public static PopupAccountUserListPage PopupAccountUserListPage { get; set; }
        public static PopupSettingsAccountPage PopupSettingsAccountPage { get; set; }
        public static AccountDetailPage AccountDetailPage { get; set; }
        public static PopupAddCategoryPage PopupAddCategoryPage { get; set; }
        public static PopupEditCategoryPage PopupEditCategoryPage { get; set; }
        public static PopupDeleteAlertPage PopupDeleteAlertPage { get; set; }
        public static SignUpPage SignUpPage { get; set; }

        //ViewModel
        public static HomeViewModel HomeViewModel { get; set; }
        public static ProfileViewModel ProfileViewModel { get; set; }
        public static CategoryListViewModel CategoryListViewModel { get; set; }
        public static PopupEditCategoryViewModel PopupEditCategoryViewModel { get; set; }

        public static ImageSource Avatar { get; set; }

        public static Account SelectedAccountDropdown { get; set; }
        public static PopupRecordAccountFilterPage PopupRecordAccountFilterPage { get; set; }
        public static RecordFilter RecordFilter { get; set; }
        public static PopupAccountRecordListViewModel PopupAccountRecordListViewModel { get; set; }
        public static ObservableCollection<Record> RecordTemp { get; set; }
        public static ObservableCollection<Record> RecordListTemp { get; set; }
        public static bool EditCategory { get; set; } = false;
        public static PopupAccountRecordListPage PopupAccountRecordListPage { get; set; }
        public static PopupAccountUserLevelPage PopupAccountUserLevelPage { get; set; }
        public static PopupAccountUserListViewModel PopupAccountUserListViewModel { get; set; }

        public static bool ButtonEditRecordPopup { get; set; } = false;
        public static int UserLevel { get; set; } = 1;
    }
}
