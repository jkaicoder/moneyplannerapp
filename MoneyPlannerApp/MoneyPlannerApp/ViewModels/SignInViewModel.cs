﻿using System.Collections;
using System.Diagnostics;
using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Plugin.Toast;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace MoneyPlannerApp.ViewModels
{
    public class SignInViewModel : ViewModelBase
    {
        private readonly INavigation _navigation;
        private readonly UserServices _userServices;

        private User _user;
        private bool _rememberMe = false;
        private bool _isUserNameError;
        private bool _isPasswordError;
        private string _userNameErrorText;
        private string _passwordErrorText;

        public DelegateCommand SignInCommand { get; }

        public DelegateCommand ForgotPasswordCommand { get; }

        public bool IsUserNameError
        {
            get => _isUserNameError;
            set => SetProperty(ref _isUserNameError, value);
        }

        public bool IsPasswordError
        {
            get => _isPasswordError;
            set => SetProperty(ref _isPasswordError, value);
        }

        public string UserNameErrorText
        {
            get => _userNameErrorText;
            set => SetProperty(ref _userNameErrorText, value);
        }

        public string PasswordErrorText
        {
            get => _passwordErrorText;
            set => SetProperty(ref _passwordErrorText, value);
        }

        public User User
        {
            get => _user;
            set
            {
                SetProperty(ref _user, value);
            }
        }

        public SignInViewModel(INavigation navigation)
        {
            _navigation = navigation;
            _userServices = new UserServices();
            _user = new User();
            SignInCommand = new DelegateCommand(SignIn);
            ForgotPasswordCommand = new DelegateCommand(ForgotPassword);
        }

        public bool RememberMe
        {
            get => _rememberMe;
            set => SetProperty(ref _rememberMe, value);
        }

        private void ForgotPassword()
        {
            _navigation.PushAsync(new ForgotPasswordPage());
        }

        private bool CheckValidation()
        {
            ArrayList errorList = new ArrayList();

            string userName = User.UserName;
            string passWord = User.Password;

            if (ValidationClass.IsNullOrEmpty(userName))
            {
                IsUserNameError = true;
                UserNameErrorText = "This field is required";
                errorList.Add(this);
            }
            else
            {
                IsUserNameError = false;
                
            }

            if (ValidationClass.IsNullOrEmpty(passWord))
            {
                IsPasswordError = true;
                PasswordErrorText = "This field is required";
                errorList.Add(this);
            }
            else
            {
                IsPasswordError = false;
            }

            if (errorList.Count > 0)
            {
                return false;
            }

            return true;

        }

        private void DataProcess()
        {
            
            _user.UserName = _user.UserName.Trim();
            _user.Password = SecurePasswordHasher.Hash(_user.Password);

            if (!RememberMe)
            {
                UserDAO.RemoveUserToken();
            }

        }


        private async void SignIn()
        {
            if (ValidationClass.CheckInternet())
            {
                if (CheckValidation())
                {
                    if (ValidationClass.IsHasSpecialCharacters(User.UserName))
                    {
                        CrossToastPopUp.Current.ShowToastError("Username or Password invalid");
                    }
                    else
                    {
                        if (ValidationClass.MinimumString(User.UserName, 6))
                        {
                            CrossToastPopUp.Current.ShowToastError("Username or Password invalid");
                        }
                        else
                        {
                            DataProcess();

                            var page = new PopupPage();
                            await _navigation.PushPopupAsync(page);

                            var resultAsync = await _userServices.SignInAsync(User);

                            await _navigation.RemovePopupPageAsync(page);

                            if (resultAsync.Status)
                            {
                                UserToken userToken;

                                if (_rememberMe)
                                {
                                    userToken = new UserToken() { UserID = resultAsync.Data.UserID, Token = resultAsync.Token, RememberMe = true };
                                }
                                else
                                {
                                    userToken = new UserToken() { UserID = resultAsync.Data.UserID, Token = resultAsync.Token, RememberMe = false };
                                }

                                UserDAO.InsertToken(userToken);

                                DataViewAndViewModel.UserLevel = resultAsync.Data.UserLevel;

                                await _navigation.PushAsync(new HomePage());
                            }
                            else
                            {
                                CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                            }
                        }
                    }
                }
            }
        }
    }
}
