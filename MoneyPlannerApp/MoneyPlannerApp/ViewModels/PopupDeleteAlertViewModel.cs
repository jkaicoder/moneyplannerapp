﻿using System;
using System.Collections.Generic;
using System.Text;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels.Base;
using Plugin.Toast;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Xamarin.Forms;

namespace MoneyPlannerApp.ViewModels
{
    public class PopupDeleteAlertViewModel : ViewModelBase
    {
        private INavigation _navigation;
        private CategoryServices _categoryServices;
        private Category _category;

        public DelegateCommand CancelCommand { get; }
        public DelegateCommand DeleteCategoryCommand { get; }

        public PopupDeleteAlertViewModel(INavigation navigation, Category category)
        {
            _navigation = navigation;
            _categoryServices = new CategoryServices();
            _category = category;
            CancelCommand = new DelegateCommand(Cancel);
            DeleteCategoryCommand = new DelegateCommand(DeleteCategory);
        }

        private async void Cancel()
        {
            await _navigation.RemovePopupPageAsync(DataViewAndViewModel.PopupDeleteAlertPage);
        }

        private async void DeleteCategory()
        {
            await _navigation.RemovePopupPageAsync(DataViewAndViewModel.PopupDeleteAlertPage);
            await _navigation.RemovePopupPageAsync(DataViewAndViewModel.PopupEditCategoryPage);

            var pageLoad = new PopupPage();
            await _navigation.PushPopupAsync(pageLoad);
            var resultAsync = await _categoryServices.DeleteCategory(_category);
            TokenServices.CheckToken(resultAsync.StatusCode, _navigation);
            await _navigation.RemovePopupPageAsync(pageLoad);
            DataViewAndViewModel.EditCategory = false;
            if (resultAsync.Status)
            {
                DataViewAndViewModel.CategoryListViewModel.GetCategoryList();
                CrossToastPopUp.Current.ShowToastSuccess(resultAsync.Message);
            }
            else
            {
                CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
            }
        }
        
    }
}
