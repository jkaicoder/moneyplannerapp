﻿using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Xamarin.Forms;
using Plugin.Toast;
using MoneyPlannerApp.DAO;
using System.Threading.Tasks;
using MoneyPlannerApp.Services;
using System.Collections;
using Rg.Plugins.Popup.Extensions;

namespace MoneyPlannerApp.ViewModels
{
    class PopupEditRecordViewModel : ViewModelBase
    {
        private INavigation _navigation;
        public Record _record;
        private bool _incomeCheck = false;
        private bool _expanseCheck = false;
        private bool _isRecordNameEmpty;
        private bool _isRecordAmountEmpty;
        private RecordServices _recordServices = new RecordServices();
        private bool _isCreateRecord = false;
        private string _errorText;
        PopupEditRecordPage _popupEditRecordPage;
        private string _accountNameTemp;
        private bool _isEditable;
        private bool _isEditButtonVisible;

        public bool IsEditButtonVisible
        {
            get => _isEditButtonVisible;
            set
            {
                SetProperty(ref _isEditButtonVisible, value);
            }
        }

        public bool IsEditable
        {
            get => _isEditable;
            set
            {
                SetProperty(ref _isEditable, value);
            }
        }
        public string AccountNameTemp
        {
            get => _accountNameTemp;
            set
            {
                SetProperty(ref _accountNameTemp, value);
            }
        }
        public string ErrorText
        {
            get => _errorText;
            set
            {
                SetProperty(ref _errorText, value);
            }
        }

        public bool IsRecordAmountEmpty
        {
            get => _isRecordAmountEmpty;
            set
            {
                SetProperty(ref _isRecordAmountEmpty, value);
            }
        }

        public bool IsRecordNameEmpty
        {
            get => _isRecordNameEmpty;
            set => SetProperty(ref _isRecordNameEmpty, value);
        }

        public DelegateCommand NextCommand { get; }
        public DelegateCommand DeleteCommand { get; }
        public DelegateCommand ChangeEditableCommand { get; }

        public bool ExpanseCheck
        {
            get => _expanseCheck;
            set
            {
                SetProperty(ref _expanseCheck, value);
            }
        }

        public bool IncomeCheck
        {
            get => _incomeCheck;
            set
            {
                SetProperty(ref _incomeCheck, value);
            }
        }

        public Record Record
        {
            get => _record;
            set
            {
                SetProperty(ref _record, value);
            }
        }


        public PopupEditRecordViewModel(INavigation navigation, Record record, PopupEditRecordPage popupEditRecordPage)
        {
            _navigation = navigation;
            _record = record;
            _popupEditRecordPage = popupEditRecordPage;
            AccountNameTemp = "Record in Account " + DataViewAndViewModel.SelectedAccountDropdown.AccountName;


            IsEditable = DataViewAndViewModel.ButtonEditRecordPopup;
            IsEditButtonVisible = !DataViewAndViewModel.ButtonEditRecordPopup;


            if (_record.RecordCategory.CategoryType)
                ExpanseCheck = true;
            else
                IncomeCheck = true;
            NextCommand = new DelegateCommand(NextScreen);
            DeleteCommand = new DelegateCommand(ExecuteDeleteRecord);
            ChangeEditableCommand = new DelegateCommand(ExecuteChangeEditable);




        }

        private void ExecuteChangeEditable()
        {
            IsEditable = true;
            DataViewAndViewModel.ButtonEditRecordPopup = true;
            IsEditButtonVisible = false;
        }

        private void ExecuteDeleteRecord()
        {


            OnAlertYesNoClicked();
        }

        private void NextScreen()
        {
            IsRecordNameEmpty = string.IsNullOrEmpty(Record.RecordName);
            IsRecordAmountEmpty = Record.RecordAmount.Equals(0);



            CheckValidation();
            if (!IsRecordNameEmpty && !IsRecordAmountEmpty)
            {


                if (CheckValidation())
                {
                    Debug.Write(Record.RecordCategory.CategoryType);
                    _navigation.RemovePopupPageAsync(_popupEditRecordPage);
                    _navigation.PushPopupAsync(new PopupChooseCategoryPage(_record, _isCreateRecord));
                }


            }
            //if(_expanseCheck)
            //{
            //    Record.RecordCategory.CategoryType = true;
            //}
            //if(_incomeCheck)
            //{
            //    Record.RecordCategory.CategoryType = false;
            //}


            //IsRecordNameEmpty = string.IsNullOrEmpty(Record.RecordName);
            //IsRecordAmountEmpty = Record.RecordAmount.Equals(0);
            //if (!IsRecordNameEmpty && !IsRecordAmountEmpty)
            //{
            //    _navigation.PushAsync(new ChooseCategoryPage(_record, _isCreateRecord));
            //}
            //if (IsRecordNameEmpty)
            //    CrossToastPopUp.Current.ShowToastMessage("Name is empty");
            //if (IsRecordAmountEmpty)
            //    CrossToastPopUp.Current.ShowToastMessage("Amount is zero");
            //if (IsRecordAmountEmpty && IsRecordNameEmpty)
            //    CrossToastPopUp.Current.ShowToastMessage("Please enter fully");



        }

        private async Task ProcessDeleteRecordAsync()
        {


            var resultAsync = await _recordServices.DeleteRecordAsync(_record);
            TokenServices.CheckToken(resultAsync.StatusCode, _navigation);
            if (resultAsync.Status)
            {
                CrossToastPopUp.Current.ShowToastSuccess(resultAsync.Message);
                await _navigation.RemovePopupPageAsync(_popupEditRecordPage);
            }
            else
            {
                CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
            }
        }

        private async void OnAlertYesNoClicked()
        {
            var answer = await App.Current.MainPage.DisplayAlert(
                "Warning", "Are you sure to delete?",
                "OK",
                "Cancel");
            if (answer)
            {
                await ProcessDeleteRecordAsync();
                DataViewAndViewModel.ButtonEditRecordPopup = false;
                DataViewAndViewModel.HomeViewModel.RefeshRecordList();
                DataViewAndViewModel.HomeViewModel.RefreshAccount();
                DataViewAndViewModel.HomeViewModel.GetTotalAccountRecordAPI();
            }

        }
        private bool CheckValidation()
        {
            ArrayList errorList = new ArrayList();

            IsRecordNameEmpty = string.IsNullOrEmpty(Record.RecordName);

            string recordName = Record.RecordName;

            if (IsRecordNameEmpty)
            {
                ErrorText = "This field is required";
                errorList.Add(this);
            }
            else
            {
                if (string.IsNullOrWhiteSpace(recordName) || ValidationClass.StartSpaces(recordName) || ValidationClass.EndSpaces(recordName))
                {
                    IsRecordNameEmpty = true;
                    ErrorText = "Not allow spaces characters";
                    errorList.Add(this);
                }
                else
                {
                    if (ValidationClass.MinimumString(recordName, 4))
                    {
                        IsRecordNameEmpty = true;
                        ErrorText = "Please input more than 4 characters";
                        errorList.Add(this);
                    }
                    else
                    {
                        if (ValidationClass.IsHasSpecialCharacters(recordName))
                        {
                            IsRecordNameEmpty = true;
                            ErrorText = "Not allow special characters";
                            errorList.Add(this);
                        }
                    }
                }
            }

            if (errorList.Count > 0)
            {
                return false;
            }
            return true;
        }

    }
}
