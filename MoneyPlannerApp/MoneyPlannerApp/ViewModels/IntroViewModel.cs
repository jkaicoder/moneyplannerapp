﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Xamarin.Forms;

namespace MoneyPlannerApp.ViewModels
{
    public class IntroViewModel : ViewModelBase
    {
        private readonly INavigation _navigation;

        public DelegateCommand OpenSignInCommand { get; }
        public DelegateCommand OpenSignUpCommand { get; }

        public IntroViewModel(INavigation navigation)
        {
            _navigation = navigation;
            OpenSignInCommand = new DelegateCommand(OpenSignIn);
            OpenSignUpCommand = new DelegateCommand(OpenSignUp);

            
        }

        private void OpenSignUp()
        {
            _navigation.PushAsync(new SignUpPage());
        }

        private void OpenSignIn()
        {
            _navigation.PushAsync(new SignInPage());
        }

        
    }
}
