﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Plugin.Toast;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace MoneyPlannerApp.ViewModels
{
    public class AccountDetailViewModel : ViewModelBase
    {
        private INavigation _navigation;
        private AccountServices _accountServices;
        private Account _account;
        private HomeViewModel _homeViewModel;
        private AccountDetailPage _accountDetailPage;

        public Account Account
        {
            get => _account;
            set => SetProperty(ref _account, value);
        }

        public DelegateCommand AccountUserListCommand { get; }
        public DelegateCommand AccountRecordListCommand { get; }
        public DelegateCommand AccountSettingsCommand { get; }
        public DelegateCommand AccountInviteMemberCommand { get; }

        public AccountDetailViewModel(AccountDetailPage accountDetailPage, HomeViewModel homeViewModel, INavigation navigation,
            Account account)
        {
            _navigation = navigation;
            _homeViewModel = homeViewModel;
            _accountDetailPage = accountDetailPage;
            _accountServices = new AccountServices();
            _account = account;

            AccountUserListCommand = new DelegateCommand(AccountUserList);
            AccountRecordListCommand = new DelegateCommand(AccountRecordList);
            AccountSettingsCommand = new DelegateCommand(AccontSettings);
            AccountInviteMemberCommand = new DelegateCommand(AccountInviteMember);

            Debug.Write(_account.AccountUserList.First().UserID);
        }

        private void AccountInviteMember()
        {
            _navigation.PushAsync(new AccountInviteMemberPage(Account));
        }


        private void AccontSettings()
        {
            _navigation.PushPopupAsync(new PopupSettingsAccountPage(_accountDetailPage, _homeViewModel, _account));
        }

        private void AccountRecordList()
        {
            _navigation.PushPopupAsync(new PopupAccountRecordListPage(_account));
        }

        private void AccountUserList()
        {
            GetAccountForID();
            _navigation.PushPopupAsync(new PopupAccountUserListPage(_account));
        }

        private async void GetAccountForID()
        {
            if (ValidationClass.CheckInternet())
            {
                var resultAsync = await _accountServices.GetAccountForID(_account.AccountID);
                TokenServices.CheckToken(resultAsync.StatusCode, _navigation);
                if (resultAsync.Status)
                {
                    Account.AccountUserList = resultAsync.Data.AccountUserList;

                    for (int i = 0; i < resultAsync.Data.AccountUserList.Count; i++)
                    {
                        if (resultAsync.Data.AccountUserList[i].OwnerAccount == 1)
                        {
                            resultAsync.Data.AccountUserList[i].AccountLevel = "Owner";
                        }
                        else if (resultAsync.Data.AccountUserList[i].OwnerAccount == 2)
                        {
                            resultAsync.Data.AccountUserList[i].AccountLevel = "Admin";
                        }
                        else
                        {
                            resultAsync.Data.AccountUserList[i].AccountLevel = "Member";
                        }
                        resultAsync.Data.AccountUserList[i].UserImage = Configuration.ID_HOST + resultAsync.Data.AccountUserList[i].UserImage;
                    }
                }
                else
                {
                    CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                }
            }
        }
    }
}
