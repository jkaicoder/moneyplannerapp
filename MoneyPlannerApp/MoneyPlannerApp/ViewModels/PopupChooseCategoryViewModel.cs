﻿using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Rg.Plugins.Popup.Extensions;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using Plugin.Toast;
using Xamarin.Forms;

namespace MoneyPlannerApp.ViewModels
{
    class PopupChooseCategoryViewModel : ViewModelBase
    {
        private const string RED_COLOR = "#0FCB58";
        private const string GREEN_COLOR = "#FF1B4A";
        private CategoryServices _categoryServices;
        private readonly INavigation _navigation;
        private Category _categorySelected;
        private Record _record;

        PopupChooseCategoryPage _popupChooseCategoryPage;

        private ObservableCollection<Category> _categoryList;
        private ObservableCollection<Category> _categoryListIncome;
        private ObservableCollection<Category> _categoryListExpense;

        public DelegateCommand AddCategoryCommand { get; }
        private bool _isCreateRecord;

        private string _searchText;
        private ObservableCollection<Category> _categoryListTemp;

        public Record Record
        {
            get => _record;
            set
            {
                SetProperty(ref _record, value);
            }
        }
        public ObservableCollection<Category> CategoryListTemp
        {
            get => _categoryListTemp;
            set
            {
                SetProperty(ref _categoryListTemp, value);
            }
        }
        public ObservableCollection<Category> CategoryList
        {
            get => _categoryList;
            set
            {
                SetProperty(ref _categoryList, value);
            }
        }

        public string SearchText
        {
            get => _searchText;
            set
            {
                SetProperty(ref _searchText, value);
                FilterListview();

            }
        }


        private void FilterListview()
        {

            //CategoryList = new ObservableCollection<Category>(CategoryDAO.GetCategoryList(!_record.RecordCategory.CategoryType));

            //GetCategoryList();
            CategoryList = CategoryListTemp;

            if (CategoryList != null && CategoryList.Count > 0)
            {
                ObservableCollection<Category> searchListExpense = new ObservableCollection<Category>();

                var tempRecordsExpense = CategoryList.Where(x => x.CategoryName.ToLower().Contains(SearchText.ToLower()) || x.CategoryID.ToString().Contains(SearchText));

                foreach (Category item in tempRecordsExpense)
                {
                    searchListExpense.Add(item);
                }
                CategoryList = searchListExpense;
            }
        }



        public PopupChooseCategoryViewModel(Record record, INavigation navigation, bool isCreateRecord, PopupChooseCategoryPage popupChooseCategoryPage)
        {



            _categoryServices = new CategoryServices();
            _categoryListIncome = new ObservableCollection<Category>();
            _categoryListExpense = new ObservableCollection<Category>();
            _navigation = navigation;
            _isCreateRecord = isCreateRecord;
            _popupChooseCategoryPage = popupChooseCategoryPage;

            AddCategoryCommand = new DelegateCommand(NextScreen);
            Debug.Write(record.RecordCategory.CategoryType);

            Record = record;

            //if (record.RecordCategory.CategoryType)
            //    CategoryList = new ObservableCollection<Category>(CategoryDAO.GetCategoryList(true));
            //else
            //    CategoryList = new ObservableCollection<Category>(CategoryDAO.GetCategoryList(false));

            GetCategoryList();



        }

        public async void GetCategoryList()
        {
            if (ValidationClass.CheckInternet())
            {
                var resultAsync = await _categoryServices.GetCategoryList();
                TokenServices.CheckToken(resultAsync.StatusCode, _navigation);
                if (resultAsync.Status)
                {

                    _categoryListIncome.Clear();
                    _categoryListExpense.Clear();

                    foreach (var item in resultAsync.Data)
                    {
                        if (item.CategoryType)
                        {
                            _categoryListExpense.Add(item);
                        }
                        else
                        {
                            _categoryListIncome.Add(item);
                        }
                    }

                    if (_record.RecordCategory.CategoryType)
                    {
                        CategoryList = _categoryListExpense;
                        CategoryListTemp = CategoryList;
                    }
                    else
                    {
                        CategoryList = _categoryListIncome;
                        CategoryListTemp = CategoryList;
                    }
                }
                else
                {
                    CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                }
            }
            
        }

        private void NextScreen()
        {
            //_navigation.PushAsync(new AddCategoryPage());
            //_navigation.PushPopupAsync(new PopupAddCategoryPage());
            Debug.Write(_record.RecordCategory.CategoryType);
        }

        public Category CategorySelected
        {
            get => _categorySelected;
            set
            {
                SetProperty(ref _categorySelected, value);
                if (ValidationClass.CheckInternet())
                {
                    if (_record.RecordCategory.CategoryType)
                    {
                        _record.RecordTypeColor = RED_COLOR;
                    }
                    else
                    {
                        _record.RecordTypeColor = GREEN_COLOR;
                    }
                    _record.RecordCategory = _categorySelected;
                    _navigation.RemovePopupPageAsync(_popupChooseCategoryPage);
                    _navigation.PushPopupAsync(new PopupSummaryPage(_record, _isCreateRecord));
                    Debug.Write(_record.RecordCategory.CategoryType);
                }
            }
        }

    }

}
