﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Plugin.Toast;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace MoneyPlannerApp.ViewModels
{
    public class PopupAccountUserListViewModel : ViewModelBase
    {
        private INavigation _navigation;
        private Account _account;
        private User _userSelectedInvite;
        private AccountServices _accountServices;
        private UserServices _userServices;
        private ObservableCollection<User> _userListInvite;
        private User _userSelectedAccount;
        private string _searchTextUserInvite;
        private UserToken _userToken;
        private User _userCurrent;
        private bool _isVisibleLeaveAccountButton = false;

        public DelegateCommand LeaveAccountCommand { get; }
        public TapGestureRecognizer AddUserToAccountCommand { get; }

        public bool IsVisibleLeaveAccountButton
        {
            get => _isVisibleLeaveAccountButton;
            set => SetProperty(ref _isVisibleLeaveAccountButton, value);
        }

        public PopupAccountUserListViewModel(INavigation navigation, Account account)
        {
            DataViewAndViewModel.PopupAccountUserListViewModel = this;
            _navigation = navigation;
            _account = account;
            _accountServices = new AccountServices();
            _userServices = new UserServices();
            _userCurrent = new User();
            _userToken = UserDAO.GetUserToken().Last();
            GetUserListInviteApi();
            GetUserCurrent();
            
            LeaveAccountCommand = new DelegateCommand(LeaveAccount);
            AddUserToAccountCommand = new TapGestureRecognizer();
        }
        private async void LeaveAccount()
        {
            if (ValidationClass.CheckInternet())
            {
                var resultAsync = await _accountServices.LeaveAccount(new InfoID() { AccountID = _account.AccountID, UserIDRemove = _userCurrent.UserID });
                TokenServices.CheckToken(resultAsync.StatusCode, _navigation);
                if (resultAsync.Status)
                {
                    CrossToastPopUp.Current.ShowToastSuccess(resultAsync.Message);
                    _navigation.RemovePage(DataViewAndViewModel.AccountDetailPage);
                    await _navigation.RemovePopupPageAsync(DataViewAndViewModel.PopupAccountUserListPage);
                    DataViewAndViewModel.HomeViewModel.Refresh();
                }
                else
                {
                    CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                }
            }
        }

        private async void GetUserCurrent()
        {
            if (ValidationClass.CheckInternet())
            {
                var resultAsync = await _userServices.GetInfoUserCurrent(_userToken.UserID);
                TokenServices.CheckToken(resultAsync.StatusCode, _navigation, DataViewAndViewModel.PopupAccountUserListPage);
                if (resultAsync.Status)
                {
                    _userCurrent = resultAsync.Data;

                    if (!IsOwnerAccount())
                    {
                        IsVisibleLeaveAccountButton = true;
                    }
                }
                else
                {
                    CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                }
            }
        }

        public void Refresh()
        {
            GetUserListInviteApi();
            GetAccountForID();
        }

        private async void GetAccountForID()
        {
            if (ValidationClass.CheckInternet())
            {
                var resultAsync = await _accountServices.GetAccountForID(_account.AccountID);
                TokenServices.CheckToken(resultAsync.StatusCode, _navigation);
                if (resultAsync.Status)
                {
                    Account.AccountUserList = resultAsync.Data.AccountUserList;

                    for (int i = 0; i < resultAsync.Data.AccountUserList.Count; i++)
                    {
                        if (resultAsync.Data.AccountUserList[i].OwnerAccount == 1)
                        {
                            resultAsync.Data.AccountUserList[i].AccountLevel = "Owner";
                        }
                        else if (resultAsync.Data.AccountUserList[i].OwnerAccount == 2)
                        {
                            resultAsync.Data.AccountUserList[i].AccountLevel = "Admin";
                        }
                        else
                        {
                            resultAsync.Data.AccountUserList[i].AccountLevel = "Member";
                        }
                        resultAsync.Data.AccountUserList[i].UserImage = Configuration.ID_HOST + resultAsync.Data.AccountUserList[i].UserImage;
                    }
                }
                else
                {
                    CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                }
            }
        }

        private async void GetUserListInviteApi()
        {
            if (ValidationClass.CheckInternet())
            {
                var resultAsync = await _userServices.GetUserListInvite(_account.AccountID);
                if (resultAsync.Status)
                {
                    for (int i = 0; i < resultAsync.Data.Count; i++)
                    {
                        resultAsync.Data[i].UserImage = Configuration.ID_HOST + resultAsync.Data[i].UserImage;
                    }
                    UserListInvite = resultAsync.Data;
                }
                else
                {
                    CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                }
            }
        }

        public User UserCurrent
        {
            get => _userCurrent;
            set => SetProperty(ref _userCurrent, value);
        }

        public Account Account
        {
            get => _account;
            set => SetProperty(ref _account, value);
        }

        public ObservableCollection<User> UserListInvite
        {
            get => _userListInvite;
            set => SetProperty(ref _userListInvite, value);
        }

        public User UserSelectedInvite
        {
            get => _userSelectedInvite;
            set
            {
                SetProperty(ref _userSelectedInvite, value);
                if (_userSelectedInvite != null)
                {
                    //if (CheckUserRoleInAccount())
                    //{
                    //    AddUserToAccount(UserDAO.GetUserToken().Last().UserID, Account.AccountID, UserSelectedInvite.UserID);
                    //    UserListInvite.Remove(_userSelectedInvite);
                    //    Account.AccountUserList.Add(_userSelectedInvite);
                    //}
                    //else
                    //{
                    //    CrossToastPopUp.Current.ShowToastError("You do not have permission to add members");
                    //}
                    AddUserToAccount(Account.AccountID, UserDAO.GetUserToken().Last().UserID, UserSelectedInvite.UserID);
                    
                }
            }
        }

        public int GetRoleAccountUser(string userID)
        {
            foreach (User user in _account.AccountUserList)
            {
                if (user.UserID.Contains(userID))
                {
                    return user.OwnerAccount;
                }
            }
            return 3;
        }

        public bool CheckUserRoleInAccount()
        {
            foreach (User user in _account.AccountUserList)
            {
                if (user.UserID == _userCurrent.UserID && user.OwnerAccount == 1)
                {
                    return true;
                }
            }
            return false;
        }

        //public bool CheckUserRoleInAccount(string userID)
        //{
        //    foreach (User user in _account.AccountUserList)
        //    {
        //        if (user.UserID == _userCurrent.UserID)
        //        {
        //            if (expr)
        //            {
        //                return true;
        //            }
        //        }
        //    }
        //    return false;
        //}

        public User UserSelectedAccount
        {
            get => _userSelectedAccount;
            set
            {
                SetProperty(ref _userSelectedAccount, value);
                if (_userSelectedAccount != null)
                {
                    //if (_userSelectedAccount.OwnerAccount != 1)
                    //{
                    //    if (CheckUserRoleInAccount())
                    //    {
                    //        RemoveUserToAccount(UserDAO.GetUserToken().Last().UserID, Account.AccountID, UserSelectedAccount.UserID);
                    //        Account.AccountUserList.Remove(_userSelectedAccount);
                    //        UserListInvite.Add(_userSelectedAccount);
                    //    }
                    //    else
                    //    {
                    //        CrossToastPopUp.Current.ShowToastError("You do not have permission to remove this member !!!");
                    //    }
                    //}
                    //else
                    //{
                    //    CrossToastPopUp.Current.ShowToastError("Cannot delete Administrator of this Account !");
                    //}
                    SettingMemberAccount();
                }
            }
        }

        private void SettingMemberAccount()
        {
            if (UserSelectedAccount.OwnerAccount != 0 && UserSelectedAccount.OwnerAccount != 1)
            {
                if (CheckLevelCurrentUser())
                {
                    _navigation.PushPopupAsync(new PopupAccountUserLevelPage(Account, UserSelectedAccount));
                }
            }
        }

        private bool CheckLevelCurrentUser()
        {
            foreach (var userItem in Account.AccountUserList)
            {
                if (_userToken.UserID.Contains(userItem.UserID) )
                {
                    if (userItem.OwnerAccount == 0 || userItem.OwnerAccount == 1)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsOwnerAccount()
        {
            foreach (var userItem in Account.AccountUserList)
            {
                if (_userToken.UserID.Contains(userItem.UserID))
                {
                    if (userItem.OwnerAccount == 1)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private async void AddUserToAccount(string accountID, string userID, string userIdAdd)
        {
            if (ValidationClass.CheckInternet())
            {
                var page = new PopupPage();
                await _navigation.PushPopupAsync(page);

                var resultAsync = await _accountServices.AddUserToAccount(new InfoID()
                {
                    UserID = userID,
                    AccountID = accountID,
                    UserIDAdd = userIdAdd,
                    Role = 3
                });
                await _navigation.RemovePopupPageAsync(page);
                TokenServices.CheckToken(resultAsync.StatusCode, _navigation);
                if (resultAsync.Status)
                {
                    UserSelectedInvite.AccountLevel = "Member";

                    //for (int i = 0; i < Account.AccountUserList.Count; i++)
                    //{
                    //    if (Account.AccountUserList[i].OwerAccount == 1)
                    //    {
                    //        Account.AccountUserList[i].AccountLevel = "Admin";
                    //    }
                    //    else
                    //    {
                    //        Account.AccountUserList[i].AccountLevel = "Member";
                    //    }
                    //}
                    _navigation.PushPopupAsync(new PopupAlertSuccessPage());
                }
                else
                {
                    CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                }
            }
        }

        //public string SearchTextUserInvite
        //{
        //    get => _searchTextUserInvite;
        //    set
        //    {
        //        SetProperty(ref _searchTextUserInvite, value);
        //        SearchCommandExecute();
        //    } 
        //}

        //private void SearchCommandExecute()
        //{
        //    GetUserListInviteApi();
        //    ObservableCollection<User> userListInviteTemp = new ObservableCollection<User>();
        //    userListInviteTemp = UserListInvite;

        //    if (UserListInvite != null && UserListInvite.Count > 0)
        //    {
        //        ObservableCollection<User> searchUserListInvite = new ObservableCollection<User>();

        //        var tempUserInvete = UserListInvite.Where(x => x.UserName.ToLower().Contains(SearchTextUserInvite.ToLower()) || x.Email.ToString().Contains(SearchTextUserInvite));

        //        foreach (User item in tempUserInvete)
        //        {
        //            searchUserListInvite.Add(item);
        //        }
        //        UserListInvite = searchUserListInvite;
        //    }
        //}
    }
}
