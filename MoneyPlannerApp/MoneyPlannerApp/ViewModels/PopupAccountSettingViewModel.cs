﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Plugin.Toast;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace MoneyPlannerApp.ViewModels
{
    public class PopupAccountSettingViewModel : ViewModelBase
    {
        private INavigation _navigation;
        private AccountServices _accountServices;
        private Account _account;
        private UserToken _userToken;

        public DelegateCommand DeleteAccountCommand { get; }
        public DelegateCommand ResetAccountCommand { get; }

        public PopupAccountSettingViewModel(INavigation navigation, Account account)
        {
            _navigation = navigation;
            _accountServices = new AccountServices();
            _account = account;
            _userToken = UserDAO.GetUserToken().Last();

            DeleteAccountCommand = new DelegateCommand(DeleteAccount);  
            ResetAccountCommand = new DelegateCommand(ResetAccount);
        }

        private async void ResetAccount()
        {
            var answer = await App.Current.MainPage.DisplayAlert(
                "Warning [Reset]", "All data about money, records, members will be deleted. Are you sure to reset?",
                "OK",
                "Cancel");
            if (answer)
            {
                if (ValidationClass.CheckInternet())
                {
                    var resultAsync = await _accountServices.ResetAccount(new InfoID()
                        { AccountID = _account.AccountID, UserID = _userToken.UserID });
                    TokenServices.CheckToken(resultAsync.StatusCode, _navigation,
                        DataViewAndViewModel.PopupSettingsAccountPage);
                    if (resultAsync.Status)
                    {
                        CrossToastPopUp.Current.ShowToastSuccess(resultAsync.Message);
                        DataViewAndViewModel.HomeViewModel.Refresh();
                        await _navigation.RemovePopupPageAsync(DataViewAndViewModel.PopupSettingsAccountPage);
                    }
                    else
                    {
                        CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                    }
                }
            }
        }

        private async void DeleteAccount()
        {
            var answer = await App.Current.MainPage.DisplayAlert(
                "Warning [Delete]", "All data about money, records, members will be deleted. Are you sure to delete?",
                "OK",
                "Cancel");
            if (answer)
            {
                if (ValidationClass.CheckInternet())
                {
                    var resultAsync = await _accountServices.DeleteAccount(new InfoID() { AccountID = _account.AccountID, UserID = _userToken.UserID });
                    TokenServices.CheckToken(resultAsync.StatusCode, _navigation, DataViewAndViewModel.PopupSettingsAccountPage);
                    if (resultAsync.Status)
                    {
                        CrossToastPopUp.Current.ShowToastSuccess(resultAsync.Message);
                        _navigation.RemovePage(DataViewAndViewModel.AccountDetailPage);
                        await _navigation.RemovePopupPageAsync(DataViewAndViewModel.PopupSettingsAccountPage);
                        DataViewAndViewModel.HomeViewModel.Refresh();
                    }
                    else
                    {
                        CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                    }
                }
            }
        }
    }
}
