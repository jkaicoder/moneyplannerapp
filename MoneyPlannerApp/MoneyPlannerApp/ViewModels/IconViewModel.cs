﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace MoneyPlannerApp.Models
{
    public class IconRepository : ViewModelBase
    {
        private readonly INavigation _navigation;
        private ObservableCollection<Icon> _icon;
        private Icon _iconSelectedItem;
        private Category _category = new Category();
        private bool _isEditCategory = false;

        private Account _account;

        private int _kiemtra = -1;


        public ObservableCollection<Icon> IconInfo
        {
            get => _icon;
            set => this._icon = value;
        }

        public IconRepository(INavigation navigation, Category category)
        {
            _navigation = navigation;
            GenerateIcon();
            _category = category;
        }

        public IconRepository(INavigation navigation, Category category, bool isEditCategory)
        {
            _navigation = navigation;
            GenerateIcon();
            _category = category;
            _isEditCategory = isEditCategory;
        }

        public IconRepository (INavigation navigation, Account account, int kiemtra)
        {
            _navigation = navigation;
            GenerateIcon();
            _account = account;
            _kiemtra = kiemtra;

        }

        internal void GenerateIcon()
        {
            _icon = new ObservableCollection<Icon>();
            _icon.Add(new Icon() { IconURL = "carwash.png" });
            _icon.Add(new Icon() { IconURL = "cosmetics.png" });
            _icon.Add(new Icon() { IconURL = "fuel.png" });
            _icon.Add(new Icon() { IconURL = "headband.png" });
            _icon.Add(new Icon() { IconURL = "lift.png" });
            _icon.Add(new Icon() { IconURL = "park.png" });
            _icon.Add(new Icon() { IconURL = "shoes.png" });
            _icon.Add(new Icon() { IconURL = "spotify.png" });
            _icon.Add(new Icon() { IconURL = "support.png" });
            _icon.Add(new Icon() { IconURL = "taxi.png" });
            _icon.Add(new Icon() { IconURL = "travel.png" });
            _icon.Add(new Icon() { IconURL = "tshirt.png" });
            _icon.Add(new Icon() { IconURL = "video.png" });
            _icon.Add(new Icon() { IconURL = "groceries.png" });
            _icon.Add(new Icon() { IconURL = "social.png" });
            _icon.Add(new Icon() { IconURL = "coffin.png" });
            _icon.Add(new Icon() { IconURL = "coffee.png" });
            _icon.Add(new Icon() { IconURL = "savings.png" });
            _icon.Add(new Icon() { IconURL = "restaurant.png" });
            _icon.Add(new Icon() { IconURL = "present.png" });
            _icon.Add(new Icon() { IconURL = "tea.png" });
            _icon.Add(new Icon() { IconURL = "football.png" });
            _icon.Add(new Icon() { IconURL = "doctor.png" });
            _icon.Add(new Icon() { IconURL = "couple.png" });
        }

        public Icon IconSelectedItem
        {
            get => _iconSelectedItem;
            set
            {
                if (_iconSelectedItem != value)
                {
                    _iconSelectedItem = value;
                    _category.CategoryIcon = _iconSelectedItem.IconURL;

                    if (_isEditCategory)
                    {
                        _navigation.PopAsync(true);
                        _navigation.PushPopupAsync(new PopupEditCategoryPage(_category));
                        
                    }
                    else if (_kiemtra != -1)
                    {
                        SetIconForAccount();
                        _navigation.PopAsync(true);
                        _navigation.PushPopupAsync(new PopupAddAccountPage(_account));
                    }
                    else
                    {
                        _navigation.PopAsync(true);
                        _navigation.PushPopupAsync(new PopupAddCategoryPage(_category));
                    }
                    
                }
            }
        }

        public void SetIconForAccount()
        {
            _account.AccountIcon = _iconSelectedItem.IconURL;
        }
    }
}
