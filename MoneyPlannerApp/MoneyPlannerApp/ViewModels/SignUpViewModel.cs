﻿using System;
using System.Collections;
using System.Diagnostics;
using ImageCircle.Forms.Plugin.Abstractions;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Toast;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using System.ComponentModel.DataAnnotations;

namespace MoneyPlannerApp.ViewModels
{
    public class SignUpViewModel : ViewModelBase
    {
        public enum GenderList
        {
            Unknown,
            Male,
            Female
        }
        private readonly INavigation _navigation;
        private readonly UserServices _userServices;

        public DelegateCommand SignUpCommand { get; }
        public DelegateCommand SelectAvatarCommand { get; }

        private User _user;
        private ImageSource imgSource = null;
        private CircleImage _avatar;

        private bool _isUserNameError;
        private bool _isPasswordError;
        private bool _isConfirmPasswordError;
        private bool _isEmailError;
        private string _userNameErrorText;
        private string _passwordErrorText;
        private string _confirmPasswordErrorText;
        private string _emailErrorText;
        private bool _acceptPolicy;
        private string _confirmPassword;
        private string _colorChecked = "#878787";
        private string newPasswordTemp;

        public User User
        {
            get => _user;
            set
            {
                SetProperty(ref _user, value);
            }
        }

        public string ConfirmPassword
        {
            get => _confirmPassword;
            set => SetProperty(ref _confirmPassword, value);
        }

        public bool IsUserNameError
        {
            get => _isUserNameError;
            set => SetProperty(ref _isUserNameError, value);
        }

        public bool IsPasswordError
        {
            get => _isPasswordError;
            set => SetProperty(ref _isPasswordError, value);
        }

        public bool IsConfirmPasswordError
        {
            get => _isConfirmPasswordError;
            set => SetProperty(ref _isConfirmPasswordError, value);
        }

        public bool IsEmailError
        {
            get => _isEmailError;
            set => SetProperty(ref _isEmailError, value);
        }

        public string UserNameErrorText
        {
            get => _userNameErrorText;
            set => SetProperty(ref _userNameErrorText, value);
        }

        public string PasswordErrorText
        {
            get => _passwordErrorText;
            set => SetProperty(ref _passwordErrorText, value);
        }

        public string ConfirmPasswordErrorText
        {
            get => _confirmPasswordErrorText;
            set => SetProperty(ref _confirmPasswordErrorText, value);
        }

        public string EmailErrorText
        {
            get => _emailErrorText;
            set => SetProperty(ref _emailErrorText, value);
        }

        public bool AcceptPolicy
        {
            get => _acceptPolicy;
            set => SetProperty(ref _acceptPolicy, value);
        }

        public string ColorChecked
        {
            get => _colorChecked;
            set => SetProperty(ref _colorChecked, value);
        }

        public SignUpViewModel(INavigation navigation)
        {
            _navigation = navigation;
            _userServices = new UserServices();

            SignUpCommand = new DelegateCommand(SignUp);
            //SelectAvatarCommand = new DelegateCommand(SelectAvatarAsync);

            _user = new User();
        }

        private bool CheckValidation()
        {
            ArrayList errorList = new ArrayList();

            string userName = User.UserName;
            string passWord = User.Password;
            string confirmPassword = ConfirmPassword;
            string email = User.Email;


            //Validation Username

            if (ValidationClass.IsNullOrEmpty(userName))
            {
                IsUserNameError = true;
                UserNameErrorText = "This field is required";
                errorList.Add(this);
            }
            else
            {
                IsUserNameError = false;
                if (ValidationClass.MinimumString(userName, 6))
                {
                    IsUserNameError = true;
                    UserNameErrorText = "Please input more than 6 characters";
                    errorList.Add(this);
                }
                else
                {
                    IsUserNameError = false;
                    if (ValidationClass.IsHasSpecialCharacters(userName))
                    {
                        IsUserNameError = true;
                        UserNameErrorText = "Not allow special characters";
                        errorList.Add(this);
                    }
                    else
                    {
                        IsUserNameError = false;
                        if (ValidationClass.IsUppercase(userName))
                        {
                            IsUserNameError = true;
                            UserNameErrorText = "Not allow uppercase characters";
                            errorList.Add(this);
                        }
                    }
                }
            }

            //Validation Password

            if (ValidationClass.IsNullOrEmpty(passWord))
            {
                IsPasswordError = true;
                PasswordErrorText = "This field is required";
                errorList.Add(this);
            }
            else
            {
                IsPasswordError = false;
                if (ValidationClass.MinimumString(passWord, 6))
                {
                    IsPasswordError = true;
                    PasswordErrorText = "Please input more than 6 characters";
                    errorList.Add(this);
                }
                else
                {
                    IsPasswordError = false;
                }
            }

            //Validation Confirm Password
            if (ValidationClass.IsNullOrEmpty(_confirmPassword))
            {
                IsConfirmPasswordError = true;
                ConfirmPasswordErrorText = "This field is required";
                errorList.Add(this);
            }
            else
            {
                IsConfirmPasswordError = false;
                if (!String.Equals(ConfirmPassword, passWord))
                {
                    IsConfirmPasswordError = true;
                    ConfirmPasswordErrorText = "Password does not match";
                    errorList.Add(this);
                }
                else
                {
                    IsConfirmPasswordError = false;
                }
            }


            //Validation Email

            if (ValidationClass.IsNullOrEmpty(email))
            {
                IsEmailError = true;
                EmailErrorText = "This field is required";
                errorList.Add(this);
            }
            else
            {
                IsEmailError = false;
                if (!ValidationClass.IsValidEmail(email))
                {
                    IsEmailError = true;
                    EmailErrorText = "The format of Email is incorrect";
                    errorList.Add(this);
                }
                else
                {
                    IsEmailError = false;
                }
            }

            //Validation Checked

            if (!AcceptPolicy)
            {
                ColorChecked = "#BA2C44";
                CrossToastPopUp.Current.ShowToastError("You must accept the policy and terms");
                errorList.Add(this);
            }
            else
            {
                ColorChecked = "#878787";
            }

            if (errorList.Count > 0)
            {
                return false;
            }

            return true;
        }



        private void DataProcess()
        {
            newPasswordTemp = _user.Password;
            _user.UserName = _user.UserName.Trim();
            _user.Password = SecurePasswordHasher.Hash(_user.Password);
            _user.CreatedDate = DateTime.Now;
            //_user.BirthDay = DateTime.Now;
            //code of Han
            _user.Gender = (int)GenderList.Unknown;
            _user.Address = string.Empty;
            _user.FirstName = string.Empty;
            _user.LastName = string.Empty;
            _user.Job = string.Empty;
            _user.PhoneNumber = string.Empty;

        }

        private async void SignUp()
        {

            if (ValidationClass.CheckInternet())
            {

                if (CheckValidation())
                {
                    DataProcess();

                    var pageLoad = new PopupPage();
                    await _navigation.PushPopupAsync(pageLoad);
                    var resultAsync = await _userServices.SignUpAsync(User);
                    await _navigation.RemovePopupPageAsync(pageLoad);

                    if (resultAsync.Status)
                    {
                        CrossToastPopUp.Current.ShowToastSuccess(resultAsync.Message);
                        _navigation.RemovePage(DataViewAndViewModel.SignUpPage);
                        await _navigation.PushAsync(new SignInPage());
                    }
                    else
                    {
                        _user.Password = newPasswordTemp;

                        CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                    }
                }
            }
        }

        //private async void SelectAvatarAsync()
        //{
        //    await CrossMedia.Current.Initialize();
        //    if (!CrossMedia.Current.IsPickPhotoSupported)
        //    {
        //        CrossToastPopUp.Current.ShowToastError("Your device does not currently support this functionality");
        //        return;
        //    }

        //    var mediaOptions = new PickMediaOptions()
        //    {
        //        PhotoSize = PhotoSize.Large
        //    };

        //    var selectedImageFile = await CrossMedia.Current.PickPhotoAsync(mediaOptions);

        //    if (Avatar.Source == null)
        //    {
        //        CrossToastPopUp.Current.ShowToastError("Could not get the image, please try again");
        //        return;
        //    }

        //    Avatar.Source = ImageSource.FromStream(() => selectedImageFile.GetStream());


        //    if (selectedImageFile != null)
        //    {
        //        imgSource = Avatar.Source;
        //        DataViewAndViewModel.Avatar = imgSource;
        //    }
        //    else
        //    {
        //        if (imgSource != null)
        //        {
        //            Avatar.Source = imgSource;
        //            DataViewAndViewModel.Avatar = imgSource;
        //        }
        //        else
        //        {
        //            Avatar.Source = "ic_avatar";
        //            DataViewAndViewModel.Avatar = "ic_avatar";
        //        }
        //    }
        //}
    }
}
