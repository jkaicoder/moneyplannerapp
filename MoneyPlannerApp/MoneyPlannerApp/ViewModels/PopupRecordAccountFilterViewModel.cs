﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.ViewModels.Base;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace MoneyPlannerApp.ViewModels
{
    public class PopupRecordAccountFilterViewModel : ViewModelBase
    {
        private INavigation _navigation;
        private string _sortByValue;
        private bool _isCheckedAllRecordType;
        private bool _isCheckedIncomeRecordType;
        private bool _isCheckedExpenseRecordType;
        private bool _isCheckedAllTime;
        private bool _isCheckedTodayTime;
        private bool _isCheckedThisWeekTime;
        private bool _isCheckedThisMonthTime;
        private RecordFilter _recordFilter;
        private ObservableCollection<Record> _recordList;
        private List<string> _recordUserNameList;

        public DelegateCommand CloseRecordAccountFilterCommand { get; }
        public DelegateCommand RecordAccountFilterCommand { get; }

        public string SortByValue
        {
            get => _sortByValue;
            set => SetProperty(ref _sortByValue, value);
        }

        public bool IsCheckedAllRecordType
        {
            get => _isCheckedAllRecordType;
            set => SetProperty(ref _isCheckedAllRecordType, value);
        }

        public bool IsCheckedIncomeRecordType
        {
            get => _isCheckedIncomeRecordType;
            set => SetProperty(ref _isCheckedIncomeRecordType, value);
        }

        public bool IsCheckedExpenseRecordType
        {
            get => _isCheckedExpenseRecordType;
            set => SetProperty(ref _isCheckedExpenseRecordType, value);
        }

        public bool IsCheckedAllTime
        {
            get => _isCheckedAllTime;
            set => SetProperty(ref _isCheckedAllTime, value);
        }

        public bool IsCheckedTodayTime
        {
            get => _isCheckedTodayTime;
            set => SetProperty(ref _isCheckedTodayTime, value);
        }

        public bool IsCheckedThisWeekTime
        {
            get => _isCheckedThisWeekTime;
            set => SetProperty(ref _isCheckedThisWeekTime, value);
        }

        public bool IsCheckedThisMonthTime
        {
            get => _isCheckedThisMonthTime;
            set => SetProperty(ref _isCheckedThisMonthTime, value);
        }

        public RecordFilter RecordFilter
        {
            get => _recordFilter;
            set => SetProperty(ref _recordFilter, value);
        }

        public ObservableCollection<Record> RecordList
        {
            get => _recordList;
            set => SetProperty(ref _recordList, value);
        }

        public PopupRecordAccountFilterViewModel(INavigation navigation, ObservableCollection<Record> recordList)
        {
            _navigation = navigation;
            _recordFilter = new RecordFilter();
            _recordList = new ObservableCollection<Record>();
            RecordList = recordList;
            _recordUserNameList = new List<string>();
            GetRecordUserNameList();
            IsCheckedAllRecordType = true;
            IsCheckedAllTime = true;
            CloseRecordAccountFilterCommand = new DelegateCommand(CloseRecordAccountFilter);
            RecordAccountFilterCommand = new DelegateCommand(RecordAccountFilter);
        }

        public List<string> RecordUserNameList
        {
            get => _recordUserNameList;
            set => SetProperty(ref _recordUserNameList, value);
        }

        private void GetRecordUserNameList()
        {
            foreach (var recordItem in RecordList)
            {
                if (RecordUserNameList.Count > 0)
                {
                    if (!RecordUserNameList.Contains(recordItem.RecordUserName))
                    {
                        RecordUserNameList.Add(recordItem.RecordUserName);
                    }
                }
                else
                {
                    RecordUserNameList.Add(recordItem.RecordUserName);
                }
            }
        }

        private void RecordAccountFilter()
        {
            GetValueFilter();
            DataViewAndViewModel.RecordFilter = RecordFilter;
            DataViewAndViewModel.PopupAccountRecordListViewModel.RecordListFilter();
            _navigation.RemovePopupPageAsync(DataViewAndViewModel.PopupRecordAccountFilterPage);
        }

        private void CloseRecordAccountFilter()
        {
            _navigation.RemovePopupPageAsync(DataViewAndViewModel.PopupRecordAccountFilterPage);
        }

        private void GetValueFilter()
        {
            _recordFilter.SortByUser = SortByValue;

            if (IsCheckedAllRecordType)
            {
                _recordFilter.RecordType = "All";
            }
            else if (IsCheckedIncomeRecordType)
            {
                _recordFilter.RecordType = "Income";
            }
            else
            {
                _recordFilter.RecordType = "Expense";
            }

            if (IsCheckedAllTime)
            {
                _recordFilter.Time = "All";
            }
            else if (IsCheckedTodayTime)
            {
                _recordFilter.Time = "Today";
            }
            else if(IsCheckedThisWeekTime)
            {
                _recordFilter.Time = "Week";
            }
            else
            {
                _recordFilter.Time = "Month";
            }

        }
    }
}
