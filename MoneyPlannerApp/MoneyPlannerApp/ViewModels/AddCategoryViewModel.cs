﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Plugin.Toast;
using Xamarin.Forms;

namespace MoneyPlannerApp.ViewModels
{
    public class AddCategoryViewModel : ViewModelBase
    {
        private INavigation _navigation;

        public DelegateCommand SelectIconCommand { get; }
        public DelegateCommand AddCategoryCommand { get; }

        private Category _category;
        private bool _isCategoryNameEmpty;

        public Category Category
        {
            get => _category;
            set
            {
                SetProperty(ref _category, value);
                IsCategoryNameEmpty = true;
            }
        }

        public bool IsCategoryNameEmpty
        {
            get => _isCategoryNameEmpty;
            set => SetProperty(ref _isCategoryNameEmpty, value);
        }

        public AddCategoryViewModel(INavigation navigation)
        {
            _navigation = navigation;
            SelectIconCommand = new DelegateCommand(OpenSelectIcon);
            AddCategoryCommand = new DelegateCommand(AddCategory);
            _category = new Category();

        }

        private void AddCategory()
        {
            IsCategoryNameEmpty = string.IsNullOrEmpty(Category.CategoryName);

            if (!IsCategoryNameEmpty)
            {
                if (CategoryDAO.AddCategory(_category))
                {
                    CrossToastPopUp.Current.ShowToastSuccess("Delete successful");
                    _navigation.PushAsync(new CategoryListPage());
                }
            }
        }

        private void OpenSelectIcon()
        {
            _navigation.PushAsync(new IconListPage(_category));
        }
    }
}
