﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Plugin.Toast;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace MoneyPlannerApp.ViewModels
{
    public class CategoryListViewModel : ViewModelBase
    {
        private const string NO_CLICK = "#526BFE";
        private const string CLICK = "SlateBlue";
        private bool status = false;

        private readonly INavigation _navigation;
        private CategoryServices _categoryServices;
        
        private Category _categorySelected;

        private string _searchText;

        private ObservableCollection<Category> _categoryList;
        private ObservableCollection<Category> _categoryListIncome;
        private ObservableCollection<Category> _categoryListExpense;

        private string _statusColorButtonIncome = NO_CLICK;
        private string _statusColorButtonExpense = CLICK;
        private string _statusText;
        private bool _isVisibleTextNotiSearch = false;
        private string _haveResultText;

        public ObservableCollection<Category> CategoryList
        {
            get => _categoryList;
            set => SetProperty(ref _categoryList, value);
        }

        public DelegateCommand AddCategoryCommand { get; }

        public DelegateCommand CategoryListIncomeCommand { get; }

        public DelegateCommand CategoryListExpenseCommand { get; }

        public DelegateCommand BackToListCommand { get; }

        public bool IsVisibleTextNotiSearch
        {
            get => _isVisibleTextNotiSearch;
            set => SetProperty(ref _isVisibleTextNotiSearch, value);
        }

        public string HaveResultText
        {
            get => _haveResultText;
            set => SetProperty(ref _haveResultText, value);
        }

        public CategoryListViewModel(INavigation iNavigation)
        {
            DataViewAndViewModel.CategoryListViewModel = this;
            _navigation = iNavigation;
            _categoryServices = new CategoryServices();
            _categoryListIncome = new ObservableCollection<Category>();
            _categoryListExpense = new ObservableCollection<Category>();

            HaveResultText = "Have ";
            GetCategoryList();

            StatusColorButtonIncome = CLICK;
            StatusColorButtonExpense = NO_CLICK;

            AddCategoryCommand = new DelegateCommand(OpenAddCategory);
            CategoryListIncomeCommand = new DelegateCommand(ShowCategoryListIncome);
            CategoryListExpenseCommand = new DelegateCommand(ShowCategoryListExpense);
            BackToListCommand = new DelegateCommand(BacToList);
        }

        public async void GetCategoryList()
        {
            if (ValidationClass.CheckInternet())
            {
                var resultAsync = await _categoryServices.GetCategoryList();
                TokenServices.CheckToken(resultAsync.StatusCode, _navigation);
                if (resultAsync.Status)
                {

                    _categoryListIncome.Clear();
                    _categoryListExpense.Clear();

                    foreach (var item in resultAsync.Data)
                    {
                        if (item.CategoryType)
                        {
                            _categoryListExpense.Add(item);
                        }
                        else
                        {
                            _categoryListIncome.Add(item);
                        }
                    }

                    if (status)
                    {
                        CategoryList = _categoryListExpense;
                    }
                    else
                    {
                        CategoryList = _categoryListIncome;
                    }
                }
                else
                {
                    CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                }
            }
        }

        private void BacToList()
        {
            _navigation.PushAsync(new HomePage());
        }

        private void ShowCategoryListExpense()
        {
            status = true;
            StatusText = "Result : Expense";
            StatusColorButtonIncome = NO_CLICK;
            StatusColorButtonExpense = CLICK;

            CategoryList = _categoryListExpense;
        }

        private void ShowCategoryListIncome()
        {
            status = false;
            StatusText = "Result : Income";
            StatusColorButtonIncome = CLICK;
            StatusColorButtonExpense = NO_CLICK;

            CategoryList = _categoryListIncome;

        }

        public string StatusText
        {
            get => _statusText;
            set => SetProperty(ref _statusText, value);
        }

        public string StatusColorButtonIncome
        {
            get => _statusColorButtonIncome;
            set => SetProperty(ref _statusColorButtonIncome, value);
        }

        public string StatusColorButtonExpense
        {
            get => _statusColorButtonExpense;
            set => SetProperty(ref _statusColorButtonExpense, value);
        }

        private void OpenAddCategory()
        {
            _navigation.PushPopupAsync(new PopupAddCategoryPage());
        }

        public Category CategorySelected
        {
            get => _categorySelected;
            set
            {
                SetProperty(ref _categorySelected, value);
                if (CategorySelected != null)
                {
                    _navigation.PushPopupAsync(new PopupEditCategoryPage(_categorySelected));
                }
            }
        }

        public string SearchText
        {
            get => _searchText;
            set
            {
                _categorySelected = null;
                SetProperty(ref _searchText, value);
                SearchCommandExecute(status);
            }
        }

        private void SearchCommandExecute(bool categoryType)
        {
            //CategoryList = new ObservableCollection<Category>(CategoryDAO.GetCategoryList(categoryType));
            if (categoryType)
            {
                CategoryList = _categoryListExpense;
            }
            else
            {
                CategoryList = _categoryListIncome;
            }

            string searchTextTemp = SearchText.Trim();

            if (CategoryList != null && CategoryList.Count > 0)
            {
                ObservableCollection<Category> searchListExpense = new ObservableCollection<Category>();

                var tempRecordsExpense = CategoryList.Where(x => x.CategoryName.ToLower().Contains(searchTextTemp.ToLower()) || x.CategoryDescription.ToString().Contains(searchTextTemp));

                foreach (Category item in tempRecordsExpense)
                {
                    searchListExpense.Add(item);
                }
                CategoryList = searchListExpense;

                if (CategoryList.Count == 0)
                {
                    IsVisibleTextNotiSearch = true;
                }
                else
                {
                    IsVisibleTextNotiSearch = false;
                }

                if (CategoryList.Count == 0 || CategoryList.Count == 1)
                {
                    HaveResultText = "Has ";
                }
                else
                {
                    HaveResultText = "Have ";
                }
            }
        }
    }
}
