﻿using System.Collections;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Plugin.Toast;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace MoneyPlannerApp.ViewModels
{
    public class PopupEditCategoryViewModel : ViewModelBase
    {
        private INavigation _navigation;
        private CategoryServices _categoryServices;
        private Category _category;

        public DelegateCommand SelectIconCommand { get; }
        public DelegateCommand EditCategoryCommand { get; }
        public DelegateCommand DeleteCategoryCommand { get; }
        public DelegateCommand UpdateCategoryCommand { get; }

        private bool _isChecked;
        private string _errorTextCategoryName;
        private string _errorTextCategoryDescription;
        private bool _isCategoryNameError;
        private bool _isCategoryDescriptionError;

        private bool isEditCategory = true;
        private bool _isEnableCategoryName;
        private bool _isEnableCategoryDescription;
        private bool _isVisibleEdit;
        private bool _isVisibleDelete;
        private bool _isVisibleUpdate;
        private bool _isEnableCategoryType;
        private bool _isEnableCategoryIcon;

        public bool IsEnableCategoryName
        {
            get => _isEnableCategoryName;
            set => SetProperty(ref _isEnableCategoryName, value);
        }

        public bool IsEnableCategoryDescription
        {
            get => _isEnableCategoryDescription;
            set => SetProperty(ref _isEnableCategoryDescription, value);
        }

        public bool IsEnableCategoryType
        {
            get => _isEnableCategoryType;
            set => SetProperty(ref _isEnableCategoryType, value);
        }

        public bool IsEnableCategoryIcon
        {
            get => _isEnableCategoryIcon;
            set => SetProperty(ref _isEnableCategoryIcon, value);
        }

        public bool IsVisibleEdit
        {
            get => _isVisibleEdit;
            set => SetProperty(ref _isVisibleEdit, value);
        }

        public bool IsVisibleDelete
        {
            get => _isVisibleDelete;
            set => SetProperty(ref _isVisibleDelete, value);
        }

        public bool IsVisibleUpdate
        {
            get => _isVisibleUpdate;
            set => SetProperty(ref _isVisibleUpdate, value);
        }

        public PopupEditCategoryViewModel(INavigation navigation, Category category)
        {
            DataViewAndViewModel.PopupEditCategoryViewModel = this;
            _navigation = navigation;
            _categoryServices = new CategoryServices();
            _category = category;

            if (DataViewAndViewModel.EditCategory)
            {
                SetStatusEdit();
            }
            else
            {
                SetStatusDefault();
            }

            SelectIconCommand = new DelegateCommand(OpenSelectIcon);
            DeleteCategoryCommand = new DelegateCommand(DeleteCategory);
            UpdateCategoryCommand = new DelegateCommand(UpdateCategory);
            EditCategoryCommand = new DelegateCommand(EditCategory);

            SetIsChecked();
        }

        private void SetStatusDefault()
        {
            IsEnableCategoryName = false;
            IsEnableCategoryDescription = false;
            IsEnableCategoryType = false;
            IsEnableCategoryIcon = false;

            IsVisibleDelete = false;
            IsVisibleEdit = true;
            IsVisibleUpdate = false;
        }

        private void SetStatusEdit()
        {
            IsEnableCategoryName = true;
            IsEnableCategoryDescription = true;
            IsEnableCategoryType = true;
            IsEnableCategoryIcon = true;

            IsVisibleDelete = true;
            IsVisibleEdit = false;
            IsVisibleUpdate = true;
        }

        private void EditCategory()
        {
            DataViewAndViewModel.EditCategory = true;
            IsEnableCategoryName = true;
            IsEnableCategoryDescription = true;
            IsEnableCategoryType = true;
            IsEnableCategoryIcon = true;

            IsVisibleDelete = true;
            IsVisibleEdit = false;
            IsVisibleUpdate = true;
        }

        public Category Category
        {
            get => _category;
            set => SetProperty(ref _category, value);
        }

        public bool IsCategoryNameError
        {
            get => _isCategoryNameError;
            set => SetProperty(ref _isCategoryNameError, value);
        }

        public bool IsCategoryDescriptionError
        {
            get => _isCategoryDescriptionError;
            set => SetProperty(ref _isCategoryDescriptionError, value);
        }


        public string ErrorTextCategoryName
        {
            get => _errorTextCategoryName;
            set => SetProperty(ref _errorTextCategoryName, value);
        }

        public string ErrorTextCategoryDescription
        {
            get => _errorTextCategoryDescription;
            set => SetProperty(ref _errorTextCategoryDescription, value);
        }

        private void SetIsChecked()
        {
            if (Category.CategoryType)
            {
                IsChecked = false;
            }
            else
            {
                IsChecked = true;
            }
        }

        public bool IsChecked
        {
            get => _isChecked;
            set => SetProperty(ref _isChecked, value);
        }

        private void OpenSelectIcon()
        {
            _navigation.RemovePopupPageAsync(DataViewAndViewModel.PopupEditCategoryPage);
            _navigation.PushAsync(new IconListPage(_category, isEditCategory));
        }

        private bool CheckValidation()
        {
            ArrayList errorList = new ArrayList();

            IsCategoryNameError = string.IsNullOrEmpty(Category.CategoryName);
            IsCategoryDescriptionError = string.IsNullOrEmpty(Category.CategoryDescription);

            string categoryName = Category.CategoryName;
            string categoryDescription = Category.CategoryDescription;

            if (IsCategoryNameError)
            {
                ErrorTextCategoryName = "This field is required";
                errorList.Add(this);
            }
            else
            {
                if (ValidationClass.MinimumString(categoryName, 4))
                {
                    IsCategoryNameError = true;
                    ErrorTextCategoryName = "Requires more than 4 characters";
                    errorList.Add(this);
                }
                else
                {
                    if (ValidationClass.IsHasSpecialCharacters(categoryName))
                    {
                        IsCategoryNameError = true;
                        ErrorTextCategoryName = "Not allow special characters";
                        errorList.Add(this);
                    }
                }
            }

            if (IsCategoryDescriptionError)
            {
                ErrorTextCategoryDescription = "This field is required";
                errorList.Add(this);
            }
            else
            {
                if (ValidationClass.MinimumString(categoryDescription, 4))
                {
                    IsCategoryDescriptionError = true;
                    ErrorTextCategoryDescription = "Requires more than 4 characters";
                    errorList.Add(this);
                }
                else
                {
                    if (ValidationClass.IsHasSpecialCharacters(categoryDescription))
                    {
                        IsCategoryDescriptionError = true;
                        ErrorTextCategoryDescription = "Not allow special characters";
                        errorList.Add(this);
                    }
                }
            }

            if (errorList.Count > 0)
            {
                return false;
            }
            return true;
        }

        private async void UpdateCategory()
        {
            if (CheckValidation())
            {
                DataViewAndViewModel.EditCategory = false;
                SetStatusDefault();
                if (ValidationClass.CheckInternet())
                {
                    var pageLoad = new PopupPage();
                    await _navigation.PushPopupAsync(pageLoad);
                    var resultAsync = await _categoryServices.UpdateCategory(Category);
                    TokenServices.CheckToken(resultAsync.StatusCode, _navigation);
                    await _navigation.RemovePopupPageAsync(pageLoad);
                    if (resultAsync.Status)
                    {
                        DataViewAndViewModel.CategoryListViewModel.GetCategoryList();
                        await _navigation.RemovePopupPageAsync(DataViewAndViewModel.PopupEditCategoryPage);
                        CrossToastPopUp.Current.ShowToastSuccess(resultAsync.Message);
                    }
                    else
                    {
                        CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                    }
                }
            }
        }

        private async void DeleteCategory()
        {
            await _navigation.PushPopupAsync(new PopupDeleteAlertPage(Category));
        }
    }
}
