﻿using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Plugin.Toast;
using System.Diagnostics;
using System.Collections;

namespace MoneyPlannerApp.ViewModels
{
    public class PopupChangePasswordViewModel : ViewModelBase
    {
        private UserServices _userServices;
        public DelegateCommand OkCommand { get; }
        public DelegateCommand CancelCommand { get; }
        private string _confirmPassword;
        private Password _password;
        private bool _isConfirmPasswordNull;
        private bool _isOldPasswordNull;
        private bool _isNewPasswordNull;
        PopupChangePasswordPage _popupChangePasswordPage;
        private string _errorTextOldPassword;
        private string _errorTextNewPassword;
        private string _errorTextConfirmPassword;

        public string NewPasswordTemp
        {
            get => _newPasswordTemp;
            set
            {
                SetProperty(ref _newPasswordTemp, value);
            }
        }

        public string ErrorTextConfirmPassword
        {
            get => _errorTextConfirmPassword;
            set
            {
                SetProperty(ref _errorTextConfirmPassword, value);
            }
        }
        public string ErrorTextNewPassword
        {
            get => _errorTextNewPassword;
            set
            {
                SetProperty(ref _errorTextNewPassword, value);
            }
        }
        public string ErrorTextOldPassword
        {
            get => _errorTextOldPassword;
            set
            {
                SetProperty(ref _errorTextOldPassword, value);
            }
        }

        public bool IsConfirmPasswordNull
        {
            get => _isConfirmPasswordNull;
            set
            {
                SetProperty(ref _isConfirmPasswordNull, value);
            }
        }
        public INavigation _navigation;
        private string _newPasswordTemp;

        public bool IsNewPasswordNull
        {
            get => _isNewPasswordNull;
            set
            {
                SetProperty(ref _isNewPasswordNull, value);
            }
        }
        public string ConfirmPassword
        {
            get => _confirmPassword;
            set
            {
                SetProperty(ref _confirmPassword, value);
            }
        }
        public bool IsOldPasswordNull
        {
            get => _isOldPasswordNull;
            set
            {
                SetProperty(ref _isOldPasswordNull, value);
            }
        }



        public Password Password
        {
            get => _password;
            set
            {
                SetProperty(ref _password, value);
            }
        }

        public PopupChangePasswordViewModel(INavigation navigation, PopupChangePasswordPage popupChangePasswordPage)
        {
            _password = new Password();
            _userServices = new UserServices();
            _popupChangePasswordPage = popupChangePasswordPage;
            _navigation = navigation;
            CancelCommand = new DelegateCommand(ExecuteCancel);
            OkCommand = new DelegateCommand(ExecuteChangePasswordAsync);
        }

        private async void ExecuteChangePasswordAsync() //delete async
        {

            //IsNewPasswordNull = string.IsNullOrEmpty(_password.NewPassword);
            //IsConfirmPasswordNull = string.IsNullOrEmpty(ConfirmPassword);

            //if (IsOldPasswordNull || IsNewPasswordNull || IsConfirmPasswordNull)
            //{
            //    CrossToastPopUp.Current.ShowToastError("Please typee fully");
            //}

            //else if (_confirmPassword.Equals(_password.NewPassword))
            //{
            //    Debug.Write(_confirmPassword);
            //    Debug.Write(_password.NewPassword);
            //    _password.OldPassword = SecurePasswordHasher.Hash(_password.OldPassword);
            //    _password.NewPassword = SecurePasswordHasher.Hash(_password.NewPassword);
            //    var resultAsync = await _userServices.ChangePasswordAsync(_password);

            //    if (resultAsync.Status)
            //    {
            //        CrossToastPopUp.Current.ShowToastSuccess(resultAsync.Message);
            //        await _navigation.RemovePopupPageAsync(_popupChangePasswordPage);
            //    }
            //    else
            //    {
            //        CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
            //    }

            //}
            //else
            //{
            //    CrossToastPopUp.Current.ShowToastError("Confirm Password is not correct!");
            //}


            if (CheckValidation())
            {
                //_confirmPassword = SecurePasswordHasher.Hash(_confirmPassword);
                //_password.OldPassword = SecurePasswordHasher.Hash(_password.OldPassword);
                //_password.NewPassword = SecurePasswordHasher.Hash(_password.NewPassword);
                if (ValidationClass.CheckInternet())
                {

                    Debug.Write(ConfirmPassword);
                    Debug.Write(NewPasswordTemp);
                    if (ConfirmPassword.Equals(NewPasswordTemp))
                    {

                        //ConfirmPassword = SecurePasswordHasher.Hash(_confirmPassword);
                        Password.OldPassword = SecurePasswordHasher.Hash(_password.OldPassword);
                        Password.NewPassword = NewPasswordTemp;
                        Password.NewPassword = SecurePasswordHasher.Hash(_password.NewPassword);
                        var resultAsync = await _userServices.ChangePasswordAsync(_password);

                        TokenServices.CheckToken(resultAsync.StatusCode, _navigation, _popupChangePasswordPage);

                        if (resultAsync.Status)
                        {
                            CrossToastPopUp.Current.ShowToastSuccess(resultAsync.Message);
                            await _navigation.RemovePopupPageAsync(_popupChangePasswordPage);
                        }
                        else
                        {
                            IsOldPasswordNull = true;
                            ErrorTextOldPassword = "Current password is not correct!";
                            //CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                        }
                    }
                    else
                    {
                        IsConfirmPasswordNull = true;
                        ErrorTextConfirmPassword = "Confirm Password does not match!";
                    }
                }
            }

        }

        private void ExecuteCancel()
        {
            _navigation.RemovePopupPageAsync(_popupChangePasswordPage);
        }

        public bool CheckValidation()
        {
            
                ArrayList errorList = new ArrayList();

                //Validation Old Password
                if (ValidationClass.IsNullOrEmpty(_password.OldPassword))
                {
                    IsOldPasswordNull = true;
                    ErrorTextOldPassword = "This field is required";
                    errorList.Add(this);
                }
                else
                {
                    IsOldPasswordNull = false;
                    if (ValidationClass.MinimumString(_password.OldPassword, 6))
                    {
                        IsOldPasswordNull = true;
                        ErrorTextOldPassword = "Please input more than 6 characters";
                        errorList.Add(this);
                    }
                    else
                    {
                        IsOldPasswordNull = false;
                    }
                }

                //Validation New Password
                if (ValidationClass.IsNullOrEmpty(NewPasswordTemp))
                {
                    IsNewPasswordNull = true;
                    ErrorTextNewPassword = "This field is required";
                    errorList.Add(this);
                }
                else
                {
                    IsNewPasswordNull = false;
                    if (ValidationClass.MinimumString(NewPasswordTemp, 6))
                    {
                        IsNewPasswordNull = true;
                        ErrorTextNewPassword = "Please input more than 6 characters";
                        errorList.Add(this);
                    }
                    else
                    {
                        IsNewPasswordNull = false;
                    }
                }

                //Validation Confirm Password
                if (ValidationClass.IsNullOrEmpty(ConfirmPassword))
                {
                    IsConfirmPasswordNull = true;
                    ErrorTextConfirmPassword = "This field is required";
                    errorList.Add(this);
                }
                else
                {
                    IsConfirmPasswordNull = false;
                    if (ValidationClass.MinimumString(ConfirmPassword, 6))
                    {
                        IsConfirmPasswordNull = true;
                        ErrorTextConfirmPassword = "Please input more than 6 characters";
                        errorList.Add(this);
                    }
                    else
                    {
                        IsConfirmPasswordNull = false;
                    }
                }

                if (errorList.Count > 0)
                {
                    return false;
                }

                return true;

            
        }
    }
}
