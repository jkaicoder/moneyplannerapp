﻿using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Xamarin.Forms;
using Plugin.Toast;
using MoneyPlannerApp.Services;
using System.Collections;

namespace MoneyPlannerApp.ViewModels
{
    public class InputAmountViewModel : ViewModelBase
    {
        private Account _accountSelected;
        private INavigation _navigation;
        private Record _record;
        private bool _isRecordNameEmpty;
        private bool _isRecordAmountEmpty;
        public string AccountNameTemp
        {
            get => _accountNameTemp;
            set
            {
                SetProperty(ref _accountNameTemp, value);
            }
        }

        public string ErrorTextAmount
        {
            get => _errorTextAmount;
            set
            {
                SetProperty(ref _errorTextAmount, value);
            }
        }
        public string ErrorText
        {
            get => _errorText;
            set
            {
                SetProperty(ref _errorText, value);
            }
        }
        private bool _isCreateRecord;
        private string _errorText;
        private string _errorTextAmount;
        private string _accountNameTemp;

        public bool IsRecordAmountEmpty
        {
            get => _isRecordAmountEmpty;
            set
            {
                SetProperty(ref _isRecordAmountEmpty, value);
            }
        }

        public bool IsRecordNameEmpty
        {
            get => _isRecordNameEmpty;
            set => SetProperty(ref _isRecordNameEmpty, value);
        }

        public DelegateCommand NextCommand { get; }

        public Record Record
        {
            get => _record;
            set
            {
                SetProperty(ref _record, value);
                Debug.Write(_record.RecordAmount);
            }
        }

        public InputAmountViewModel(INavigation navigation, Account accountSelected)
        {
            AccountNameTemp = "New Record in Account " + DataViewAndViewModel.SelectedAccountDropdown.AccountName;
            _accountSelected = accountSelected;
            NextCommand = new DelegateCommand(NextScreen);
            _navigation = navigation;
            _isCreateRecord = true;
            Record = new Record();
            Record.RecordCategory = new Category();


        }

        private void NextScreen()
        {
            Debug.Write(_record.RecordAmount);
            Debug.Write(Record.RecordAmount);
            IsRecordNameEmpty = string.IsNullOrEmpty(Record.RecordName);
            Debug.Write(Record.RecordAmount);
            if (Record.RecordAmount == 0)
            {
                IsRecordAmountEmpty = true;
                ErrorTextAmount = "This field is requied";
            }

            else
            {
                IsRecordAmountEmpty = false;
            }



            CheckValidation();
            if (!IsRecordNameEmpty && !IsRecordAmountEmpty)
            {


                if (CheckValidation())
                {
                    _navigation.PushAsync(new ChooseCategoryPage(_record, _isCreateRecord));
                }


            }



            //if (IsRecordNameEmpty)
            //    CrossToastPopUp.Current.ShowToastMessage("Name is empty");
            //if(IsRecordAmountEmpty)
            //    CrossToastPopUp.Current.ShowToastMessage("Amount is zero");
            //if (IsRecordAmountEmpty && IsRecordNameEmpty)
            //    CrossToastPopUp.Current.ShowToastMessage("Please enter fully");



        }

        private bool CheckValidation()
        {
            ArrayList errorList = new ArrayList();

            IsRecordNameEmpty = string.IsNullOrEmpty(Record.RecordName);

            string recordName = Record.RecordName;

            if (IsRecordNameEmpty)
            {
                ErrorText = "This field is required";
                errorList.Add(this);
            }
            else
            {
                if (string.IsNullOrWhiteSpace(recordName) || ValidationClass.StartSpaces(recordName) || ValidationClass.EndSpaces(recordName))
                {
                    IsRecordNameEmpty = true;
                    ErrorText = "Not allow spaces characters";
                    errorList.Add(this);
                }
                else
                {
                    if (ValidationClass.MinimumString(recordName, 4))
                    {
                        IsRecordNameEmpty = true;
                        ErrorText = "Requires more than 4 characters";
                        errorList.Add(this);
                    }
                    else
                    {
                        if (ValidationClass.IsHasSpecialCharacters(recordName))
                        {
                            IsRecordNameEmpty = true;
                            ErrorText = "Not allow special characters";
                            errorList.Add(this);
                        }
                    }
                }
            }

            if (errorList.Count > 0)
            {
                return false;
            }
            return true;
        }
    }
}
