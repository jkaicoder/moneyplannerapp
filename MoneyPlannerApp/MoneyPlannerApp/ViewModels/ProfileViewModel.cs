﻿using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Plugin.Toast;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace MoneyPlannerApp.ViewModels
{
    public class ProfileViewModel : ViewModelBase
    {
        private UserServices _userServices;
        private INavigation _navigation;

        public DelegateCommand UpdateProfileCommand { get; }
        public DelegateCommand ChangePasswordCommand { get; }
        private UserToken _userToken;
        private User _user;
        private User _userTemp;
        private string _genderTemp;
        private string _imageAvatar = "blank_image";
        private string _birthdayTemp;
        private DateTime _dateTimeTemp;

        public string BirthdayTemp
        {
            get => _birthdayTemp;
            set
            {
                SetProperty(ref _birthdayTemp, value);
            }
        }

        public string ImageAvatar
        {
            get => _imageAvatar;
            set
            {
                SetProperty(ref _imageAvatar, value);
            }
        }

        public string GenderTemp
        {
            get => _genderTemp;
            set
            {
                SetProperty(ref _genderTemp, value);
            }
        }

        public User UserTemp
        {
            get => _userTemp;
            set
            {
                SetProperty(ref _userTemp, value);
            }
        }
        public User User
        {
            get => _user;
            set
            {
                SetProperty(ref _user, value);
            }
        }

        public ProfileViewModel(INavigation navigation)
        {
            DataViewAndViewModel.ProfileViewModel = this;
            _navigation = navigation;
            _userServices = new UserServices();
            _user = new User();
            _userTemp = new User();
            
            BirthdayTemp = _user.BirthDay.ToString();
            UpdateProfileCommand = new DelegateCommand(NextToUpdateProfile);
            ChangePasswordCommand = new DelegateCommand(ChangePassword);

            GetUserByID();
            
        }

        private void ChangePassword()
        {
            _navigation.PushPopupAsync(new PopupChangePasswordPage());
        }

        private void NextToUpdateProfile()
        {
            Debug.Write(User.FirstName);
            _navigation.PushAsync(new EditProfilePage());
        }

        public async void GetUserByID()
        {
            _userToken = UserDAO.GetUserToken().Last();

            var resultAsync = await _userServices.GetInfoUserCurrent(_userToken.UserID);

            TokenServices.CheckToken(resultAsync.StatusCode, _navigation);

            if (resultAsync.Status)
            {
                User = resultAsync.Data;
                ImageAvatar = Configuration.ID_HOST + _user.UserImage;

                if (_user.Gender == 1)
                {
                    GenderTemp = "Male";
                }
                if (_user.Gender == 2)
                {
                    GenderTemp = "Female";
                }
                if (_user.Gender == 0)
                {
                    GenderTemp = "Not update";
                }

                //------------------

                if (_user.Address == "")
                {
                    UserTemp.Address = "Not update";
                }
                if (_user.Address != "")
                {
                    UserTemp.Address = _user.Address;
                }

                //------------------

                if(_user.Email == "")
                {
                    UserTemp.Email = "Not update";
                }
                if(_user.Email != "")
                {
                    UserTemp.Email = _user.Email;
                }

                //------------------

                if (_user.Job == "")
                {
                    UserTemp.Job = "Not update";
                }
                if (_user.Job != "")
                {
                    UserTemp.Job = _user.Job;
                }

                //------------------

                if (_user.PhoneNumber == "")
                {
                    UserTemp.PhoneNumber = "Not update";
                }
                if (_user.PhoneNumber != "")
                {
                    UserTemp.PhoneNumber = _user.PhoneNumber;
                }

                //------------------
                if (_user.BirthDay == _dateTimeTemp)
                {
                    BirthdayTemp = "Not update";
                }
                if (_user.BirthDay != _dateTimeTemp)
                {
                    BirthdayTemp = String.Format("{0:dd MMMM, yyyy}",_user.BirthDay);
                }
                //------------------
                if(_user.FirstName == "" && _user.LastName == "")
                {
                    UserTemp.FirstName = "Not update";
                }
                if (_user.FirstName != "" || _user.LastName != "")
                {
                    UserTemp.FirstName = _user.FirstName;
                    UserTemp.LastName = _user.LastName;
                }


            }
            else
            {
                CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
            }
        }



    }
}
