﻿using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Plugin.Toast;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace MoneyPlannerApp.ViewModels
{
    class EditProfileViewModel : ViewModelBase
    {
        private User _user;

        private INavigation _navigation;
        private UserServices _userService;
        private EditProfilePage _editProfilePage;
        private UserToken _userToken;
        private UserServices _userServices;
        private DateTime _defaultDateTime;
        private DateTime _dateTimeNow;
        public bool IsBirthdayFuture
        {
            get => _isBirthdayFuture;
            set
            {
                SetProperty(ref _isBirthdayFuture, value);
            }
        }

        public bool IsBirthdayHaveValues
        {
            get => _isBirthdayHaveValues;
            set
            {
                SetProperty(ref _isBirthdayHaveValues, value);
            }
        }

        public bool IsBirthdayNotUpdate
        {
            get => _isBirthdayNotUpdate;
            set
            {
                SetProperty(ref _isBirthdayNotUpdate, value);
            }
        }

        public bool RadioButonUnknowCheck
        {
            get => _radioButonUnknowCheck;
            set
            {
                SetProperty(ref _radioButonUnknowCheck, value);
            }
        }

        public int GenderIntTemp
        {
            get => _genderIntTemp;
            set
            {
                SetProperty(ref _genderIntTemp, value);
            }
        }

        public bool IsEmailEmpty
        {
            get => _isEmailEmpty;
            set
            {
                SetProperty(ref _isEmailEmpty, value);
            }
        }

        public bool IsEditProfileButtonVisible
        {
            get => _isEditProfileButtonVisible;
            set
            {
                SetProperty(ref _isEditProfileButtonVisible, value);
            }
        }
        public bool IsEditable
        {
            get => _isEditable;
            set
            {
                SetProperty(ref _isEditable, value);
            }
        }


        public bool RadioButtonFemaleCheck
        {
            get => _radioButtonFemaleCheck;
            set
            {
                SetProperty(ref _radioButtonFemaleCheck, value);
            }
        }
        public bool IsPhoneError
        {
            get => _isPhoneError;
            set
            {
                SetProperty(ref _isPhoneError, value);
            }
        }
        public bool RadioButtonMaleCheck
        {
            get => _radioButtonMaleCheck;
            set
            {
                SetProperty(ref _radioButtonMaleCheck, value);
            }
        }
        private string _imageAvatar;
        private bool _isFirstNameNull;
        private bool _radioButtonMaleCheck;
        private bool _isPhoneError;
        private bool _radioButtonFemaleCheck;
        private bool _isEditable;
        private bool _isEditProfileButtonVisible;
        private global::System.Boolean _isEmailEmpty;
        private int _genderIntTemp;
        private bool _radioButonUnknowCheck;
        private bool _isBirthdayNotUpdate;
        private bool _isBirthdayHaveValues;
        private bool _isBirthdayFuture;

        public bool IsFirstNameNull
        {
            get => _isFirstNameNull;
            set
            {
                SetProperty(ref _isFirstNameNull, value);
            }
        }
        public DelegateCommand UpdateProfileCommand { get; }
        public DelegateCommand CancelCommand { get; }
        public DelegateCommand ChangeEditableCommand { get; }
        public string ImageAvatar
        {
            get => _imageAvatar;
            set
            {
                SetProperty(ref _imageAvatar, value);
            }
        }

        public User User
        {
            get => _user;
            set
            {
                SetProperty(ref _user, value);
            }
        }

        public EditProfileViewModel(INavigation navigation, EditProfilePage editProfilePage)
        {
            _editProfilePage = editProfilePage;
            _userServices = new UserServices();
            _user = new User();
            _navigation = navigation;
            _defaultDateTime = new DateTime();
            _dateTimeNow = new DateTime();
            _dateTimeNow = DateTime.Now;

            GetUserByID();

            Debug.Write(User.Gender);

            RadioButonUnknowCheck = true;

            IsEditable = false;
            IsEditProfileButtonVisible = true;


            ImageAvatar = Configuration.ID_HOST + _user.UserImage;
            _userService = new UserServices();

            UpdateProfileCommand = new DelegateCommand(ExecuteUpdateProfile);
            CancelCommand = new DelegateCommand(ExecuteCancel);
            ChangeEditableCommand = new DelegateCommand(ExecuteChangeEditable);

        }

        private void ExecuteChangeEditable()
        {
            if (ValidationClass.CheckInternet())
            {
                IsEditable = true;
                IsEditProfileButtonVisible = false;

                if (User.FirstName == "Not update")
                    User.FirstName = "";

                if (User.LastName == "Not update")
                    User.LastName = "";

                if (User.Job == "Not update")
                    User.Job = "";

                if (User.Address == "Not update")
                    User.Address = "";

                if (User.PhoneNumber == "Not update")
                    User.PhoneNumber = "";

                if (User.BirthDay == _defaultDateTime)
                {
                    IsBirthdayNotUpdate = false;
                    IsBirthdayHaveValues = true;
                }
            }

        }


        private void ExecuteCancel()
        {
            OnAlertYesNoClicked();
        }

        private async void ExecuteUpdateProfile()
        {
            if (ValidationClass.CheckInternet())
            {

                IsFirstNameNull = string.IsNullOrEmpty(User.FirstName);
                IsEmailEmpty = string.IsNullOrEmpty(User.Email);


                if (DateTime.Compare(User.BirthDay, _dateTimeNow) > 0)
                {
                    IsBirthdayFuture = true;
                }
                else
                {
                    IsBirthdayFuture = false;
                }




                if (!IsFirstNameNull && !PhoneCheck() && DateTime.Compare(User.BirthDay, _dateTimeNow) <= 0)
                {
                    ProcessUser();
                    if (!RadioButtonFemaleCheck && !RadioButtonMaleCheck)
                    {
                        _user.Gender = 0;
                    }
                    if (RadioButtonMaleCheck)
                    {
                        _user.Gender = 1;
                    }
                    if (RadioButtonFemaleCheck)
                    {
                        _user.Gender = 2;
                    }



                    var page = new PopupPage();
                    await _navigation.PushPopupAsync(page);

                    var resultAsync = await _userService.UpdateUserInfoAsync(_user);

                    await _navigation.RemovePopupPageAsync(page);

                    TokenServices.CheckToken(resultAsync.StatusCode, _navigation);

                    if (resultAsync.Status)
                    {
                        CrossToastPopUp.Current.ShowToastSuccess(resultAsync.Message);
                        IsEditable = false;
                        IsEditProfileButtonVisible = true;

                        if (User.FirstName == "")
                            User.FirstName = "Not update";

                        if (User.LastName == "")
                            User.LastName = "Not update";

                        if (User.Job == "")
                            User.Job = "Not update";

                        if (User.Address == "")
                            User.Address = "Not update";

                        if (User.PhoneNumber == "")
                            User.PhoneNumber = "Not update";

                        if (User.BirthDay == _defaultDateTime)
                        {
                            IsBirthdayNotUpdate = true;
                            IsBirthdayHaveValues = false;
                        }

                        //DataViewAndViewModel.ProfileViewModel.GetUserByID();
                        //_navigation.RemovePage(_editProfilePage);
                    }
                    else
                    {
                        CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                    }

                }
                if (IsFirstNameNull)
                {
                    CrossToastPopUp.Current.ShowToastError("First name is required");
                }
            }

        }

        private void ProcessUser()
        {

            if (!string.IsNullOrEmpty(User.FirstName))
            {
                User.FirstName = User.FirstName.Trim();
            }
            if (!string.IsNullOrEmpty(User.LastName))
            {
                User.LastName = User.LastName.Trim();
            }
            if (!string.IsNullOrEmpty(User.Job))
            {
                User.Job = User.Job.Trim();
            }
            if (!string.IsNullOrEmpty(User.Address))
            {
                User.Address = User.Address.Trim();
            }
            if (!string.IsNullOrEmpty(User.PhoneNumber))
            {
                User.PhoneNumber = User.PhoneNumber.Trim();
            }

        }

        private async void OnAlertYesNoClicked()
        {
            var answer = await App.Current.MainPage.DisplayAlert(
                "Warning", "Are you sure to cancel?",
                "OK",
                "Cancel");
            if (answer)
            {
                //_navigation.RemovePage(_editProfilePage);
                IsEditable = false;
                IsEditProfileButtonVisible = true;

                if (GenderIntTemp == 0)
                {
                    RadioButonUnknowCheck = true;
                }
                if (GenderIntTemp == 1)
                {
                    RadioButtonMaleCheck = true;
                }
                if (GenderIntTemp == 2)
                {
                    RadioButtonFemaleCheck = true;
                }

                if (User.FirstName == "")
                    User.FirstName = "Not update";

                if (User.LastName == "")
                    User.LastName = "Not update";

                if (User.Job == "")
                    User.Job = "Not update";

                if (User.Address == "")
                    User.Address = "Not update";

                if (User.PhoneNumber == "")
                    User.PhoneNumber = "Not update";

                if (User.BirthDay == _defaultDateTime)
                {
                    IsBirthdayNotUpdate = true;
                    IsBirthdayHaveValues = false;
                }
            }

        }

        public async void GetUserByID()
        {
            if (ValidationClass.CheckInternet())
            {
                _userToken = UserDAO.GetUserToken().Last();

                var page = new PopupPage();
                await _navigation.PushPopupAsync(page);

                var resultAsync = await _userServices.GetInfoUserCurrent(_userToken.UserID);

                await _navigation.RemovePopupPageAsync(page);

                TokenServices.CheckToken(resultAsync.StatusCode, _navigation);

                if (resultAsync.Status)
                {
                    User = resultAsync.Data;

                    Debug.Write(resultAsync.Data.Gender);

                    GenderIntTemp = User.Gender;

                    if (User.Gender == 1)
                    {
                        RadioButtonMaleCheck = true;
                    }
                    if (User.Gender == 2)
                    {
                        RadioButtonFemaleCheck = true;
                    }

                    //------------------------------Process User Temp -----------------------------//
                    Debug.Write(User.PhoneNumber);

                    if (User.FirstName == "")
                        User.FirstName = "Not update";

                    if (User.LastName == "")
                        User.LastName = "Not update";

                    if (User.Job == "")
                        User.Job = "Not update";

                    if (User.Address == "")
                        User.Address = "Not update";

                    if (User.PhoneNumber == "")
                        User.PhoneNumber = "Not update";
                    if (User.BirthDay == _defaultDateTime)
                    {
                        IsBirthdayNotUpdate = true;
                        IsBirthdayHaveValues = false;
                    }
                    if (User.BirthDay != _defaultDateTime)
                    {
                        IsBirthdayNotUpdate = false;
                        IsBirthdayHaveValues = true;
                    }



                }
                else
                {
                    CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                }
            }
        }

        private bool PhoneCheck()
        {
            if (!string.IsNullOrEmpty(User.PhoneNumber) && User.PhoneNumber.Length < 10)
            {
                IsPhoneError = true;
                return true;
            }
            if (User.PhoneNumber.Length == 10)
            {
                IsPhoneError = false;
                return false;
            }
            if (string.IsNullOrEmpty(User.PhoneNumber))
            {
                IsPhoneError = false;
                return false;
            }
            else
            {
                return false;
            }
        }
    }
}
