﻿using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using MoneyPlannerApp.Data;
using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Plugin.Toast;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using PopupPage = MoneyPlannerApp.Views.PopupPage;
using System;
using System.Linq;
using System.Windows.Input;

namespace MoneyPlannerApp.ViewModels
{
    public class HomeViewModel : ViewModelBase
    {
        private readonly INavigation _navigation;
        private readonly AccountServices _accountServices;
        private UserServices _userServices;
        private readonly RecordServices _recordServices;

        private string _itemIconHome;
        private string _itemTextColorHome;
        private string _itemIconHomeSize;

        private string _itemIconNew;
        private string _itemTextColorNew;
        private string _itemIconNewSize;

        private string _itemIconAccount;
        private string _itemTextColorAccount;
        private string _itemIconAccountSize;

        private string _itemIconMore;
        private string _itemTextColorMore;
        private string _itemIconMoreSize;
        private ObservableCollection<Account> _accountList;
        private Account _selectedAccount;
        private Record _recordSelected;
        private int _incomeNumber;
        private int _expenseNumber;
        private Account _accountSelected;
        private readonly HomeViewModel _homeViewModel;

        private string _searchTextAccount;
        private Account _selectedAccountDropdown;
        private ObservableCollection<Record> _recordList;
        private ObservableCollection<Account> _accountListTemp;
        private int _totalRecord;
        private int _totalAccount;
        private Account _accountRecord;
        private int _accountAmountSelected;
        private int _accountAmount;
        private bool _isVisibleTextNotiSearch;
        private bool _isVisibleSearchBarAccount = true;
        private string _notiAccountText;
        private bool _isVisibleCategoriesManagement = false;


        public DelegateCommand HomeItemPage { get; }

        public DelegateCommand NewItemPage { get; }

        public DelegateCommand AccountItemPage { get; }

        public DelegateCommand MoreItemPage { get; }

        public DelegateCommand NextProfile { get; }

        //Event on every item 

        public DelegateCommand CategoryListCommand { get; }

        public DelegateCommand CreateRecordCommand { get; }

        public DelegateCommand DeleteAllRecordCommand { get; }

        public DelegateCommand SignOutCommand { get; }

        public DelegateCommand RefreshCommand { get; }

        public DelegateCommand AddAccountCommand { get; }

        public DelegateCommand ChangePasswordCommand { get; }

        public ObservableCollection<Account> AccountListTemp
        {
            get => _accountListTemp;
            set => SetProperty(ref _accountListTemp, value);
        }

        public int TotalRecord
        {
            get => _totalRecord;
            set => SetProperty(ref _totalRecord, value);
        }

        public int TotalAccount
        {
            get => _totalAccount;
            set => SetProperty(ref _totalAccount, value);
        }

        public bool IsVisibleSearchBarAccount
        {
            get => _isVisibleSearchBarAccount;
            set => SetProperty(ref _isVisibleSearchBarAccount, value);
        }

        public string NotiAccountText
        {
            get => _notiAccountText;
            set => SetProperty(ref _notiAccountText, value);
        }

        public bool IsVisibleCategoriesManagement
        {
            get => _isVisibleCategoriesManagement;
            set => SetProperty(ref _isVisibleCategoriesManagement, value);
        }

        public HomeViewModel(INavigation navigation)
        {
            DataViewAndViewModel.HomeViewModel = this;

            _navigation = navigation;
            _homeViewModel = this;
            _accountServices = new AccountServices();
            _userServices = new UserServices();
            _recordServices = new RecordServices();
            
            

            HomeItemPage = new DelegateCommand(OpenHomeItemPage);

            NewItemPage = new DelegateCommand(OpenNewItemPage);

            AccountItemPage = new DelegateCommand(OpenAccountItemPage);

            MoreItemPage = new DelegateCommand(OpenMoreItemPage);

            NextProfile = new DelegateCommand(NextToProfilePage);

            ItemIconHome = "ic_home_active";
            ItemTextColorHome = "#5AA6EA";
            ItemIconHomeSize = "30";

            ItemIconNew = "ic_add";
            ItemTextColorNew = "#7A7A7A";
            ItemIconNewSize = "22";

            ItemIconAccount = "ic_account";
            ItemTextColorAccount = "#7A7A7A";
            ItemIconAccountSize = "22";

            ItemIconMore = "ic_settings";
            ItemTextColorMore = "#7A7A7A";
            ItemIconMoreSize = "22";

            CategoryListCommand = new DelegateCommand(OpenCategoryList);

            CreateRecordCommand = new DelegateCommand(ExecuteCreateRecord);

            //RecordList = new ObservableCollection<Record>(RecordDAO.GetRecordList());

            //AccountList = new ObservableCollection<Account>(AccountDAO.GetAccountList());

            RecordList = new ObservableCollection<Record>();

            AccountList = new ObservableCollection<Account>();

            AccountListTemp = new ObservableCollection<Account>();

            GetTotalAccountRecordAPI();

            GetAccountListAPI();

            if (DataViewAndViewModel.UserLevel == 1)
            {
                IsVisibleCategoriesManagement = true;
            }

            DeleteAllRecordCommand = new DelegateCommand(DeleteAllRecord);

            SignOutCommand = new DelegateCommand(SignOut);

            CurrentUser = UserData.Currentuser;

            RefreshCommand = new DelegateCommand(Refresh);

            AddAccountCommand = new DelegateCommand(ExecuteAddAccount);

            ChangePasswordCommand = new DelegateCommand(ChangePassword);
        }

        private void ChangePassword()
        {
            if (ValidationClass.CheckInternet())
            {
                _navigation.PushPopupAsync(new PopupChangePasswordPage());
            }
        }

        public async Task GetTotalAccountRecordAPI()
        {
            if (ValidationClass.CheckInternet())
            {
                var resultAsync = await _userServices.TotalAccountRecord();
                TokenServices.CheckToken(resultAsync.StatusCode, _navigation);
                if (resultAsync.Status)
                {
                    TotalAccount = resultAsync.Data.TotalAccount;
                    TotalRecord = resultAsync.Data.TotalRecord;
                }
            }
        }

        public Account SelectedAccountDropdown
        {
            get => _selectedAccountDropdown;
            set
            {
                SetProperty(ref _selectedAccountDropdown, value);
                AccountAmount = 0;
                GetRecordListAPI(SelectedAccountDropdown);
                DataViewAndViewModel.SelectedAccountDropdown = SelectedAccountDropdown;
            }
        }

        public int AccountAmountSelected
        {
            get => _accountAmountSelected;
            set => SetProperty(ref _accountAmountSelected, value);
        }

        private void ExecuteAddAccount()
        {
            _navigation.PushPopupAsync(new PopupAddAccountPage());
        }

        public async Task GetAccountListAPI()
        {
            if (ValidationClass.CheckInternet())
            {
                var page = new PopupPage();
                await _navigation.PushPopupAsync(page);
                var resultAsync = await _accountServices.GetAccountListAsync();
                await _navigation.RemovePopupPageAsync(page);
                TokenServices.CheckToken(resultAsync.StatusCode, _navigation);
                if (resultAsync.Status)
                {
                    AccountList.Clear();
                    AccountList = resultAsync.Data;

                    AccountListTemp.Clear();

                    foreach (var accountItem in AccountList)
                    {
                        AccountListTemp.Add(accountItem);
                    }

                    if (AccountList.Count > 0)
                    {
                        SelectedAccountDropdown = AccountList[0];
                        IsVisibleSearchBarAccount = true;
                        IsVisibleTextNotiSearch = false;
                    }
                    else
                    {
                        IsVisibleSearchBarAccount = false;
                        IsVisibleTextNotiSearch = true;
                        NotiAccountText = "Sorry, no account has been created.";
                    }
                }
                else
                {
                    CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                }
            }
        }

        public async Task RefreshAccount()
        {
            var resultAsync = await _accountServices.GetAccountListAsync();
            TokenServices.CheckToken(resultAsync.StatusCode, _navigation);
            if (resultAsync.Status)
            {
                AccountList = resultAsync.Data;
                //AccountList.Clear();
                //foreach (var accountItem in AccountList)
                //{
                //    AccountListTemp.Add(accountItem);
                //}
                //DataViewAndViewModel.HomeViewModel.Refresh();
            }
            else
            {
                CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
            }
        }


        public void RefeshRecordList()
        {
            AccountAmount = 0;
            GetRecordListAPI(SelectedAccountDropdown);
        }

        private async void GetRecordListAPI(Account selectedAccountDropdown)
        {
            if (selectedAccountDropdown != null)
            {
                //var page = new PopupPage();
                //await _navigation.PushPopupAsync(page);
                var resultAsync = await _recordServices.GetRecordForAccountAsync(selectedAccountDropdown);
                TokenServices.CheckToken(resultAsync.StatusCode, _navigation);
                //await _navigation.RemovePopupPageAsync(page);
                if (resultAsync.Status)
                {
                    AddColorForRecord(resultAsync.Data);
                    RecordList = resultAsync.Data;

                    if (RecordList.Count > 0)
                    {
                        ExpenseNumber = 0;
                        IncomeNumber = 0;
                        
                        for (int i = 0; i < RecordList.Count; i++)
                        {
                            if (RecordList[i].RecordCategory.CategoryType)
                            {
                                //AccountAmount -= RecordList[i].RecordAmount;
                                ExpenseNumber += RecordList[i].RecordAmount;
                            }
                            else
                            {
                                //AccountAmount += RecordList[i].RecordAmount;
                                IncomeNumber += RecordList[i].RecordAmount;
                            }
                        }
                        AccountAmount = IncomeNumber - ExpenseNumber;
                    }
                    else
                    {
                        AccountAmount = 0;
                        ExpenseNumber = 0;
                        IncomeNumber = 0;
                    }
                }
                else
                {
                    CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                }
            }
        }

        private void AddColorForRecord(ObservableCollection<Record> resultAsyncData)
        {
            foreach (var recordItem in resultAsyncData)
            {
                if (recordItem.RecordCategory.CategoryType)
                {
                    recordItem.RecordTypeColor = "#FB1B15";
                }
                else
                {
                    recordItem.RecordTypeColor = "#08B621";
                }
            }
        }

        public async void Refresh()
        {
            await GetAccountListAPI();
        }

        private void NextToProfilePage()
        {
            if (ValidationClass.CheckInternet())
            {
                _navigation.PushAsync(new EditProfilePage());
            }
        }

        private void SignOut()
        {
            if (ValidationClass.CheckInternet())
            {
                UserDAO.RemoveUserToken();
                _navigation.PushAsync(new IntroPage());
            }
        }

        private void DeleteAllRecord()
        {
            //var answer = await App.Current.MainPage.DisplayAlert(
            //    "Warning", "Are you want to delete all record ?",
            //    "OK",
            //    "Cancel");
            //if (answer)
            //{
            //    //RecordDAO.DeleteAllRecord();
            //    RecordList.Clear();
            //    CrossToastPopUp.Current.ShowToastSuccess("Delete successful");
            //}
            DataViewAndViewModel.SelectedAccountDropdown = SelectedAccountDropdown;
            if (SelectedAccountDropdown != null)
            {
                _navigation.PushAsync(new ReportPage(_accountSelected));
            }
            else
            {
                CrossToastPopUp.Current.ShowToastError("No account selected");
            }

        }

        private void ExecuteCreateRecord()
        {
            DataViewAndViewModel.SelectedAccountDropdown = SelectedAccountDropdown;
            if(SelectedAccountDropdown != null)
            {
                //_navigation.PushAsync(new InputAmountPage(_accountSelected));
                _navigation.PushPopupAsync(new PopupAddRecord(_accountSelected));
            }
            else
            {
                CrossToastPopUp.Current.ShowToastError("No account selected");
            }
            
        }

        public string ItemIconHome
        {
            get => _itemIconHome;
            set => SetProperty(ref _itemIconHome, value);
        }

        public string ItemTextColorHome
        {
            get => _itemTextColorHome;
            set => SetProperty(ref _itemTextColorHome, value);
        }

        public string ItemIconHomeSize
        {
            get => _itemIconHomeSize;
            set => SetProperty(ref _itemIconHomeSize, value);
        }


        public string ItemIconNew
        {
            get => _itemIconNew;
            set => SetProperty(ref _itemIconNew, value);
        }

        public string ItemTextColorNew
        {
            get => _itemTextColorNew;
            set => SetProperty(ref _itemTextColorNew, value);
        }

        public string ItemIconNewSize
        {
            get => _itemIconNewSize;
            set => SetProperty(ref _itemIconNewSize, value);
        }


        public string ItemIconAccount
        {
            get => _itemIconAccount;
            set => SetProperty(ref _itemIconAccount, value);
        }

        public string ItemTextColorAccount
        {
            get => _itemTextColorAccount;
            set => SetProperty(ref _itemTextColorAccount, value);
        }

        public string ItemIconAccountSize
        {
            get => _itemIconAccountSize;
            set => SetProperty(ref _itemIconAccountSize, value);
        }


        public string ItemIconMore
        {
            get => _itemIconMore;
            set => SetProperty(ref _itemIconMore, value);
        }

        public string ItemTextColorMore
        {
            get => _itemTextColorMore;
            set => SetProperty(ref _itemTextColorMore, value);
        }

        public string ItemIconMoreSize
        {
            get => _itemIconMoreSize;
            set => SetProperty(ref _itemIconMoreSize, value);
        }

        public bool IsVisibleTextNotiSearch
        {
            get => _isVisibleTextNotiSearch;
            set => SetProperty(ref _isVisibleTextNotiSearch, value);
        }

        private void OpenHomeItemPage()
        {
            SetStatus(
                new ItemNavigation() { ItemIconName = "ic_home_active", ItemTextColor = "#5AA6EA", ItemIconSize = "30" },
                new ItemNavigation() { ItemIconName = "ic_add", ItemTextColor = "#7A7A7A", ItemIconSize = "22" },
                new ItemNavigation() { ItemIconName = "ic_account", ItemTextColor = "#7A7A7A", ItemIconSize = "22" },
                new ItemNavigation() { ItemIconName = "ic_settings", ItemTextColor = "#7A7A7A", ItemIconSize = "22" }
                );
        }

        private void OpenNewItemPage()
        {
            SetStatus(
                new ItemNavigation() { ItemIconName = "ic_home", ItemTextColor = "#7A7A7A", ItemIconSize = "22" },
                new ItemNavigation() { ItemIconName = "ic_add_active", ItemTextColor = "#5AA6EA", ItemIconSize = "30" },
                new ItemNavigation() { ItemIconName = "ic_account", ItemTextColor = "#7A7A7A", ItemIconSize = "22" },
                new ItemNavigation() { ItemIconName = "ic_settings", ItemTextColor = "#7A7A7A", ItemIconSize = "22" }
            );
        }

        private void OpenAccountItemPage()
        {
            SetStatus(
                new ItemNavigation() { ItemIconName = "ic_home", ItemTextColor = "#7A7A7A", ItemIconSize = "22" },
                new ItemNavigation() { ItemIconName = "ic_add", ItemTextColor = "#7A7A7A", ItemIconSize = "22" },
                new ItemNavigation() { ItemIconName = "ic_account_active", ItemTextColor = "#5AA6EA", ItemIconSize = "30" },
                new ItemNavigation() { ItemIconName = "ic_settings", ItemTextColor = "#7A7A7A", ItemIconSize = "22" }
            );
        }

        private void OpenMoreItemPage()
        {
            SetStatus(
                new ItemNavigation() { ItemIconName = "ic_home", ItemTextColor = "#7A7A7A", ItemIconSize = "22" },
                new ItemNavigation() { ItemIconName = "ic_add", ItemTextColor = "#7A7A7A", ItemIconSize = "22" },
                new ItemNavigation() { ItemIconName = "ic_account", ItemTextColor = "#7A7A7A", ItemIconSize = "22" },
                new ItemNavigation() { ItemIconName = "ic_settings_active", ItemTextColor = "#5AA6EA", ItemIconSize = "30" }
            );
        }

        private void SetStatus(ItemNavigation itemNavigation1, ItemNavigation itemNavigation2, ItemNavigation itemNavigation3, ItemNavigation itemNavigation4)
        {
            ItemIconHome = itemNavigation1.ItemIconName;
            ItemTextColorHome = itemNavigation1.ItemTextColor;
            ItemIconHomeSize = itemNavigation1.ItemIconSize;

            ItemIconNew = itemNavigation2.ItemIconName;
            ItemTextColorNew = itemNavigation2.ItemTextColor;
            ItemIconNewSize = itemNavigation2.ItemIconSize;

            ItemIconAccount = itemNavigation3.ItemIconName;
            ItemTextColorAccount = itemNavigation3.ItemTextColor;
            ItemIconAccountSize = itemNavigation3.ItemIconSize;

            ItemIconMore = itemNavigation4.ItemIconName;
            ItemTextColorMore = itemNavigation4.ItemTextColor;
            ItemIconMoreSize = itemNavigation4.ItemIconSize;
        }

        private void OpenCategoryList()
        {
            if (ValidationClass.CheckInternet())
            {
                _navigation.PushAsync(new CategoryListPage());
            }
        }

        public ObservableCollection<Record> RecordList
        {
            get => _recordList;
            set => SetProperty(ref _recordList, value);
        }

        public ObservableCollection<Account> AccountList
        {
            get => _accountList;
            set => SetProperty(ref _accountList, value);
        }

        public Account SelectedAccount
        {
            get => _selectedAccount;
            set
            {
                SetProperty(ref _selectedAccount, value);
            }
        }

        public Record RecordSelected
        {
            get => _recordSelected;
            set
            {
                SetProperty(ref _recordSelected, value);
                if (_recordSelected != null)
                {
                    _navigation.PushPopupAsync(new PopupEditRecordPage(RecordSelected));
                }
            }
        }



        public int IncomeNumber
        {
            get => _incomeNumber;
            set => SetProperty(ref _incomeNumber, value);
        }

        public int ExpenseNumber
        {
            get => _expenseNumber;
            set => SetProperty(ref _expenseNumber, value);
        }

        public int AccountAmount
        {
            get => _accountAmount;
            set => SetProperty(ref _accountAmount, value);
        }

        public User CurrentUser { get; set; }

        public Account AccountSelected
        {
            get
            {
                return _accountSelected;
            }
            set
            {
                SetProperty(ref _accountSelected, value);
                if (AccountSelected != null)
                {
                    _navigation.PushAsync(new AccountDetailPage(_homeViewModel, _accountSelected));
                }
            }
        }

        public string SearchTextAccount
        {
            get => _searchTextAccount;
            set
            {
                _accountSelected = null;
                SetProperty(ref _searchTextAccount, value);
                SearchCommandExecute();
            }
        }


        private void SearchCommandExecute()
        {
            ObservableCollection<Account> searchAccountList = new ObservableCollection<Account>();

            string searchTextTemp = SearchTextAccount.Trim();

            var tempAccountList = AccountListTemp.Where(x => x.AccountName.ToLower().Contains(searchTextTemp.ToLower()));

            AccountList.Clear();

            foreach (Account item in tempAccountList)
            {
                searchAccountList.Add(item);
            }
            AccountList = searchAccountList;

            if (AccountList.Count == 0)
            {
                IsVisibleTextNotiSearch = true;
                NotiAccountText = "Sorry, we didn't find any results matching this search";
            }
            else
            {
                IsVisibleTextNotiSearch = false;
            }
        }

    }

    public class ItemNavigation
    {
        public string ItemIconName { get; set; }
        public string ItemTextColor { get; set; }
        public string ItemIconSize { get; set; }
    }
}

