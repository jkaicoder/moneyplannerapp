﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Plugin.Toast;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace MoneyPlannerApp.ViewModels
{
    public class PopupAddCategoryViewModel : ViewModelBase
    {
        private INavigation _navigation;
        private CategoryServices _categoryServices;

        public DelegateCommand SelectIconCommand { get; }
        public DelegateCommand AddCategoryCommand { get; }

        private Category _category;
        private bool _isChecked;
        private string _errorTextCategoryName;
        private string _errorTextCategoryDescription;
        private bool _isCategoryNameError;
        private bool _isCategoryDescriptionError;

        public Category Category
        {
            get => _category;
            set
            {
                SetProperty(ref _category, value);
                IsCategoryNameError = true;
                IsCategoryDescriptionError = true;
            }
        }

        public bool IsCategoryNameError
        {
            get => _isCategoryNameError;
            set => SetProperty(ref _isCategoryNameError, value);
        }

        public bool IsCategoryDescriptionError
        {
            get => _isCategoryDescriptionError;
            set => SetProperty(ref _isCategoryDescriptionError, value);
        }


        public string ErrorTextCategoryName
        {
            get => _errorTextCategoryName;
            set => SetProperty(ref _errorTextCategoryName, value);
        }

        public string ErrorTextCategoryDescription
        {
            get => _errorTextCategoryDescription;
            set => SetProperty(ref _errorTextCategoryDescription, value);
        }

        public PopupAddCategoryViewModel(INavigation navigation)
        {
            _navigation = navigation;
            _categoryServices = new CategoryServices();
            SelectIconCommand = new DelegateCommand(OpenSelectIcon);
            AddCategoryCommand = new DelegateCommand(AddCategory);
            _category = new Category();

        }

        public PopupAddCategoryViewModel(INavigation navigation, Category category)
        {
            _navigation = navigation;
            _categoryServices = new CategoryServices();
            _category = category;

            SelectIconCommand = new DelegateCommand(OpenSelectIcon);
            AddCategoryCommand = new DelegateCommand(AddCategory);

            SetIsChecked();
        }

        private void SetIsChecked()
        {
            if (Category.CategoryType)
            {
                IsChecked = false;
            }
            else
            {
                IsChecked = true;
            }
        }

        public bool IsChecked
        {
            get => _isChecked;
            set => SetProperty(ref _isChecked, value);
        }

        
        

        private bool CheckValidation()
        {
            ArrayList errorList = new ArrayList();

            IsCategoryNameError = string.IsNullOrEmpty(Category.CategoryName);
            IsCategoryDescriptionError = string.IsNullOrEmpty(Category.CategoryDescription);

            string categoryName = Category.CategoryName;
            string categoryDescription = Category.CategoryDescription;

            if (IsCategoryNameError)
            {
                ErrorTextCategoryName = "This field is required";
                errorList.Add(this);
            }
            else
            {
                if (ValidationClass.MinimumString(categoryName, 4))
                {
                    IsCategoryNameError = true;
                    ErrorTextCategoryName = "Please input more than 4 characters";
                    errorList.Add(this);
                }
                else
                {
                    if (ValidationClass.IsHasSpecialCharacters(categoryName))
                    {
                        IsCategoryNameError = true;
                        ErrorTextCategoryName = "Not allow special characters";
                        errorList.Add(this);
                    }
                }
            }

            if (IsCategoryDescriptionError)
            {
                ErrorTextCategoryDescription = "This field is required";
                errorList.Add(this);
            }
            else
            {
                if (ValidationClass.MinimumString(categoryDescription, 4))
                {
                    IsCategoryDescriptionError = true;
                    ErrorTextCategoryDescription = "Please input more than 4 characters";
                    errorList.Add(this);
                }
                else
                {
                    if (ValidationClass.IsHasSpecialCharacters(categoryDescription))
                    {
                        IsCategoryDescriptionError = true;
                        ErrorTextCategoryDescription = "Not allow special characters";
                        errorList.Add(this);
                    }
                }
            }

            if (errorList.Count > 0)
            {
                return false;
            }
            return true;
        }

        private void DataProcess()
        {
            Category.CategoryName = Category.CategoryName.Trim();
            Category.CategoryDescription = Category.CategoryDescription.Trim();
        }

        private async void AddCategory()
        {
            if (CheckValidation())
            {
                DataProcess();
                if (ValidationClass.CheckInternet())
                {
                    var pageLoad = new PopupPage();
                    await _navigation.PushPopupAsync(pageLoad);
                    var resultAsync = await _categoryServices.InsertCategory(_category);
                    TokenServices.CheckToken(resultAsync.StatusCode, _navigation);
                    await _navigation.RemovePopupPageAsync(pageLoad);
                    if (resultAsync.Status)
                    {
                        CrossToastPopUp.Current.ShowToastSuccess(resultAsync.Message);
                        DataViewAndViewModel.CategoryListViewModel.GetCategoryList();
                        await _navigation.RemovePopupPageAsync(DataViewAndViewModel.PopupAddCategoryPage);
                    }
                    else
                    {
                        CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                    }
                }
            }
        }

        private void OpenSelectIcon()
        {
            _navigation.RemovePopupPageAsync(DataViewAndViewModel.PopupAddCategoryPage);
            _navigation.PushAsync(new IconListPage(_category));
        }
    }
}
