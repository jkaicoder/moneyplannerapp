﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Diagnostics;
using System.Text;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Plugin.Toast;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace MoneyPlannerApp.ViewModels
{
    public class PopupAccountRecordListViewModel : ViewModelBase
    {
        private INavigation _navigation;
        private RecordServices _recordServices;
        private ObservableCollection<Record> _recordList;
        private ObservableCollection<Record> _recordListTemp;
        private string _sortByValue;

        public DelegateCommand AccountRecordFilterCommand { get;}

        public ObservableCollection<Record> RecordList
        {
            get => _recordList;
            set => SetProperty(ref _recordList, value);
        }

        public ObservableCollection<Record> RecordListTemp
        {
            get => _recordListTemp;
            set => SetProperty(ref _recordListTemp, value);
        }

        public string SortByValue
        {
            get => _sortByValue;
            set => SetProperty(ref _sortByValue, value);
        }

        public PopupAccountRecordListViewModel(INavigation navigation, Account account)
        {
            _navigation = navigation;
            DataViewAndViewModel.PopupAccountRecordListViewModel = this;
            _recordList = new ObservableCollection<Record>();
            _recordListTemp = new ObservableCollection<Record>();
            _recordServices = new RecordServices();
            SortByValue = "RecordID";
            GetRecordListAPI(account);
            AccountRecordFilterCommand = new DelegateCommand(AccountRecordFilter);
        }

        private void AccountRecordFilter()
        {
            _navigation.PushPopupAsync(new PopupRecordAccountFilterPage(RecordListTemp));
        }

        private async void GetRecordListAPI(Account account)
        {
            if (ValidationClass.CheckInternet())
            {
                var resultAsync = await _recordServices.GetRecordForAccountAsync(account);
                TokenServices.CheckToken(resultAsync.StatusCode, _navigation, DataViewAndViewModel.PopupAccountRecordListPage);
                if (resultAsync.Status)
                {
                    AddColorForRecord(resultAsync.Data);
                    RecordList = resultAsync.Data;

                    foreach (var recordItem in RecordList)
                    {
                        RecordListTemp.Add(recordItem);
                    }
                }
                else
                {
                    CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                }
            }
            
        }

        private void AddColorForRecord(ObservableCollection<Record> resultAsyncData)
        {
            foreach (var recordItem in resultAsyncData)
            {
                if (recordItem.RecordCategory.CategoryType)
                {
                    recordItem.RecordTypeColor = "#FB1B15";
                }
                else
                {
                    recordItem.RecordTypeColor = "#08B621";
                }
            }
        }

        public void RecordListFilter()
        {
            RecordList.Clear();

            foreach (var recordItem in RecordListTemp)
            {
                RecordList.Add(recordItem);
            }

            RecordFilter recordFilter = DataViewAndViewModel.RecordFilter;
            if (recordFilter.Time.Contains("All"))
            {
                if (recordFilter.RecordType.Contains("All"))
                {
                    FilterRecord(recordFilter.SortByUser);
                }
                else if (recordFilter.RecordType.Contains("Income"))
                {
                    FilterRecord(false, recordFilter.SortByUser);
                }
                else
                {
                    FilterRecord(true, recordFilter.SortByUser);
                }
            }
            else
            {
                FilterRecord(true, recordFilter.SortByUser, recordFilter.Time);
            }

        }

        

        private void FilterRecord(string SortByUser)
        {
            foreach (var itemRecordList in RecordListTemp)
            {
                if (SortByUser != null)
                {
                    if (!itemRecordList.RecordUserName.Contains(SortByUser))
                    {
                        RecordList.Remove(itemRecordList);
                    }
                }
            }
        }

        public void FilterRecord(bool recordType, string sortByUser)
        {
            foreach (var itemRecordList in RecordListTemp)
            {
                if (sortByUser == null)
                {
                    if (itemRecordList.RecordCategory.CategoryType != recordType)
                    {
                        RecordList.Remove(itemRecordList);
                    }
                }
                else if (itemRecordList.RecordCategory.CategoryType != recordType || !itemRecordList.RecordUserName.Contains(sortByUser))
                {
                    RecordList.Remove(itemRecordList);
                }
            }
        }

        private void FilterRecord(bool recordType, string sortByUser, string time)
        {
            foreach (var itemRecordList in RecordListTemp)
            {
                if (sortByUser == null)
                {
                    if (itemRecordList.RecordCategory.CategoryType != recordType || DateTime.Now.DayOfYear != itemRecordList.RecordDate.DayOfYear)
                    {
                        RecordList.Remove(itemRecordList);
                    }
                }
                else if (itemRecordList.RecordCategory.CategoryType != recordType || !itemRecordList.RecordUserName.Contains(sortByUser))
                {
                    RecordList.Remove(itemRecordList);
                }
            }
        }
    }
}
