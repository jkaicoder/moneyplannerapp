﻿using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Plugin.Toast;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MoneyPlannerApp.ViewModels
{
    public class ReportViewModel : ViewModelBase
    {
        private INavigation _navigation;
        private string _dayColor = "#5033DE";
        private string _monthColor = "#5033DE";
        private string _yearColor = "#5033DE";

        public string DayMonthYear
        {
            get => _dayMonthYear;
            set
            {
                SetProperty(ref _dayMonthYear, value);
            }
        }
        public string AccountName
        {
            get => _accountName;
            set
            {
                SetProperty(ref _accountName, value);
            }
        }

        private List<CharTest> _charTests;
        private List<CharTest> _charTests2;

        public DelegateCommand DaySelectCommand { get; }
        public DelegateCommand MonthSelectCommand { get; }
        public DelegateCommand YearSelectCommand { get; }

        private ReportService _reportService;
        private ObservableCollection<Report> _listReport;
        private string _accountName;
        private string _dayMonthYear;
        private ObservableCollection<Report> _listReportDay;
        private ObservableCollection<Report> _listReportMonth;

        public ObservableCollection<Report> ListReportMonth
        {
            get => _listReportMonth;
            set
            {
                SetProperty(ref _listReportMonth, value);
            }
        }
        public ObservableCollection<Report> ListReportDay
        {
            get => _listReportDay;
            set
            {
                SetProperty(ref _listReportDay, value);
            }
        }
        public ObservableCollection<Report> ListReport
        {
            get => _listReport;
            set
            {
                SetProperty(ref _listReport, value);
            }
        }

        public List<CharTest> CharTests
        {
            get => _charTests;
            set
            {
                SetProperty(ref _charTests, value);
            }
        }
        public List<CharTest> CharTests2
        {
            get => _charTests2;
            set
            {
                SetProperty(ref _charTests2, value);
            }
        }

        public string YearColor
        {
            get => _yearColor;
            set
            {
                SetProperty(ref _yearColor, value);
            }
        }
        public string MonthColor
        {
            get => _monthColor;
            set
            {
                SetProperty(ref _monthColor, value);
            }
        }

        public string DayColor
        {
            get => _dayColor;
            set
            {
                SetProperty(ref _dayColor, value);
            }
        }




        public ReportViewModel(INavigation navigation, Account _accountSelected)
        {
            _navigation = navigation;
            _reportService = new ReportService();
            AccountName = "Account : " + DataViewAndViewModel.SelectedAccountDropdown.AccountName;

            DaySelectCommand = new DelegateCommand(ExecuteDaySelectAsync);
            MonthSelectCommand = new DelegateCommand(ExecuteMonthSelect);
            YearSelectCommand = new DelegateCommand(ExecuteYearSelect);

            ExecuteDaySelectAsync();



        }

        private void ExecuteYearSelect()
        {
            DayColor = "#5033DE";
            MonthColor = "#5033DE";
            YearColor = "#9B59B6"; //Change color Button Year
            DayMonthYear = "Year";
            CharTests = new List<CharTest>()
            {
                new CharTest { Name = "2016", Height = 80 },
                new CharTest { Name = "2017", Height = 70 },
                new CharTest { Name = "2018", Height = 40 },
                new CharTest { Name = "2019", Height = 82 }
            };

            CharTests2 = new List<CharTest>()
            {
                new CharTest { Name = "2016", Height = 90 },
                new CharTest { Name = "2017", Height = 50 },
                new CharTest { Name = "2018", Height = 20 },
                new CharTest { Name = "2019", Height = 182 }
            };
        }

        private void ExecuteMonthSelect()
        {
            DayColor = "#5033DE";
            MonthColor = "#9B59B6"; //Change color Button Month
            YearColor = "#5033DE";
            DayMonthYear = "Month";
            GetReportByMonth();
        }

        private void ExecuteDaySelectAsync()
        {
            
            DayColor = "#9B59B6"; //Change color Button Day
            MonthColor = "#5033DE";
            YearColor = "#5033DE";
            DayMonthYear = "Day";
            GetReportByDay();
            

        }
        public async void GetReportByDay()
        {
            if (ValidationClass.CheckInternet())
            {

                var page = new PopupPage();
                await _navigation.PushPopupAsync(page);

                var resultAsync = await _reportService.GetReportTypeDayAsync(DataViewAndViewModel.SelectedAccountDropdown);

                await _navigation.RemovePopupPageAsync(page);

                if (resultAsync.Status)
                {
                    ListReport = resultAsync.Data;
                    Debug.Write(ListReport[1].DateString);
                    foreach (var item in ListReport)
                    {
                        string[] word = item.DateString.Split(' ');
                        item.DateString = word[0];
                    }

                }
                else
                {
                    CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                }
            }
        }
        public async void GetReportByMonth()
        {
            if (ValidationClass.CheckInternet())
            {
                var page = new PopupPage();
                await _navigation.PushPopupAsync(page);

                var resultAsync = await _reportService.GetReportTypeMonthAsync(DataViewAndViewModel.SelectedAccountDropdown);

                await _navigation.RemovePopupPageAsync(page);

                if (resultAsync.Status)
                {
                    ListReport = resultAsync.Data;
                    Debug.Write(ListReport[1].DateString);
                    foreach (var item in ListReport)
                    {
                        string[] word = item.DateString.Split(' ');
                        item.DateString = word[1];
                    }

                }
                else
                {
                    CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                }
            }
        }
    }
}
