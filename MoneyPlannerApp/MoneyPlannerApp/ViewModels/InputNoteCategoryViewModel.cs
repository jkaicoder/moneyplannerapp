﻿using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using Xamarin.Forms;

namespace MoneyPlannerApp.ViewModels
{
    public class InputNoteCategoryViewModel : ViewModelBase
    {
        private INavigation _navigation;
        private Record _record;
        private bool _isCreateRecord;
        private bool _isDescriptionNull;

        public bool IsDescriptionNull
        {
            get => _isDescriptionNull;
            set
            {
                SetProperty(ref _isDescriptionNull, value);
            }
        }
        public DelegateCommand NextCommand { get; }

        public Record RecordTest
        {
            get => _record;
            set
            {
                SetProperty(ref _record, value);
            }
        }

        public InputNoteCategoryViewModel(INavigation navigation, Record record, bool isCreateRecord)
        {
            _navigation = navigation;
            _record = record;
            _isCreateRecord = isCreateRecord;
            NextCommand = new DelegateCommand(NextScreen);
            RecordTest.RecordDate = DateTime.Now;

        }

        private void NextScreen()
        {
            IsDescriptionNull = string.IsNullOrEmpty(_record.RecordDescription);
            if(!IsDescriptionNull)
            {
                _record.RecordDescription = _record.RecordDescription.Trim();
            }
            
            _navigation.PushAsync(new SummaryPage(_record, _isCreateRecord));
        }
    }
}
