﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels.Base;
using Plugin.Toast;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Xamarin.Forms;

namespace MoneyPlannerApp.ViewModels
{
    public class ForgotPasswodViewModel : ViewModelBase
    {
        private INavigation _navigation;
        private UserServices _userServices;
        private string _email;
        private bool _isEmailError;
        private string _emailErrorText;
        private bool _isVisibleEmail;
        private bool _isVisibleCode;
        private bool _isVisibleNewPassword;
        private bool _isVisibleConfirmNewPassword;
        private bool _isVisibleAgainButton;
        private string _code;
        private string _newPassword;
        private string _confirmNewPassword;
        private bool _isCodeError;
        private string _codeErrorText;
        private bool _isNewPasswordError;
        private string _newPasswordErrorText;
        private bool _isConfirmNewPasswordError;
        private string _confirmNewPasswordErrorText;
        private bool _isVisibleSendButton;
        private bool _isVisibleResetButton;
        private bool _isVisibleTip;

        public DelegateCommand SendCommand { get; }

        public DelegateCommand ReSendCommand { get; }

        public DelegateCommand ResetCommand { get; }

        public DelegateCommand SendAgainCommand { get; }

        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        public string Code
        {
            get => _code;
            set => SetProperty(ref _code, value);
        }

        public string NewPassword
        {
            get => _newPassword;
            set => _newPassword = value;
        }

        public string ConfirmNewPassword
        {
            get => _confirmNewPassword;
            set => SetProperty(ref _confirmNewPassword, value);
        }

        public bool IsEmailError
        {
            get => _isEmailError;
            set => SetProperty(ref _isEmailError, value);
        }

        public string EmailErrorText
        {
            get => _emailErrorText;
            set => SetProperty(ref _emailErrorText, value);
        }

        public bool IsCodeError
        {
            get => _isCodeError;
            set => SetProperty(ref _isCodeError, value);
        }

        public string CodeErrorText
        {
            get => _codeErrorText;
            set => SetProperty(ref _codeErrorText, value);
        }

        public bool IsNewPasswordError
        {
            get => _isNewPasswordError;
            set => SetProperty(ref _isNewPasswordError, value);
        }

        public string NewPasswordErrorText
        {
            get => _newPasswordErrorText;
            set => SetProperty(ref _newPasswordErrorText, value);
        }

        public bool IsConfirmNewPasswordError
        {
            get => _isConfirmNewPasswordError;
            set => SetProperty(ref _isConfirmNewPasswordError, value);
        }

        public string ConfirmNewPasswordErrorText
        {
            get => _confirmNewPasswordErrorText;
            set => SetProperty(ref _confirmNewPasswordErrorText, value);
        }


        public bool IsVisibleEmail
        {
            get => _isVisibleEmail;
            set => SetProperty(ref _isVisibleEmail, value);
        }

        public bool IsVisibleCode
        {
            get => _isVisibleCode;
            set => SetProperty(ref _isVisibleCode, value);
        }

        public bool IsVisibleNewPassword
        {
            get => _isVisibleNewPassword;
            set => SetProperty(ref _isVisibleNewPassword, value);
        }

        public bool IsVisibleConfirmNewPassword
        {
            get => _isVisibleConfirmNewPassword;
            set => SetProperty(ref _isVisibleConfirmNewPassword, value);
        }

        public bool IsVisibleAgainButton
        {
            get => _isVisibleAgainButton;
            set => SetProperty(ref _isVisibleAgainButton, value);
        }

        public bool IsVisibleSendButton
        {
            get => _isVisibleSendButton;
            set => SetProperty(ref _isVisibleSendButton, value);
        }

        public bool IsVisibleResetButton
        {
            get => _isVisibleResetButton;
            set => SetProperty(ref _isVisibleResetButton, value);
        }

        public bool IsVisibleTip
        {
            get => _isVisibleTip;
            set => SetProperty(ref _isVisibleTip, value);
        }

        public ForgotPasswodViewModel(INavigation navigation)
        {
            _navigation = navigation;
            _userServices = new UserServices();
            IsVisibleEmail = true;
            IsVisibleCode = false;
            IsVisibleNewPassword = false;
            IsVisibleConfirmNewPassword = false;
            IsVisibleSendButton = true;
            IsVisibleAgainButton = false;
            IsVisibleResetButton = false;
            IsVisibleTip = true;

            SendCommand = new DelegateCommand(SendEmail);
            ReSendCommand = new DelegateCommand(ReSendEmail);
            SendAgainCommand = new DelegateCommand(SendAgain);
            ResetCommand = new DelegateCommand(Reset);
        }

        private void ReSendEmail()
        {
            SendEmail();
        }

        private async void Reset()
        {
            string newPasswordTemp = _newPassword;
            if (CheckValidationForReset())
            {
                _newPassword = SecurePasswordHasher.Hash(_newPassword);
                ForgetPassword resetPassword = new ForgetPassword() { Email = _email, Code = _code, NewPassword = _newPassword};
                if (ValidationClass.CheckInternet())
                {
                    var page = new PopupPage();
                    await _navigation.PushPopupAsync(page);
                    var resultAsync = await _userServices.ResetPassword(resetPassword);
                    await _navigation.RemovePopupPageAsync(page);
                    if (resultAsync.Status)
                    {
                        CrossToastPopUp.Current.ShowToastSuccess(resultAsync.Message);
                    }
                    else
                    {
                        _newPassword = newPasswordTemp;
                        CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                    }
                }
            }
        }

        private void SendAgain()
        {
            IsVisibleEmail = true;
            IsVisibleCode = false;
            IsVisibleNewPassword = false;
            IsVisibleConfirmNewPassword = false;
            IsVisibleAgainButton = false;
            IsVisibleSendButton = true;
            IsVisibleResetButton = false;
            IsVisibleTip = true;
        }


        private bool CheckValidation()
        {
            ArrayList errorListOne = new ArrayList();

     


            //Email
            if (ValidationClass.IsNullOrEmpty(_email))
            {
                IsEmailError = true;
                EmailErrorText = "This field is required";
                errorListOne.Add(this);
            }
            else
            {
                IsEmailError = false;
                if (!ValidationClass.IsValidEmail(_email))
                {
                    IsEmailError = true;
                    EmailErrorText = "The format of Email is incorrect";
                    errorListOne.Add(this);
                }
                else
                {
                    IsEmailError = false;
                }
            }

            if (errorListOne.Count > 0)
            {
                return false;
            }

            return true;

        }

        private bool CheckValidationForReset()
        {
            ArrayList errorListTwo = new ArrayList();

            //Code

            if (ValidationClass.IsNullOrEmpty(_code))
            {
                IsCodeError = true;
                CodeErrorText = "This field is required";
                errorListTwo.Add(this);
            }
            else
            {
                IsCodeError = false;
            }


            ////New password

            if (ValidationClass.IsNullOrEmpty(_newPassword))
            {
                IsNewPasswordError = true;
                NewPasswordErrorText = "This field is required";
                errorListTwo.Add(this);
            }
            else
            {
                IsNewPasswordError = false;
                if (ValidationClass.MinimumString(_newPassword, 6))
                {
                    IsNewPasswordError = true;
                    NewPasswordErrorText = "Please input more than 6 characters";
                    errorListTwo.Add(this);
                }
                else
                {
                    IsNewPasswordError = false;
                }
            }

            //Confirm new password

            if (ValidationClass.IsNullOrEmpty(_confirmNewPassword))
            {
                IsConfirmNewPasswordError = true;
                ConfirmNewPasswordErrorText = "This field is required";
                errorListTwo.Add(this);
            }
            else
            {
                IsConfirmNewPasswordError = false;
                if (!String.Equals(_confirmNewPassword, _newPassword))
                {
                    IsConfirmNewPasswordError = true;
                    ConfirmNewPasswordErrorText = "Password does not match";
                    errorListTwo.Add(this);
                }
                else
                {
                    IsConfirmNewPasswordError = false;
                }
            }

            if (errorListTwo.Count > 0)
            {
                return false;
            }

            return true;
        }

        private async void SendEmail()
        {
            if (ValidationClass.CheckInternet())
            {
                if (CheckValidation())
                {
                    ForgetPassword forgetPassword = new ForgetPassword() { Email = _email };
                   
                    var page = new PopupPage();
                    await _navigation.PushPopupAsync(page);
                    var resultAsync = await _userServices.ForgetPassword(forgetPassword);
                    await _navigation.RemovePopupPageAsync(page);
                    if (resultAsync.Status)
                    {
                        StatusAfterSendEmail();
                        CrossToastPopUp.Current.ShowToastSuccess(resultAsync.Message);
                    }
                    else
                    {
                        CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                    }
                }
            }
        }

        private void StatusAfterSendEmail()
        {
            IsVisibleEmail = false;
            IsVisibleCode = true;
            IsVisibleNewPassword = true;
            IsVisibleConfirmNewPassword = true;
            IsVisibleSendButton = false;
            IsVisibleAgainButton = true;
            IsVisibleResetButton = true;
            IsVisibleTip = false;
        }
    }

    public class ForgetPassword
    {
        public string Email { get; set; }
        public string Code { get; set; }
        public string NewPassword { get; set; }
    }
}
