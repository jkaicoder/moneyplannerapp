﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Plugin.Toast;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace MoneyPlannerApp.ViewModels
{
    public class PopupAccountUserLevelViewModel : ViewModelBase
    {
        private INavigation _navigation;
        private AccountServices _accountServices;
        private User _user;
        private Account _account;
        private bool _isCheckedAdmin;
        private bool _isCheckedMember;

        public DelegateCommand UpdateCommand { get; }
        public DelegateCommand RemoveCommand { get; }

        public bool IsCheckedAdmin
        {
            get => _isCheckedAdmin;
            set => SetProperty(ref _isCheckedAdmin, value);
        }

        public bool IsCheckedMember
        {
            get => _isCheckedMember;
            set => SetProperty(ref _isCheckedMember, value);
        }

        public PopupAccountUserLevelViewModel(INavigation navigation, Account account, User user)
        {
            _navigation = navigation;
            _accountServices = new AccountServices();
            _account = account;
            _user = user;

            GetLevelAccountMember();

            UpdateCommand = new DelegateCommand(Update);
            RemoveCommand = new DelegateCommand(Remove);
        }

        private async void Remove()
        {
            if (ValidationClass.CheckInternet())
            {
                var page = new PopupPage();
                await _navigation.PushPopupAsync(page);
                var resultAsync = await _accountServices.RemoveUserFromAccount(new RemoveUserAccount()
                {
                    UserID = UserDAO.GetUserToken().Last().UserID,
                    AccountID = _account.AccountID,
                    UserIDRemove = _user.UserID
                });
                await _navigation.RemovePopupPageAsync(page);
                TokenServices.CheckToken(resultAsync.StatusCode, _navigation);
                if (resultAsync.Status)
                {
                    _navigation.RemovePopupPageAsync(DataViewAndViewModel.PopupAccountUserLevelPage);
                    DataViewAndViewModel.PopupAccountUserListViewModel.Refresh();
                    CrossToastPopUp.Current.ShowToastSuccess(resultAsync.Message);
                }
                else
                {
                    CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                }
            }
        }

        private async void Update()
        {
            if (ValidationClass.CheckInternet())
            {
                var page = new PopupPage();
                await _navigation.PushPopupAsync(page);
                var resultAsync = await _accountServices.SetLevelAccountMember(new InfoID()
                {
                    UserID = UserDAO.GetUserToken().Last().UserID,
                    AccountID = _account.AccountID,
                    UserIDEdit = _user.UserID,
                    Role = GetLevelToUpdate()
                });
                await _navigation.RemovePopupPageAsync(page);
                TokenServices.CheckToken(resultAsync.StatusCode, _navigation);
                if (resultAsync.Status)
                {
                    _navigation.RemovePopupPageAsync(DataViewAndViewModel.PopupAccountUserLevelPage);
                    DataViewAndViewModel.PopupAccountUserListViewModel.Refresh();
                    CrossToastPopUp.Current.ShowToastSuccess(resultAsync.Message);
                }
                else
                {
                    CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                }
            }
        }

        private int GetLevelToUpdate()
        {
            if (IsCheckedAdmin)
            {
                return 2;
            }

            return 3;
        }

        private void GetLevelAccountMember()
        {
            if (_user.OwnerAccount == 2)
            {
                IsCheckedAdmin = true;
            }
            else
            {
                IsCheckedMember = true;
            }
        }
    }
}
