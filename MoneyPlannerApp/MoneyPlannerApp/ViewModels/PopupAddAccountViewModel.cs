﻿using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.Services;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Plugin.Toast;
using System.Diagnostics;
using System.Collections;

namespace MoneyPlannerApp.ViewModels
{
    public class PopupAddAccountViewModel : ViewModelBase
    {
        private Account _account;
        public DelegateCommand AddAccountCommand { get; }
        public DelegateCommand CancelCommand { get; }
        public DelegateCommand SelectIconCommand { get; }
        private AccountServices _accountServices;
        INavigation _navigation;
        PopupAddAccountPage _popupAddAccountPage;
        private bool _isAccountNameEmpry;
        private string _errorText;

        public string ErrorText
        {
            get => _errorText;
            set
            {
                SetProperty(ref _errorText, value);
            }
        }

        public bool IsAccountNameEmpry
        {
            get => _isAccountNameEmpry;
            set
            {
                SetProperty(ref _isAccountNameEmpry, value);
            }
        }

        public Account Account
        {
            get => _account;
            set
            {
                SetProperty(ref _account, value);
            }
        }

        public PopupAddAccountViewModel(INavigation navigation, PopupAddAccountPage popupAddAccountPage)
        {
            _popupAddAccountPage = popupAddAccountPage;
            _accountServices = new AccountServices();
            _navigation = navigation;
            _account = new Account();
            Account.AccountAmount = 0;
            Account.AccountIcon = "upload";
            AddAccountCommand = new DelegateCommand(ExecuteAddAccount);
            CancelCommand = new DelegateCommand(ExecuteCancel);
            SelectIconCommand = new DelegateCommand(ExeuteSelectIcon);
        }

        public PopupAddAccountViewModel(INavigation navigation, PopupAddAccountPage popupAddAccountPage, Account account)
        {
            _popupAddAccountPage = popupAddAccountPage;
            _accountServices = new AccountServices();
            _navigation = navigation;
            Account = account;
            AddAccountCommand = new DelegateCommand(ExecuteAddAccount);
            CancelCommand = new DelegateCommand(ExecuteCancel);
            SelectIconCommand = new DelegateCommand(ExeuteSelectIcon);
        }

        private void ExeuteSelectIcon()
        {
            _navigation.RemovePopupPageAsync(_popupAddAccountPage);
            _navigation.PushAsync(new IconListPage(_account, 1));
        }

        private void ExecuteCancel()
        {
            _navigation.RemovePopupPageAsync(_popupAddAccountPage);
        }

        private async void ExecuteAddAccount()
        {
            if (ValidationClass.CheckInternet())
            {

                CheckValidation();
                if (CheckValidation())
                {
                    ProcessAccount();
                    await ProcessAPIAsync();
                }
                else
                {
                    //CrossToastPopUp.Current.ShowToastError("Account name is empty");
                }
            }

        }

        public void ProcessAccount()
        {
            _account.AccountName = _account.AccountName.Trim();
            _account.AccountDate = DateTime.Now;

        }

        public async System.Threading.Tasks.Task ProcessAPIAsync()
        {
            if (ValidationClass.CheckInternet())
            {
                var page = new PopupPage();
                await _navigation.PushPopupAsync(page);

                var resultAsync = await _accountServices.InsertAccountAsync(_account);

                await _navigation.RemovePopupPageAsync(page);

                TokenServices.CheckToken(resultAsync.StatusCode, _navigation);

                if (resultAsync.Status)
                {
                    CrossToastPopUp.Current.ShowToastSuccess(resultAsync.Message);
                    await _navigation.RemovePopupPageAsync(_popupAddAccountPage);
                    DataViewAndViewModel.HomeViewModel.Refresh();
                    DataViewAndViewModel.HomeViewModel.GetTotalAccountRecordAPI();
                }
                else
                {
                    CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
                }
            }
        }

        private bool CheckValidation()
        {
            ArrayList errorList = new ArrayList();

            IsAccountNameEmpry = string.IsNullOrWhiteSpace(Account.AccountName);


            string recordName = Account.AccountName;

            if (IsAccountNameEmpry)
            {
                ErrorText = "This field is required";
                errorList.Add(this);
            }
            else
            {
                if (string.IsNullOrWhiteSpace(recordName) || ValidationClass.StartSpaces(recordName) || ValidationClass.EndSpaces(recordName))
                {
                    IsAccountNameEmpry = true;
                    ErrorText = "Not allow spaces characters";
                    errorList.Add(this);
                }
                else
                {
                    if (ValidationClass.MinimumString(recordName, 4))
                    {
                        IsAccountNameEmpry = true;
                        ErrorText = "Requires more than 4 characters";
                        errorList.Add(this);
                    }
                    else
                    {
                        if (ValidationClass.IsHasSpecialCharacters(recordName))
                        {
                            IsAccountNameEmpry = true;
                            ErrorText = "Not allow special characters";
                            errorList.Add(this);
                        }
                    }
                }
            }

            if (errorList.Count > 0)
            {
                return false;
            }
            return true;
        }
    }
}
