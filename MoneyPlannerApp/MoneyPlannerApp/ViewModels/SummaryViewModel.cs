﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Helper;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.ViewModels.Base;
using Xamarin.Forms;
using Plugin.Toast;
using MoneyPlannerApp.Views;
using SQLiteNetExtensions.Extensions;
using MoneyPlannerApp.Services;
using System.Threading.Tasks;

namespace MoneyPlannerApp.ViewModels
{
    public class SummaryViewModel : ViewModelBase
    {
        private INavigation _navigation;
        private Record _record;
        private string _tempCategoryType;
        private Category _category;
        public DelegateCommand OkCommand { get; }
        public DelegateCommand CancelCommand { get; }
        private bool _isCreateRecord;
        private string _color;
        private RecordServices _recordServices = new RecordServices();

        public string Color
        {
            get => _color;
            set
            {
                SetProperty(ref _color, value);
            }
        }

        public string TempCategoryType
        {
            get => _tempCategoryType;
            set
            {
                SetProperty(ref _tempCategoryType, value);

            }
        }

        public Record Record
        {
            get => _record;
            set
            {
                SetProperty(ref _record, value);
            }
        }

        public SummaryViewModel(INavigation navigation, Record record, bool isCreateRecord)
        {
            _navigation = navigation;
            _record = record;
            _isCreateRecord = isCreateRecord;
            OkCommand = new DelegateCommand(ExecuteOkCommand);
            CancelCommand = new DelegateCommand(ExeCancelCommand);
            Debug.Write("123" + _record.RecordCategory.CategoryType);

            if (_record.RecordCategory.CategoryType)
            {
                Color = "#E74C3C";
                TempCategoryType = "Expanse";
            }
            else
            {
                TempCategoryType = "Income";
                Color = "#2ECC71";
            }


        }
        private async void ExecuteOkCommand()
        {
            if (ValidationClass.CheckInternet())
            {
                if (_isCreateRecord)
                {
                    await ProcessPostRecordAsync();
                }
                else
                {
                    await ProcessPutRecordAsync();
                }
            }
            
            //await ProcessPostRecordAsync();

            //This is code in local
            //if (_isCreateRecord)
            //{
            //    if (RecordDAO.AddRecord(_record))
            //    {
            //        _navigation.PushAsync(new HomePage());
            //        CrossToastPopUp.Current.ShowToastMessage("Add successful");
            //    }
            //    else
            //    {
            //        CrossToastPopUp.Current.ShowToastMessage("Somethings wrong");
            //    }
            //}
            //else
            //{
            //    if (RecordDAO.UpdateRecord(_record))
            //    {
            //        _navigation.PushAsync(new HomePage());
            //        CrossToastPopUp.Current.ShowToastMessage("Update successful");
            //    }
            //    else
            //    {
            //        CrossToastPopUp.Current.ShowToastMessage("Somethings wrong");
            //    }
            //}


        }

        private async Task ProcessPostRecordAsync()
        {
            DataProcess();

            var resultAsync = await _recordServices.InsertRecordAsync(_record);

            if (resultAsync.Status)
            {
                CrossToastPopUp.Current.ShowToastSuccess(resultAsync.Message);
                await _navigation.PushAsync(new HomePage());
            }
            else
            {
                CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
            }
        }


        private async Task ProcessPutRecordAsync()
        {
            DataProcess();
            var resultAsync = await _recordServices.UpdateRecordAsync(_record);

            if (resultAsync.Status)
            {
                CrossToastPopUp.Current.ShowToastSuccess(resultAsync.Message);
                await _navigation.PushAsync(new HomePage());
            }
            else
            {
                CrossToastPopUp.Current.ShowToastError(resultAsync.Message);
            }
        }

        private void DataProcess()
        {
            _record.RecordName = _record.RecordName.Trim();

        }

        private void ExeCancelCommand()
        {
            OnAlertYesNoClicked();

        }

        private async void OnAlertYesNoClicked()
        {
            var answer = await App.Current.MainPage.DisplayAlert(
                "Warning", "Are you sure to cancel?",
                "OK",
                "Cancel");
            if (answer)
            {
                await _navigation.PushAsync(new HomePage());
            }
        }


    }
}
