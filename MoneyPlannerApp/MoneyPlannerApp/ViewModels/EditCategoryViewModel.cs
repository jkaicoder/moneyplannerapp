﻿using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.MVVM.Commands;
using MoneyPlannerApp.ViewModels.Base;
using MoneyPlannerApp.Views;
using Plugin.Toast;
using Xamarin.Forms;

namespace MoneyPlannerApp.ViewModels
{
    public class EditCategoryViewModel : ViewModelBase
    {
        private INavigation _navigation;
        private Category _category;

        public DelegateCommand SelectIconCommand { get; }
        public DelegateCommand DeleteCategoryCommand { get; }
        public DelegateCommand UpdateCategoryCommand { get; }

        private bool _isCategoryNameEmpty;

        public EditCategoryViewModel(INavigation navigation, Category category)
        {
            _navigation = navigation;
            _category = category;

            SelectIconCommand = new DelegateCommand(OpenSelectIcon);
            DeleteCategoryCommand = new DelegateCommand(DeleteCategory);
            UpdateCategoryCommand = new DelegateCommand(UpdateCategory);
        }

        public Category Category
        {
            get => _category;
            set
            {
                SetProperty(ref _category, value);
                IsCategoryNameEmpty = true;
            }
        }

        public bool IsCategoryNameEmpty
        {
            get => _isCategoryNameEmpty;
            set => SetProperty(ref _isCategoryNameEmpty, value);
        }

        private void OpenSelectIcon()
        {
            bool flag = true;
            _navigation.PushAsync(new IconListPage(_category, flag));
        }

        private void UpdateCategory()
        {
            IsCategoryNameEmpty = string.IsNullOrEmpty(Category.CategoryName);

            if (!IsCategoryNameEmpty)
            {
                CategoryDAO.UpdateCategory(_category);
                CrossToastPopUp.Current.ShowToastSuccess("Update successful");
                _navigation.PushAsync(new CategoryListPage());
            }
        }

        private async void DeleteCategory()
        {
            var answer = await App.Current.MainPage.DisplayAlert("Warning", "Are you want to delete this category ?", "OK",
                "Cancel");
            if (answer)
            {
                CategoryDAO.DeleteCategory(_category);
                CrossToastPopUp.Current.ShowToastSuccess("Delete successful");
                _navigation.PushAsync(new CategoryListPage());
            }
        }
        
    }
}
