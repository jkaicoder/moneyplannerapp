﻿using MoneyPlannerApp.Views;
using System;
using System.Diagnostics;
using System.Linq;
using FormsControls.Base;
using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Helper;
using MoneyPlannerApp.Models;
using MoneyPlannerApp.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace MoneyPlannerApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            //DBConnection._dbConnection.CreateTable<Category>();
            //DBConnection._dbConnection.CreateTable<Record>();
            DBConnection._dbConnection.CreateTable<UserToken>();

            if (CheckRememberMe())
            {
                MainPage = new AnimationNavigationPage(new HomePage());
            }
            else
            {
                MainPage = new AnimationNavigationPage(new IntroPage());
            }

            //DBConnection._dbConnection.DeleteAll<UserToken>();



        }

        public bool CheckRememberMe()
        {
            
            int countList = UserDAO.GetUserToken().Count();

            if (countList != 0)
            {
                if (UserDAO.GetUserToken().Last().RememberMe)
                {
                    return true;
                }
                return false;
            }
            return false;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
