﻿using SQLite;

namespace MoneyPlannerApp.Helper
{
    public static class DBConnection
    {
        private static string _dbPath;
        public static SQLiteConnection _dbConnection;

        static DBConnection()
        {
            _dbPath = ConnectionDBPath.DBPath();
            _dbConnection = new SQLiteConnection(_dbPath);
        }
    }
}
