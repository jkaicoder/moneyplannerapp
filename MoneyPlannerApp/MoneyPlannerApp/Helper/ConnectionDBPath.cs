﻿using System.IO;

namespace MoneyPlannerApp.Helper
{
    public static class ConnectionDBPath
    {
        public static string DBPath()
        {
            return Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "myDB.db3");
        }
    }
}
