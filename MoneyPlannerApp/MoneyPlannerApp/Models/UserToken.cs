﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace MoneyPlannerApp.Models
{
    [Table("UserToken")]
    public class UserToken
    {
        [PrimaryKey]
        public string UserID { get; set; }
        public string Token { get; set; }
        public bool RememberMe { get; set; }
    }
}
