﻿using System;

namespace MoneyPlannerApp.Models
{
    public class Password
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}