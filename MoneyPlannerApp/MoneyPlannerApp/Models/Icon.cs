﻿using MoneyPlannerApp.ViewModels.Base;

namespace MoneyPlannerApp.Models
{
    public class Icon : ViewModelBase
    {
        private string _iconUrl;

        public string IconURL
        {
            get => _iconUrl;
            set { SetProperty(ref _iconUrl, value); }
        }
    }
}
