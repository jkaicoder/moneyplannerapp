﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyPlannerApp.Models
{
    public class RemoveUserAccount
    {
        public string UserID { get; set; }
        public string AccountID { get; set; }
        public string UserIDRemove { get; set; }
    }
}
