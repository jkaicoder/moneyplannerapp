﻿using System;
using System.Collections.ObjectModel;
using MoneyPlannerApp.ViewModels.Base;

namespace MoneyPlannerApp.Models
{
    public class Account : ViewModelBase
    {
        private string _accountName;
        private ObservableCollection<Record> _accountRecordList;
        private ObservableCollection<User> _accountUserList;
        private string _accountIcon;
        private int _accountAmount;
        private DateTime _accountDate;
        private User _accountUser;

        public string AccountID { get; set; }

        public string AccountName
        {
            get => _accountName;
            set => SetProperty(ref _accountName, value);
        }

        public int AccountAmount
        {
            get => _accountAmount;
            set => SetProperty(ref _accountAmount, value);
        }

        public DateTime AccountDate
        {
            get => _accountDate;
            set => SetProperty(ref _accountDate, value);
        }

        public ObservableCollection<Record> AccountRecordList
        {
            get => _accountRecordList;
            set => SetProperty(ref _accountRecordList, value);
        }

        public ObservableCollection<User> AccountUserList
        {
            get => _accountUserList;
            set => SetProperty(ref _accountUserList, value);
        }

        public string AccountIcon
        {
            get => _accountIcon;
            set => SetProperty(ref _accountIcon, value);
        }

        public User AccountUser
        {
            get => _accountUser;
            set => SetProperty(ref _accountUser, value);
        }

    }
}
