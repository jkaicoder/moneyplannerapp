﻿using MoneyPlannerApp.ViewModels.Base;
using SQLite;

namespace MoneyPlannerApp.Models
{
    [Table("Categorizes")]
    public class Category : ViewModelBase
    {
        private string _categoryIcon = "upload.png";
        private bool _categoryType = true;

        [PrimaryKey, AutoIncrement]
        public string CategoryID { get; set; }

        public string CategoryName { get; set; }

        public string CategoryDescription { get; set; }

        public string CategoryIcon
        {
            get => _categoryIcon;
            set => _categoryIcon = value;
        }

        public bool CategoryType
        {
            get => _categoryType;
            set
            {
                SetProperty(ref _categoryType, value);
            }
        }
    }
}
