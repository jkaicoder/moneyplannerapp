﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyPlannerApp.Models
{
    public class Report
    {
        public DateTime Date { get; set; }
        public string DateString { get; set; }
        public double Income { get; set; }
        public double Expense { get; set; }
    }
}
