﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyPlannerApp.Models
{
    public class RecordFilter
    {
        public string SortByUser { get; set; }
        public string RecordType { get; set; }
        public string Time { get; set; }
    }
}
