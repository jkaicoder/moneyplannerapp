
﻿using System;
using MoneyPlannerApp.ViewModels.Base;

namespace MoneyPlannerApp.Models
{
    public class User : ViewModelBase
    {
        private string _job;
        private string _userName;
        private string _address;
        private string _firstName;
        private string _lastName;
        private string _email;
        private int _gender;
        private string _phoneNumber;
        private DateTime _birthDay;

        public string UserID { get; set; }

        public string UserName
        {
            get => _userName;
            set => SetProperty(ref _userName, value);
        }

        public string Password { get; set; }
        public DateTime CreatedDate { get; set; }
        public string FirstName
        {
            get => _firstName;
            set
            {
                SetProperty(ref _firstName, value);
            }
        }
        public string LastName
        {
            get => _lastName;
            set
            {
                SetProperty(ref _lastName, value);
            }
        }
        public string Address
        {
            get => _address;
            set
            {
                SetProperty(ref _address, value);
            }
        }
        public string Email
        {
            get => _email;
            set
            {
                SetProperty(ref _email, value);
            }
        }
        public string Job
        {
            get => _job;
            set
            {
                SetProperty(ref _job, value);
            }
        }
        public int Gender
        {
            get => _gender;
            set
            {
                SetProperty(ref _gender, value);
            }
        }
        public int UserLevel { get; set; }
        public string PhoneNumber
        {
            get => _phoneNumber;
            set
            {
                SetProperty(ref _phoneNumber, value);
            }
        }
        public string UserImage { get; set; }
        public DateTime BirthDay
        {
            get => _birthDay;
            set
            {
                SetProperty(ref _birthDay, value);
            }
        }
        public bool UserStatus { get; set; }
        public int OwnerAccount { get; set; }
        public string AccountLevel { get; set; }
    }
}
