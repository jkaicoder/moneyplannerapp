﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyPlannerApp.Models
{
    public class InfoID
    {
        public string UserID { get; set; }
        public string AccountID { get; set; }
        public int Role { get; set; }
        public string UserIDAdd { get; set; }
        public string UserIDRemove { get; set; }
        public string UserIDEdit { get; set; }
    }
}
