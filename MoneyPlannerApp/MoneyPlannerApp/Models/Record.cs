﻿using System;
using MoneyPlannerApp.ViewModels.Base;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace MoneyPlannerApp.Models
{
    [Table("Records")]
    public class Record : ViewModelBase
    {
        private string _recordName;
        private int _recordAmount;
        private string _recordDescription;
        private DateTime _recordDate;
        private Category _recordCategory;
        private string _recordTypeColor;
        private string _recordUserName;

        public string RecordID { get; set; }

        public string RecordName
        {
            get => _recordName;
            set
            {
                SetProperty(ref _recordName, value);
            }
        }
        public int RecordAmount
        {
            get => _recordAmount;
            set
            {
                SetProperty(ref _recordAmount, value);
            }
        }
        public string RecordDescription
        {
            get => _recordDescription;
            set
            {
                SetProperty(ref _recordDescription, value);
            }
        }
        public DateTime RecordDate
        {
            get => _recordDate;
            set
            {
                SetProperty(ref _recordDate, value);
            }
        }

        [ForeignKey(typeof(Category))]
        public int CategoryCategoryID { get; set; }

        [OneToOne]
        public Category RecordCategory
        {
            get => _recordCategory;
            set => SetProperty(ref _recordCategory, value);
        }

        public string RecordTypeColor
        {
            get => _recordTypeColor;
            set => _recordTypeColor = value;
        }

        public string RecordUserName
        {
            get => _recordUserName;
            set => SetProperty(ref _recordUserName, value);
        }
    }
}
