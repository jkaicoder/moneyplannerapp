﻿using Plugin.Toast;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Plugin.Connectivity;

namespace MoneyPlannerApp.Services
{
    public static class ValidationClass
    {
        public static bool IsNullOrEmpty(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return true;
            }

            return false;
        }

        public static bool IsHasSpecialCharacters(string str)
        {
            string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,""";

            for (int i = 0; i < specialChar.Length; i++)
            {
                for (int j = 0; j < str.Length; j++)
                {
                    if (specialChar[i].CompareTo(str[j]) == 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static bool MinimumString(string str, int minNumber)
        {
            str = str.Trim();
            if (str.Length < minNumber)
            {
                return true;
            }

            return false;
        }

        public static bool StartSpaces(string str)
        {
            for (int i = 0; i < str.Length; i++)
            {
                if (str[0].ToString() == " ")
                {
                    return true;
                }
            }

            return false;
        }

        public static bool EndSpaces(string str)
        {
            if (str[str.Length-1].ToString() == " ")
            {
                return true;
            }

            return false;
        }

        public static bool IsUppercase(string str)
        {
            // Consider string to be uppercase if it has no lowercase letters.
            for (int i = 0; i < str.Length; i++)
            {
                if (Char.IsUpper(str[i]))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsValidEmail(string email)
        {
            return new EmailAddressAttribute().IsValid(email);
        }

        //public static string CutSpacesCharacters(string str)
        //{
        //    for (int i = 0; i < str.Length; i++)
        //    {
        //        if (str[i].ToString() == " " && str[i + 1].ToString() == " ")
        //        {
        //            str[i+1].
        //        }
        //    }
        //}

        public static bool CheckInternet()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                return true;
            }
            else
            {
                CrossToastPopUp.Current.ShowToastError("No internet connection!");
                return false;
            }
        }
    }
}
