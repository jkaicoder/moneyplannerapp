﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using MoneyPlannerApp.Models;

namespace MoneyPlannerApp.Services
{
    public class AccountApiResponse<T>
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public ObservableCollection<Account> Data { get; set; }
        public int StatusCode { get; set; }
    }
}
