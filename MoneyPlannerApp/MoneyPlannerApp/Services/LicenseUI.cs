﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyPlannerApp.Services
{
    public static class LicenseUI
    {
        private const string LICENSE = "OTQ2NDhAMzEzNzJlMzEyZTMwQjAzZGRKWTdyNTRqUFFvdnExU1ZOMkhWeGwyMU85a2gybm5KR1JNdWRoND0=";

        public static string GetLicense()
        {
            return LICENSE;
        }
    }
}
