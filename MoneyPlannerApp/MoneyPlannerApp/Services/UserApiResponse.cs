﻿using MoneyPlannerApp.Models;

namespace MoneyPlannerApp.Services
{
    public class UserApiResponse<T>
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public string Token { get; set; }
        public User Data { get; set; }
        public int StatusCode { get; set; }
    }
}
