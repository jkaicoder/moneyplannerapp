﻿using MoneyPlannerApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace MoneyPlannerApp.Services
{
    public class ReportApiResponse<T>
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public ObservableCollection<Report> Data { get; set; }
        public int StatusCode { get; set; }
    }
}
