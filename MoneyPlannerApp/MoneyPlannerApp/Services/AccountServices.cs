﻿using System.Linq;
using System.Threading.Tasks;
using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Models;

namespace MoneyPlannerApp.Services
{
    public class AccountServices
    {
        private readonly HttpService _httpService;
        private readonly UserToken _userToken;

        public AccountServices()
        {
            _httpService = new HttpService();
            _userToken = UserDAO.GetUserToken().Last();
        }

        public Task<AccountApiResponseOneResult<object>> InsertAccountAsync(Account account)
        {
            var url = Configuration.ID_HOST + "account/" + _userToken.UserID;

            return _httpService.PostAsyncAccountWithToken<object>(url, account, _userToken.Token);
        }

        public Task<AccountApiResponse<object>> GetAccountListAsync()
        {
            var url = Configuration.ID_HOST + "account/" + _userToken.UserID;
            return _httpService.GetAsyncAccountWithToken<object>(url, _userToken.Token);
        }

        public Task<AccountApiResponseOneResult<object>> GetAccountForID(string accountID)
        {
            var url = Configuration.ID_HOST + "account/getaccount/" + accountID;
            return _httpService.GetAsyncAccountForIDWithToken<object>(url, _userToken.Token);
        }

        public Task<AccountApiResponseOneResult<object>> AddUserToAccount(InfoID infoId)
        {
            var url = Configuration.ID_HOST + "account/sendMailUserToAccount";
            return _httpService.PutAsyncAccountWithToken<object>(url, infoId, _userToken.Token);
        }

        public Task<AccountApiResponseOneResult<object>> RemoveUserFromAccount(RemoveUserAccount removeUserAccount)
        {
            var url = Configuration.ID_HOST + "account/removeUserInAccount";
            return _httpService.DeleteAsyncAccountWithToken<object>(url, removeUserAccount, _userToken.Token);
        }

        public Task<AccountApiResponseOneResult<object>> DeleteAccount(InfoID infoId)
        {
            var url = Configuration.ID_HOST + "account";
            return _httpService.DeleteAsyncAccountWithToken<object>(url, infoId, _userToken.Token);
        }

        public Task<AccountApiResponseOneResult<object>> ResetAccount(InfoID infoId)
        {
            var url = Configuration.ID_HOST + "account/resetAccount";
            return _httpService.PutAsyncAccountWithToken<object>(url, infoId, _userToken.Token);
        }

        public Task<AccountApiResponseOneResult<object>> LeaveAccount(InfoID infoId)
        {
            var url = Configuration.ID_HOST + "account/leaveUserInAccount";
            return _httpService.DeleteAsyncAccountWithToken<object>(url, infoId, _userToken.Token);
        }

        public Task<AccountApiResponseOneResult<object>> SetLevelAccountMember(InfoID infoId)
        {
            var url = Configuration.ID_HOST + "account/setOwnerUserInAccount";
            return _httpService.PutAsyncAccountWithToken<object>(url, infoId, _userToken.Token);
        }

    }
}
