﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using MoneyPlannerApp.Models;

namespace MoneyPlannerApp.Services
{
    public class TotalApiResponse<T>
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public TotalAccountRecord Data { get; set; }
        public int StatusCode { get; set; }
    }

    public class TotalAccountRecord
    {
        public int TotalRecord { get; set; }
        public int TotalAccount { get; set; }
    }
}
