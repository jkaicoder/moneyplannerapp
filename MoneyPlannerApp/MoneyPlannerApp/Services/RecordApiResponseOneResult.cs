﻿using MoneyPlannerApp.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyPlannerApp.Services
{
    public class RecordApiResponseOneResult<T>
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public Record Data { get; set; }
        public int StatusCode { get; set; }
    }
}
