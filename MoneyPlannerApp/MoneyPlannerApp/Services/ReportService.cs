﻿using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyPlannerApp.Services
{
    public class ReportService
    {
        private HttpService _httpService;
        private ReportSendApi _reportSendApi;
        public UserToken _userToken;

        public ReportService()
        {
            _httpService = new HttpService();
            _reportSendApi = new ReportSendApi();
        }

        public Task<ReportApiResponse<object>> GetReportTypeDayAsync (Account selectedAccountDropdown)
        {
            var url = Configuration.ID_HOST + "report/account/day";
            _userToken = UserDAO.GetUserToken().Last();
            _reportSendApi.Date = DateTime.Now;
            _reportSendApi.AccountID = selectedAccountDropdown.AccountID;
            
            return _httpService.GetAsyncReport<object>(url, _reportSendApi, _userToken.Token);
        }
        public Task<ReportApiResponse<object>> GetReportTypeMonthAsync(Account selectedAccountDropdown)
        {
            var url = Configuration.ID_HOST + "report/account/month";
            _userToken = UserDAO.GetUserToken().Last();
            _reportSendApi.Date = DateTime.Now;
            _reportSendApi.AccountID = selectedAccountDropdown.AccountID;

            return _httpService.GetAsyncReport<object>(url, _reportSendApi, _userToken.Token);
        }
    }

    public class ReportSendApi
    {
        public DateTime Date { get; set; }
        public string AccountID { get; set; }
    }
}
