﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using MoneyPlannerApp.Models;

namespace MoneyPlannerApp.Services
{
    public class RecordApiResponseListeResult<T>
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public ObservableCollection<Record> Data { get; set; }
        public int StatusCode { get; set; }
    }
}
