﻿using System;
using System.Collections.Generic;
using System.Text;
using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Models;
using System.Threading.Tasks;
using System.Linq;
using MoneyPlannerApp.ViewModels;
using System.Diagnostics;

namespace MoneyPlannerApp.Services
{
    class RecordServices
    {
        public HttpService _httpService;
        public UserToken _userToken;

        public RecordServices()
        {
            _httpService = new HttpService();
        }

        public Task<RecordApiResponseListeResult<object>> GetRecordForAccountAsync(Account selectedAccountDropdown)
        {
            _userToken = UserDAO.GetUserToken().Last();
            var url = Configuration.ID_HOST + "record/" + selectedAccountDropdown.AccountID;
            return _httpService.GetAsyncRecordWithToken<object>(url, _userToken.Token);
        }

        public Task<RecordApiResponseOneResult<object>> InsertRecordAsync (Record record)
        {
            
            _userToken = UserDAO.GetUserToken().Last();
            var url = Configuration.ID_HOST + "record/" + DataViewAndViewModel.SelectedAccountDropdown.AccountID + "/" + _userToken.UserID;
            return _httpService.PostAsyncRecordWithToken<object>(url, record, _userToken.Token);
        }

        public Task<RecordApiResponseOneResult<object>> DeleteRecordAsync (Record record)
        {
            _userToken = UserDAO.GetUserToken().Last();
            var url = Configuration.ID_HOST + "record/" + record.RecordID + "/" + DataViewAndViewModel.SelectedAccountDropdown.AccountID + "/" + _userToken.UserID;
            return _httpService.DeleteAsyncRecordWithToken<object>(url, record, _userToken.Token);

        }

        public Task<RecordApiResponseOneResult<object>> UpdateRecordAsync (Record record)
        {
            _userToken = UserDAO.GetUserToken().Last();
            var url = Configuration.ID_HOST + "record/" + record.RecordID + "/" + DataViewAndViewModel.SelectedAccountDropdown.AccountID + "/" + _userToken.UserID;
            Debug.Write(url);
            return _httpService.UpdateAsyncRecordWithToken<object>(url, record, _userToken.Token);
        }
    }
}
