﻿using System;
using System.Collections.Generic;
using System.Text;
using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Views;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace MoneyPlannerApp.Services
{
    public static class TokenServices
    {
        public static async void CheckToken(int resultAsyncStatusCode, INavigation navigation)
        {
            if (resultAsyncStatusCode == 401)
            {
                UserDAO.RemoveUserToken();
                await navigation.PushAsync(new IntroPage());
            }
        }

        public static async void CheckToken(int resultAsyncStatusCode, INavigation navigation,
            PopupAccountRecordListPage popupAccountRecordListPage)
        {
            if (resultAsyncStatusCode == 401)
            {
                UserDAO.RemoveUserToken();
                navigation.RemovePopupPageAsync(popupAccountRecordListPage);
                await navigation.PushAsync(new IntroPage());
            }
        }

        public static async void CheckToken(int resultAsyncStatusCode, INavigation navigation, PopupSettingsAccountPage popupSettingsAccountPage)
        {
            if (resultAsyncStatusCode == 401)
            {
                UserDAO.RemoveUserToken();
                navigation.RemovePopupPageAsync(popupSettingsAccountPage);
                await navigation.PushAsync(new IntroPage());
            }
        }

        public static async void CheckToken(int resultAsyncStatusCode, INavigation navigation, PopupAccountUserListPage popupAccountUserListPage)
        {
            if (resultAsyncStatusCode == 401)
            {
                UserDAO.RemoveUserToken();
                navigation.RemovePopupPageAsync(popupAccountUserListPage);
                await navigation.PushAsync(new IntroPage());
            }
        }

        internal static async void CheckToken(int resultAsyncStatusCode, INavigation navigation, PopupSummaryPage popupSummaryPage)
        {
            if (resultAsyncStatusCode == 401)
            {
                UserDAO.RemoveUserToken();
                navigation.RemovePopupPageAsync(popupSummaryPage);
                await navigation.PushAsync(new IntroPage());
            }
        }

        internal static async void CheckToken(int resultAsyncStatusCode, INavigation navigation, PopupChangePasswordPage popupChangePasswordPage)
        {
            if (resultAsyncStatusCode == 401)
            {
                UserDAO.RemoveUserToken();
                navigation.RemovePopupPageAsync(popupChangePasswordPage);
                await navigation.PushAsync(new IntroPage());
            }
        }
    }
}
