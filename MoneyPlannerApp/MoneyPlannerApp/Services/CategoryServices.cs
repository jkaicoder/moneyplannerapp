﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoneyPlannerApp.DAO;
using MoneyPlannerApp.Models;

namespace MoneyPlannerApp.Services
{
    public class CategoryServices
    {
        private readonly HttpService _httpService;
        private UserToken _userToken;

        public CategoryServices()
        {
            _httpService = new HttpService();
            _userToken = UserDAO.GetUserToken().Last();
        }

        public Task<CategoryApiResponseOneResult<object>> InsertCategory(Category category)
        {
            var url = Configuration.ID_HOST + "category";
            return _httpService.PostAsyncCategoryWithToken<object>(url, category, _userToken.Token);
        }

        public Task<CategoryApiResponseListResult<object>> GetCategoryList()
        {
            var url = Configuration.ID_HOST + "category";
            return _httpService.GetAsyncCategoryWithToken<object>(url, _userToken.Token);
        }

        public Task<CategoryApiResponseOneResult<object>> UpdateCategory(Category category)
        {
            var url = Configuration.ID_HOST + "category/" + category.CategoryID;
            return _httpService.PutAsyncCategoryWithToken<object>(url, category, _userToken.Token);
        }

        public Task<CategoryApiResponseListResult<object>> DeleteCategory(Category category)
        {
            var url = Configuration.ID_HOST + "category/" + category.CategoryID;
            return _httpService.DeleteAsyncCategoryWithToken<object>(url, _userToken.Token);
        }
    }
}
