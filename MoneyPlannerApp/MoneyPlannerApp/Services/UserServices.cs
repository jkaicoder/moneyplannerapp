
﻿using MoneyPlannerApp.DAO;
using System.Linq;
﻿using System.Diagnostics;
using MoneyPlannerApp.Models;
using System.Threading.Tasks;
 using MoneyPlannerApp.ViewModels;


 namespace MoneyPlannerApp.Services
{
    public class UserServices
    {
        private readonly HttpService _httpService;
        private UserToken _userToken;

        public UserServices()
        {
            _httpService = new HttpService();
        }

        public Task<UserApiResponse<object>> SignUpAsync(User user)
        {
            var url = Configuration.ID_HOST + "user";

            return _httpService.PostAsync<object>(url, user);
        }

        public Task<UserApiResponse<object>> SignInAsync(User user)
        {
            var url = Configuration.ID_HOST + "user/login";

            return _httpService.PostAsync<object>(url, user);
        }

        public Task<UserApiResponse<object>> ChangePasswordAsync(Password password)
        {
            _userToken = UserDAO.GetUserToken().Last();
            var url = Configuration.ID_HOST + "user/password/" + _userToken.UserID;

            return _httpService.PutAsyncUserWithToken<object>(url, password, _userToken.Token);
        }

        public Task<UserListApiResponse<object>> GetUserListInvite(string accountID)
        {
            _userToken = UserDAO.GetUserToken().Last();
            var url = Configuration.ID_HOST + "account/getUserInviteToAccount/" + accountID;
            return _httpService.GetAsyncUserListInviteWithToken<object>(url, _userToken.Token);
        }


        public Task<UserApiResponse<object>> GetInfoUserCurrent(string userID)
        {
            _userToken = UserDAO.GetUserToken().Last();
            var url = Configuration.ID_HOST + "user/" + userID;
            return _httpService.GetAsyncUserWith<object>(url, _userToken.Token);
        }

        public Task<UserApiResponse<object>> UpdateUserInfoAsync(User user)
        {
            _userToken = UserDAO.GetUserToken().Last();
            var url = Configuration.ID_HOST + "user/" + _userToken.UserID;
            
            return _httpService.PutAsyncUserWithToken<object>(url, user, _userToken.Token);
        }

        public Task<EmailApiResponse<object>> ForgetPassword(ForgetPassword email)
        {
            var url = Configuration.ID_HOST + "user/forgetpassword";
            return _httpService.PostAsyncForgetPassword<object>(url, email);
        }

        public Task<EmailApiResponse<object>> ResetPassword(ForgetPassword resetPassword)
        {
            var url = Configuration.ID_HOST + "user/resetpassword";
            return _httpService.PostAsyncForgetPassword<object>(url, resetPassword);
        }

        public Task<TotalApiResponse<object>> TotalAccountRecord()
        {
            _userToken = UserDAO.GetUserToken().Last();
            var url = Configuration.ID_HOST + "user/totalAccountRecord/" +  _userToken.UserID;
            //var url = "google.com.vn" + _userToken.UserID;
            Debug.Write(url);
            return _httpService.GetAsyncTotalAccountRecord<object>(url, _userToken.Token);
        }

    }
}
