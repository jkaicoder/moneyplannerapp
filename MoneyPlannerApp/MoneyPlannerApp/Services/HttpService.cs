﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MoneyPlannerApp.Models;
using Newtonsoft.Json;
using Plugin.Toast;
namespace MoneyPlannerApp.Services
{
    public class HttpService
    {
        private HttpClient DefaultHttpClient()
        {
            return new HttpClient();
        }

        /*--------------Send API User--------------*/

        private async Task<UserApiResponse<T>> SendAsync<T>(string url, HttpMethod method, HttpContent content = null)
        {
            
                var request = new HttpRequestMessage(method, url);
                if (content != null)
                {
                    request.Content = content;
                }

                using (var client = DefaultHttpClient())
                {
                    var response = await client.SendAsync(request);

                    var body = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<UserApiResponse<T>>(body);

                }
            

        }

        private async Task<TotalApiResponse<T>> SendAsyncTotalWithToken<T>(string url, HttpMethod method, string token)
        {
            var request = new HttpRequestMessage(method, url);

            using (var client = DefaultHttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var response = await client.SendAsync(request);

                var body = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<TotalApiResponse<T>>(body);
            }
        }



        private async Task<UserApiResponse<T>> SendAsyncUserWithToken<T>(string url, HttpMethod method, string token, HttpContent content = null)
        {
            var request = new HttpRequestMessage(method, url);
            if (content != null)
            {
                request.Content = content;
            }
            using (var client = DefaultHttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var response = await client.SendAsync(request);

                var body = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<UserApiResponse<T>>(body);
            }
        }

        

        private async Task<UserListApiResponse<T>> SendAsyncUserWithToken<T>(string url, HttpMethod method, string token)
        {
            var request = new HttpRequestMessage(method, url);

            using (var client = DefaultHttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var response = await client.SendAsync(request);

                var body = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<UserListApiResponse<T>>(body);
            }
        }

        private async Task<UserApiResponse<T>> SendAsyncUserOneResultWithToken<T>(string url, HttpMethod method, string token)
        {
            var request = new HttpRequestMessage(method, url);

            using (var client = DefaultHttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var response = await client.SendAsync(request);

                var body = await response.Content.ReadAsStringAsync();
                Debug.Write(body);
                return JsonConvert.DeserializeObject<UserApiResponse<T>>(body);
            }
        }

        private async Task<UserApiResponse<T>> SendAsyncUserOneResultMultipartFormDatan<T>(string url, HttpMethod method, HttpContent content = null)
        {
            using (var client = new HttpClient())
            using (var formData = new MultipartFormDataContent())
            {
                formData.Add(new ByteArrayContent(File.ReadAllBytes("/path/to/file.jpg")), "image_file", "file.jpg");
                formData.Add(new StringContent("auto"), "size");
                var response = client.PostAsync("https://api.remove.bg/v1.0/removebg", formData).Result;

                if (response.IsSuccessStatusCode)
                {
                    FileStream fileStream = new FileStream("no-bg.png", FileMode.Create, FileAccess.Write, FileShare.None);
                    await response.Content.CopyToAsync(fileStream).ContinueWith((copyTask) => { fileStream.Close(); });
                }
                else
                {
                    Console.WriteLine("Error: " + response.Content.ReadAsStringAsync().Result);
                }
            }

            return null;
        }

        private async Task<EmailApiResponse<T>> SendAsyncForgetPassword<T>(string url, HttpMethod method, HttpContent content = null)
        {
            var request = new HttpRequestMessage(method, url);
            if (content != null)
            {
                request.Content = content;
            }

            using (var client = DefaultHttpClient())
            {
                var response = await client.SendAsync(request);

                var body = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<EmailApiResponse<T>>(body);
            }
        }

        /*--------------Send Api Record--------------*/

        private async Task<RecordApiResponseOneResult<T>> SendAsyncRecordOneResultWithToken<T>(string url, HttpMethod post, string token, StringContent content = null)
        {
            var request = new HttpRequestMessage(post, url);
            if (content != null)
            {
                request.Content = content;
            }
            using (var client = DefaultHttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var response = await client.SendAsync(request);
                var body = await response.Content.ReadAsStringAsync();
                Debug.Write(body);
                return JsonConvert.DeserializeObject<RecordApiResponseOneResult<T>>(body);
            }

        }

        private async Task<RecordApiResponseListeResult<T>> SendAsyncRecordListResultWithToken<T>(string url, HttpMethod method, string token)
        {
            var request = new HttpRequestMessage(method, url);

            using (var client = DefaultHttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var response = await client.SendAsync(request);

                var body = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<RecordApiResponseListeResult<T>>(body);

            }
        }


        /*-------------Send API Category--------------*/

        private async Task<CategoryApiResponseOneResult<T>> SendAsyncCategoryOneResultWithToken<T>(string url, HttpMethod method, string token, HttpContent content = null)
        {
            var request = new HttpRequestMessage(method, url);
            if (content != null)
            {
                request.Content = content;
            }

            using (var client = DefaultHttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var response = await client.SendAsync(request);

                var body = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<CategoryApiResponseOneResult<T>>(body);

            }
        }

        private async Task<CategoryApiResponseListResult<T>> SendAsyncCategoryListResultWithToken<T>(string url, HttpMethod method, string token)
        {
            var request = new HttpRequestMessage(method, url);

            using (var client = DefaultHttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var response = await client.SendAsync(request);

                var body = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<CategoryApiResponseListResult<T>>(body);
            }
        }

        /*--------------Send API Report--------------*/

        private async Task<ReportApiResponse<T>> SendAsyncReportListResultAsync<T>(string url, HttpMethod method, string token, StringContent content)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            if (content != null)
            {
                request.Content = content;
            }

            using (var client = DefaultHttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var response = await client.SendAsync(request);

                var body = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<ReportApiResponse<T>>(body);

            }
        }

        /*--------------Send API Account--------------*/

        private async Task<AccountApiResponse<T>> SendAsyncAccountWithToken<T>(string url, HttpMethod method, string token, HttpContent content = null)
        {
            var request = new HttpRequestMessage(method, url);
            if (content != null)
            {
                request.Content = content;
            }

            using (var client = DefaultHttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var response = await client.SendAsync(request);
                var body = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<AccountApiResponse<T>>(body);
            }
        }

        private async Task<AccountApiResponseOneResult<T>> SendAsyncAccountOneResultWithToken<T>(string url, HttpMethod method, string token, HttpContent content = null)
        {
            var request = new HttpRequestMessage(method, url);
            if (content != null)
            {
                request.Content = content;
            }

            using (var client = DefaultHttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var response = await client.SendAsync(request);

                var body = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<AccountApiResponseOneResult<T>>(body);

            }
        }

        private async Task<AccountApiResponse<T>> SendAsyncAccountWithToken<T>(string url, HttpMethod method, string token)
        {
            var request = new HttpRequestMessage(method, url);

            using (var client = DefaultHttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var response = await client.SendAsync(request);

                var body = await response.Content.ReadAsStringAsync();

                Debug.Write(body);

                return JsonConvert.DeserializeObject<AccountApiResponse<T>>(body);

            }
        }

        private async Task<AccountApiResponseOneResult<T>> SendAsyncAccountForIDWithTokenAsync<T>(string url, HttpMethod method, string token)
        {
            var request = new HttpRequestMessage(method, url);

            using (var client = DefaultHttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var response = await client.SendAsync(request);

                var body = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<AccountApiResponseOneResult<T>>(body);
            }
        }

        /*--------------POST--------------*/

        //User

        internal Task<UserApiResponse<T>> PostAsync<T>(string url, object body)
        {
            var content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
            return SendAsync<T>(url, HttpMethod.Post, content);
        }

        public Task<UserListApiResponse<T>> GetAsyncUserListInviteWithToken<T>(string url, string token)
        {
            return SendAsyncUserWithToken<T>(url, HttpMethod.Get, token);
        }

        public Task<EmailApiResponse<T>> PostAsyncForgetPassword<T>(string url, object body)
        {
            var content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
            return SendAsyncForgetPassword<T>(url, HttpMethod.Post, content);
        }

        
        //Record

        internal Task<RecordApiResponseOneResult<T>> PostAsyncRecordWithToken<T>(string url, Record record, string token)
        {
            var content = new StringContent(JsonConvert.SerializeObject(record), Encoding.UTF8, "application/json");
            return SendAsyncRecordOneResultWithToken<T>(url, HttpMethod.Post, token, content);
        }



        //Category

        internal Task<CategoryApiResponseOneResult<T>> PostAsyncCategoryWithToken<T>(string url, object body, string token)
        {
            var content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
            return SendAsyncCategoryOneResultWithToken<T>(url, HttpMethod.Post, token, content);

        }

        


        //Account
        internal Task<AccountApiResponseOneResult<T>> PostAsyncAccountWithToken<T>(string url, object body, string token)
        {
            var content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
            return SendAsyncAccountOneResultWithToken<T>(url, HttpMethod.Post, token, content);
        }

        /*--------------GET--------------*/
        public Task<TotalApiResponse<T>> GetAsyncTotalAccountRecord<T>(string url, string token)
        {
            return SendAsyncTotalWithToken<T>(url, HttpMethod.Get, token);
        }

        


        public Task<UserApiResponse<T>> GetAsyncUserWith<T>(string url, string token)
        {
            return SendAsyncUserOneResultWithToken<T>(url, HttpMethod.Get, token);
        }

        public Task<CategoryApiResponseListResult<T>> GetAsyncCategoryWithToken<T>(string url, string token)
        {
            return SendAsyncCategoryListResultWithToken<T>(url, HttpMethod.Get, token);
        }

        public Task<RecordApiResponseListeResult<T>> GetAsyncRecordWithToken<T>(string url, string token)
        {
            return SendAsyncRecordListResultWithToken<T>(url, HttpMethod.Get, token);
        }

        /*--------------Get report--------------*/

        public Task<ReportApiResponse<T>> GetAsyncReport<T> (string url, object body, string token)
        {
            var content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
            return SendAsyncReportListResultAsync<T>(url, HttpMethod.Get, token, content);
        }

        

        public Task<AccountApiResponse<T>> GetAsyncAccountWithToken<T>(string url, string token)
        {
            return SendAsyncAccountWithToken<T>(url, HttpMethod.Get, token);
        }

        public Task<AccountApiResponseOneResult<T>> GetAsyncAccountForIDWithToken<T>(string url, string token)
        {
            return SendAsyncAccountForIDWithTokenAsync<T>(url, HttpMethod.Get, token);
        }

        


        /*--------------PUT--------------*/

        public Task<AccountApiResponseOneResult<T>> PutAsyncAccountWithToken<T>(string url, object body ,string token)
        {
            var content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
            return SendAsyncAccountOneResultWithToken<T>(url, HttpMethod.Put, token, content);
        }

        public Task<UserApiResponse<T>> PutAsyncUserWithToken<T>(string url, object body, string token)
        {
            var content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
            return SendAsyncUserWithToken<T>(url, HttpMethod.Put, token, content);
        }

        public Task<CategoryApiResponseOneResult<T>> PutAsyncCategoryWithToken<T>(string url, object body, string token)
        {
            var content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
            return SendAsyncCategoryOneResultWithToken<T>(url, HttpMethod.Put, token, content);
        }

        internal Task<RecordApiResponseOneResult<T>> UpdateAsyncRecordWithToken<T>(string url, Record record, string token)
        {
            var content = new StringContent(JsonConvert.SerializeObject(record), Encoding.UTF8, "application/json");
            return SendAsyncRecordOneResultWithToken<T>(url, HttpMethod.Put, token, content);
        }

        /*--------------DELETE--------------*/


        public Task<AccountApiResponseOneResult<T>> DeleteAsyncAccountWithToken<T>(string url, object body, string token)
        {
            var content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
            return SendAsyncAccountOneResultWithToken<T>(url, HttpMethod.Delete, token, content);
        }


        internal Task<RecordApiResponseOneResult<T>> DeleteAsyncRecordWithToken<T>(string url, Record record, string token)
        {
            var content = new StringContent(JsonConvert.SerializeObject(record), Encoding.UTF8, "application/json");
            return SendAsyncRecordOneResultWithToken<T>(url, HttpMethod.Delete, token, content);
        }

        public Task<CategoryApiResponseListResult<T>> DeleteAsyncCategoryWithToken<T>(string url, string token)
        {
            return SendAsyncCategoryListResultWithToken<T>(url, HttpMethod.Delete, token);
        }


        
    }
}
