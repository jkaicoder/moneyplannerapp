﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyPlannerApp.Services
{
    public class EmailApiResponse<T>
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public int StatusCode { get; set; }
    }
}
