﻿using System;
using System.Collections.Generic;
using System.Text;
using MoneyPlannerApp.Models;

namespace MoneyPlannerApp.Services
{
    public class CategoryApiResponseOneResult<T>
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public Category Data { get; set; }
        public int StatusCode { get; set; }
    }
}
